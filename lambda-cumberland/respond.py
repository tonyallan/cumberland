import decimal
import json


class DecimalEncoder(json.JSONEncoder):
    # file sizes are saved as Decimal() -> return as int
    
    def default(self, o):
        
        if isinstance(o, decimal.Decimal):
            return int(o)
            
        return super(DecimalEncoder, self).default(o)


def result(result=None):
    
    print(f'result length={len(result)}')
    return dict(body=json.dumps(dict(code='success', result=result), cls=DecimalEncoder))


def error(code=None, message=None):
    
    if message is None:
        print(f'result code={code}')
        return dict(body=json.dumps(dict(code=code), cls=DecimalEncoder))
        
    else:
        print(f'result code={code} message={message}')
        return dict(body=json.dumps(dict(code=code, message=message), cls=DecimalEncoder))
        
