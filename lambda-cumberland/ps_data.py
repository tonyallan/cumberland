planning_scheme_data = {
    "metadata": {
        "notes": "The Historical Planning Scheme 'Delatite' is included in this list (with key 'zzz0').", 
        "source": "Planning Scheme Suffix & regions Clauses10-19.xlsx (Geoff Bullock, 16/09/2019)", 
        "timestamp": "2019-09-19T13:38:12", 
        "title": "Victoria Planning Scheme Index"
    }, 
    "planning_schemes": {
        "alpi": {
            "VC147": [
                "11", 
                "14", 
                "17", 
                "18", 
                "19H (Hume)"
            ], 
            "VC148": [
                "10-19G"
            ], 
            "lga_name": "Alpine Shire", 
            "planning_scheme": "Alpine", 
            "suffix": "alpi"
        }, 
        "alpr": {
            "VC147": [
                "G", 
                "H"
            ], 
            "VC148": [
                "K"
            ], 
            "lga_name": "Alpine Resorts (Unincorporated)", 
            "planning_scheme": "Alpine Resorts", 
            "suffix": "alpr"
        }, 
        "arat": {
            "VC147": [
                "CH (Central Highlands)"
            ], 
            "VC148": [
                "C"
            ], 
            "lga_name": "Ararat Rural City", 
            "planning_scheme": "Ararat", 
            "suffix": "arat"
        }, 
        "ball": {
            "VC147": [
                "CH (Central Highlands)"
            ], 
            "VC148": [
                "C"
            ], 
            "lga_name": "Ballarat City", 
            "planning_scheme": "Ballarat", 
            "suffix": "ball"
        }, 
        "bany": {
            "VC147": [
                "MM (Metropolitan Melbourne)"
            ], 
            "VC148": [
                "A"
            ], 
            "lga_name": "Banyule City", 
            "planning_scheme": "Banyule", 
            "suffix": "bany"
        }, 
        "basc": {
            "VC147": [
                "G (Gippsland)"
            ], 
            "VC148": [
                "E"
            ], 
            "lga_name": "Bass Coast Shire", 
            "planning_scheme": "Bass Coast", 
            "suffix": "basc"
        }, 
        "bawb": {
            "VC147": [
                "G (Gippsland)"
            ], 
            "VC148": [
                "E"
            ], 
            "lga_name": "Baw Baw Shire", 
            "planning_scheme": "Baw Baw", 
            "suffix": "bawb"
        }, 
        "bays": {
            "VC147": [
                "MM (Metropolitan Melbourne)"
            ], 
            "VC148": [
                "A"
            ], 
            "lga_name": "Bayside City", 
            "planning_scheme": "Bayside", 
            "suffix": "bays"
        }, 
        "bena": {
            "VC147": [
                "H (Hume)"
            ], 
            "VC148": [
                "G"
            ], 
            "lga_name": "Benalla Rural City", 
            "planning_scheme": "Benalla", 
            "suffix": "bena"
        }, 
        "blok": {
            "VC147": [
                "LMN (Loddon Mallee North)"
            ], 
            "VC148": [
                "H"
            ], 
            "lga_name": "Buloke Shire", 
            "planning_scheme": "Buloke", 
            "suffix": "blok"
        }, 
        "boro": {
            "VC147": [
                "MM (Metropolitan Melbourne)"
            ], 
            "VC148": [
                "A"
            ], 
            "lga_name": "Boroondara City", 
            "planning_scheme": "Boroondara", 
            "suffix": "boro"
        }, 
        "brim": {
            "VC147": [
                "MM (Metropolitan Melbourne)"
            ], 
            "VC148": [
                "A"
            ], 
            "lga_name": "Brimbank City", 
            "planning_scheme": "Brimbank", 
            "suffix": "brim"
        }, 
        "camp": {
            "VC147": [
                "LMN (Loddon Mallee North)"
            ], 
            "VC148": [
                "H"
            ], 
            "lga_name": "Campaspe Shire", 
            "planning_scheme": "Campaspe", 
            "suffix": "camp"
        }, 
        "card": {
            "VC147": [
                "MM (Metropolitan Melbourne)"
            ], 
            "VC148": [
                "A"
            ], 
            "lga_name": "Cardinia Shire", 
            "planning_scheme": "Cardinia", 
            "suffix": "card"
        }, 
        "case": {
            "VC147": [
                "MM (Metropolitan Melbourne)"
            ], 
            "VC148": [
                "A"
            ], 
            "lga_name": "Casey City", 
            "planning_scheme": "Casey", 
            "suffix": "case"
        }, 
        "cgol": {
            "VC147": [
                "MM (Metropolitan Melbourne)"
            ], 
            "VC148": [
                "I"
            ], 
            "lga_name": "Central Goldfields Shire", 
            "planning_scheme": "Central Goldfields", 
            "suffix": "cgol"
        }, 
        "cola": {
            "VC147": [
                "G21 (Geelong G21)"
            ], 
            "VC148": [
                "D"
            ], 
            "lga_name": "Colac Otway Shire", 
            "planning_scheme": "Colac Otway", 
            "suffix": "cola"
        }, 
        "cora": {
            "VC147": [
                "GSC (Great South Coast)"
            ], 
            "VC148": [
                "F"
            ], 
            "lga_name": "Corangamite Shire", 
            "planning_scheme": "Corangamite", 
            "suffix": "cora"
        }, 
        "dare": {
            "VC147": [
                "MM (Metropolitan Melbourne)"
            ], 
            "VC148": [
                "A"
            ], 
            "lga_name": "Darebin City", 
            "planning_scheme": "Darebin", 
            "suffix": "dare"
        }, 
        "egip": {
            "VC147": [
                "G (Gippsland)"
            ], 
            "VC148": [
                "E"
            ], 
            "lga_name": "East Gippsland Shire", 
            "planning_scheme": "East Gippsland", 
            "suffix": "egip"
        }, 
        "fisi": {
            "VC147": [
                "G (Gippsland)"
            ], 
            "VC148": [
                "E"
            ], 
            "lga_name": "French Island (Unincorporated)", 
            "notes": "a.k.a. 'French Island & Sandstone Island'.", 
            "planning_scheme": "French Island and Sandstone Island", 
            "suffix": "fisi"
        }, 
        "fran": {
            "VC147": [
                "MM (Metropolitan Melbourne)"
            ], 
            "VC148": [
                "A"
            ], 
            "lga_name": "Frankston City", 
            "planning_scheme": "Frankston", 
            "suffix": "fran"
        }, 
        "gann": {
            "VC147": [
                "LMN (Loddon Mallee North)"
            ], 
            "VC148": [
                "H"
            ], 
            "lga_name": "Gannawarra Shire", 
            "planning_scheme": "Gannawarra", 
            "suffix": "gann"
        }, 
        "gben": {
            "VC147": [
                "LMS (Loddon Mallee South)"
            ], 
            "VC148": [
                "I"
            ], 
            "lga_name": "Greater Bendigo City", 
            "planning_scheme": "Greater Bendigo", 
            "suffix": "gben"
        }, 
        "gdan": {
            "VC147": [
                "MM (Metropolitan Melbourne)"
            ], 
            "VC148": [
                "A"
            ], 
            "lga_name": "Greater Dandenong City", 
            "planning_scheme": "Greater Dandenong", 
            "suffix": "gdan"
        }, 
        "gelg": {
            "VC147": [
                "GSC (Great South Coast)"
            ], 
            "VC148": [
                "F"
            ], 
            "lga_name": "Glenelg Shire", 
            "planning_scheme": "Glenelg", 
            "suffix": "gelg"
        }, 
        "ggee": {
            "VC147": [
                "G21 (Geelong G21)"
            ], 
            "VC148": [
                "D"
            ], 
            "lga_name": "Greater Geelong City", 
            "planning_scheme": "Greater Geelong", 
            "suffix": "ggee"
        }, 
        "glen": {
            "VC147": [
                "MM (Metropolitan Melbourne)"
            ], 
            "VC148": [
                "A"
            ], 
            "lga_name": "Glen Eira City", 
            "planning_scheme": "Glen Eira", 
            "suffix": "glen"
        }, 
        "gpla": {
            "VC147": [
                "CH", 
                " G21"
            ], 
            "VC148": [
                "M"
            ], 
            "lga_name": "Golden Plains Shire", 
            "planning_scheme": "Golden Plains", 
            "suffix": "gpla"
        }, 
        "gshe": {
            "VC147": [
                "H (Hume)"
            ], 
            "VC148": [
                "G"
            ], 
            "lga_name": "Greater Shepparton City", 
            "planning_scheme": "Greater Shepparton", 
            "suffix": "gshe"
        }, 
        "hbay": {
            "VC147": [
                "MM (Metropolitan Melbourne)"
            ], 
            "VC148": [
                "A"
            ], 
            "lga_name": "Hobsons Bay City", 
            "planning_scheme": "Hobsons Bay", 
            "suffix": "hbay"
        }, 
        "hepb": {
            "VC147": [
                "CH (Central Highlands)"
            ], 
            "VC148": [
                "C"
            ], 
            "lga_name": "Hepburn Shire", 
            "planning_scheme": "Hepburn", 
            "suffix": "hepb"
        }, 
        "hind": {
            "VC147": [
                "WSM (Wimmera Southern Mallee)"
            ], 
            "VC148": [
                "J"
            ], 
            "lga_name": "Hindmarsh Shire", 
            "planning_scheme": "Hindmarsh", 
            "suffix": "hind"
        }, 
        "hors": {
            "VC147": [
                "WSM (Wimmera Southern Mallee)"
            ], 
            "VC148": [
                "J"
            ], 
            "lga_name": "Horsham Rural City", 
            "planning_scheme": "Horsham", 
            "suffix": "hors"
        }, 
        "hume": {
            "VC147": [
                "MM (Metropolitan Melbourne)"
            ], 
            "VC148": [
                "A"
            ], 
            "lga_name": "Hume City", 
            "planning_scheme": "Hume", 
            "suffix": "hume"
        }, 
        "indi": {
            "VC147": [
                "H (Hume)"
            ], 
            "VC148": [
                "G"
            ], 
            "lga_name": "Indigo Shire", 
            "planning_scheme": "Indigo", 
            "suffix": "indi"
        }, 
        "king": {
            "VC147": [
                "MM (Metropolitan Melbourne)"
            ], 
            "VC148": [
                "A"
            ], 
            "lga_name": "Kingston City", 
            "planning_scheme": "Kingston", 
            "suffix": "king"
        }, 
        "knox": {
            "VC147": [
                "MM (Metropolitan Melbourne)"
            ], 
            "VC148": [
                "A"
            ], 
            "lga_name": "Knox City", 
            "planning_scheme": "Knox", 
            "suffix": "knox"
        }, 
        "latr": {
            "VC147": [
                "G (Gippsland)"
            ], 
            "VC148": [
                "E"
            ], 
            "lga_name": "Latrobe City", 
            "planning_scheme": "Latrobe", 
            "suffix": "latr"
        }, 
        "lodd": {
            "VC147": [
                "LMS (Loddon Mallee South)"
            ], 
            "VC148": [
                "I"
            ], 
            "lga_name": "Loddon Shire", 
            "planning_scheme": "Loddon", 
            "suffix": "lodd"
        }, 
        "macr": {
            "VC147": [
                "LMS (Loddon Mallee South)"
            ], 
            "VC148": [
                "L"
            ], 
            "lga_name": "Macedon Ranges Shire", 
            "planning_scheme": "Macedon Ranges", 
            "suffix": "macr"
        }, 
        "malx": {
            "VC147": [
                "LMS (Loddon Mallee South)"
            ], 
            "VC148": [
                "I"
            ], 
            "lga_name": "Mount Alexander Shire", 
            "planning_scheme": "Mount Alexander", 
            "suffix": "malx"
        }, 
        "mann": {
            "VC147": [
                "MM (Metropolitan Melbourne)"
            ], 
            "VC148": [
                "A"
            ], 
            "lga_name": "Manningham City", 
            "planning_scheme": "Manningham", 
            "suffix": "mann"
        }, 
        "mans": {
            "VC147": [
                "H (Hume)"
            ], 
            "VC148": [
                "G"
            ], 
            "lga_name": "Mansfield Shire", 
            "planning_scheme": "Mansfield", 
            "suffix": "mans"
        }, 
        "mari": {
            "VC147": [
                "MM (Metropolitan Melbourne)"
            ], 
            "VC148": [
                "A"
            ], 
            "lga_name": "Maribyrnong City", 
            "planning_scheme": "Maribyrnong", 
            "suffix": "mari"
        }, 
        "maro": {
            "VC147": [
                "MM (Metropolitan Melbourne)"
            ], 
            "VC148": [
                "A"
            ], 
            "lga_name": "Maroondah City", 
            "planning_scheme": "Maroondah", 
            "suffix": "maro"
        }, 
        "melb": {
            "VC147": [
                "MM (Metropolitan Melbourne)"
            ], 
            "VC148": [
                "A"
            ], 
            "lga_name": "Melbourne City", 
            "planning_scheme": "Melbourne", 
            "suffix": "melb"
        }, 
        "melt": {
            "VC147": [
                "MM (Metropolitan Melbourne)"
            ], 
            "VC148": [
                "A"
            ], 
            "lga_name": "Melton Shire", 
            "planning_scheme": "Melton", 
            "suffix": "melt"
        }, 
        "mild": {
            "VC147": [
                "LMN (Loddon Mallee North)"
            ], 
            "VC148": [
                "H"
            ], 
            "lga_name": "Mildura Rural City", 
            "planning_scheme": "Mildura", 
            "suffix": "mild"
        }, 
        "mith": {
            "VC147": [
                "MM", 
                " H"
            ], 
            "VC148": [
                "N"
            ], 
            "lga_name": "Mitchell Shire", 
            "planning_scheme": "Mitchell", 
            "suffix": "mith"
        }, 
        "moir": {
            "VC147": [
                "H (Hume)"
            ], 
            "VC148": [
                "G"
            ], 
            "lga_name": "Moira Shire", 
            "planning_scheme": "Moira", 
            "suffix": "moir"
        }, 
        "mona": {
            "VC147": [
                "MM (Metropolitan Melbourne)"
            ], 
            "VC148": [
                "A"
            ], 
            "lga_name": "Monash City", 
            "planning_scheme": "Monash", 
            "suffix": "mona"
        }, 
        "moon": {
            "VC147": [
                "MM (Metropolitan Melbourne)"
            ], 
            "VC148": [
                "A"
            ], 
            "lga_name": "Moonee Valley City", 
            "planning_scheme": "Moonee Valley", 
            "suffix": "moon"
        }, 
        "moor": {
            "VC147": [
                "CH (Central Highlands)"
            ], 
            "VC148": [
                "C"
            ], 
            "lga_name": "Moorabool Shire", 
            "planning_scheme": "Moorabool", 
            "suffix": "moor"
        }, 
        "more": {
            "VC147": [
                "MM (Metropolitan Melbourne)"
            ], 
            "VC148": [
                "A"
            ], 
            "lga_name": "Moreland City", 
            "planning_scheme": "Moreland", 
            "suffix": "more"
        }, 
        "morn": {
            "VC147": [
                "MM (Metropolitan Melbourne)"
            ], 
            "VC148": [
                "A"
            ], 
            "lga_name": "Mornington Peninsula Shire", 
            "planning_scheme": "Mornington Peninsula", 
            "suffix": "morn"
        }, 
        "moyn": {
            "VC147": [
                "GSC (Great South Coast)"
            ], 
            "VC148": [
                "F"
            ], 
            "lga_name": "Moyne Shire", 
            "planning_scheme": "Moyne", 
            "suffix": "moyn"
        }, 
        "muri": {
            "VC147": [
                "H (Hume)"
            ], 
            "VC148": [
                "G"
            ], 
            "lga_name": "Murrindindi Shire", 
            "planning_scheme": "Murrindindi", 
            "suffix": "muri"
        }, 
        "ngra": {
            "VC147": [
                "WSM (Wimmera Southern Mallee)"
            ], 
            "VC148": [
                "J"
            ], 
            "lga_name": "Northern Grampians Shire", 
            "planning_scheme": "Northern Grampians", 
            "suffix": "ngra"
        }, 
        "nill": {
            "VC147": [
                "MM (Metropolitan Melbourne)"
            ], 
            "VC148": [
                "A"
            ], 
            "lga_name": "Nillumbik Shire", 
            "planning_scheme": "Nillumbik", 
            "suffix": "nill"
        }, 
        "pmel": {
            "VC147": [
                "MM (Metropolitan Melbourne)"
            ], 
            "VC148": [
                "A"
            ], 
            "lga_name": "Hobsons Bay, Maribyrnong, Melbourne, Port Phillip", 
            "planning_scheme": "Port of Melbourne", 
            "suffix": "pmel"
        }, 
        "port": {
            "VC147": [
                "MM (Metropolitan Melbourne)"
            ], 
            "VC148": [
                "A"
            ], 
            "lga_name": "Port Phillip City", 
            "planning_scheme": "Port Phillip", 
            "suffix": "port"
        }, 
        "pyrn": {
            "VC147": [
                "CH (Central Highlands)"
            ], 
            "VC148": [
                "C"
            ], 
            "lga_name": "Pyrenees Shire", 
            "planning_scheme": "Pyrenees", 
            "suffix": "pyrn"
        }, 
        "quen": {
            "VC147": [
                "G21 (Geelong G21)"
            ], 
            "VC148": [
                "D"
            ], 
            "lga_name": "Queenscliffe Borough", 
            "planning_scheme": "Queenscliffe", 
            "suffix": "quen"
        }, 
        "sgip": {
            "VC147": [
                "G (Gippsland)"
            ], 
            "VC148": [
                "E"
            ], 
            "lga_name": "South Gippsland Shire", 
            "planning_scheme": "South Gippsland", 
            "suffix": "sgip"
        }, 
        "sgra": {
            "VC147": [
                "GSC (Great South Coast)"
            ], 
            "VC148": [
                "F"
            ], 
            "lga_name": "Southern Grampians Shire", 
            "planning_scheme": "Southern Grampians", 
            "suffix": "sgra"
        }, 
        "ston": {
            "VC147": [
                "MM (Metropolitan Melbourne)"
            ], 
            "VC148": [
                "A"
            ], 
            "lga_name": "Stonnington City", 
            "planning_scheme": "Stonnington", 
            "suffix": "ston"
        }, 
        "strb": {
            "VC147": [
                "H (Hume)"
            ], 
            "VC148": [
                "G"
            ], 
            "lga_name": "Strathbogie Shire", 
            "planning_scheme": "Strathbogie", 
            "suffix": "strb"
        }, 
        "surf": {
            "VC147": [
                "G21 (Geelong G21)"
            ], 
            "VC148": [
                "D"
            ], 
            "lga_name": "Surf Coast Shire", 
            "planning_scheme": "Surf Coast", 
            "suffix": "surf"
        }, 
        "swan": {
            "VC147": [
                "LMN (Loddon Mallee North)"
            ], 
            "VC148": [
                "H"
            ], 
            "lga_name": "Swan Hill Rural City", 
            "planning_scheme": "Swan Hill", 
            "suffix": "swan"
        }, 
        "towg": {
            "VC147": [
                "H (Hume)"
            ], 
            "VC148": [
                "G"
            ], 
            "lga_name": "Towong Shire", 
            "planning_scheme": "Towong", 
            "suffix": "towg"
        }, 
        "wang": {
            "VC147": [
                "H (Hume)"
            ], 
            "VC148": [
                "G"
            ], 
            "lga_name": "Wangaratta Rural City", 
            "planning_scheme": "Wangaratta", 
            "suffix": "wang"
        }, 
        "warr": {
            "VC147": [
                "GSC (Great South Coast)"
            ], 
            "VC148": [
                "F"
            ], 
            "lga_name": "Warrnambool City", 
            "planning_scheme": "Warrnambool", 
            "suffix": "warr"
        }, 
        "wdon": {
            "VC147": [
                "H (Hume)"
            ], 
            "VC148": [
                "G"
            ], 
            "lga_name": "Wodonga City", 
            "planning_scheme": "Wodonga", 
            "suffix": "wdon"
        }, 
        "well": {
            "VC147": [
                "G (Gippsland)"
            ], 
            "VC148": [
                "E"
            ], 
            "lga_name": "Wellington Shire", 
            "planning_scheme": "Wellington", 
            "suffix": "well"
        }, 
        "whse": {
            "VC147": [
                "MM (Metropolitan Melbourne)"
            ], 
            "VC148": [
                "A"
            ], 
            "lga_name": "Whitehorse City", 
            "planning_scheme": "Whitehorse", 
            "suffix": "whse"
        }, 
        "wsea": {
            "VC147": [
                "MM (Metropolitan Melbourne)"
            ], 
            "VC148": [
                "A"
            ], 
            "lga_name": "Whittlesea City", 
            "planning_scheme": "Whittlesea", 
            "suffix": "wsea"
        }, 
        "wwim": {
            "VC147": [
                "WSM (Wimmera Southern Mallee)"
            ], 
            "VC148": [
                "J"
            ], 
            "lga_name": "West Wimmera Shire", 
            "planning_scheme": "West Wimmera", 
            "suffix": "wwim"
        }, 
        "wynd": {
            "VC147": [
                "MM (Metropolitan Melbourne)"
            ], 
            "VC148": [
                "A"
            ], 
            "lga_name": "Wyndham City", 
            "planning_scheme": "Wyndham", 
            "suffix": "wynd"
        }, 
        "yara": {
            "VC147": [
                "MM (Metropolitan Melbourne)"
            ], 
            "VC148": [
                "A"
            ], 
            "lga_name": "Yarra City", 
            "planning_scheme": "Yarra", 
            "suffix": "yara"
        }, 
        "yari": {
            "VC147": [
                "WSM (Wimmera Southern Mallee)"
            ], 
            "VC148": [
                "J"
            ], 
            "lga_name": "Yarriambiack Shire", 
            "planning_scheme": "Yarriambiack", 
            "suffix": "yari"
        }, 
        "yran": {
            "VC147": [
                "MM (Metropolitan Melbourne)"
            ], 
            "VC148": [
                "A"
            ], 
            "lga_name": "Yarra Ranges Shire", 
            "planning_scheme": "Yarra Ranges", 
            "suffix": "yran"
        }, 
        "zzz0": {
            "deleted": True, 
            "notes": "Delatite was split into Benalla and Mansfield in 2003.", 
            "planning_scheme": "Delatite", 
            "suffix": "zzz0"
        }
    }
}
