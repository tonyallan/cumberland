import json
import pprint
import urllib.parse

import boto3

import respond, ps_data, function

warm_start = None
dynamodb   = None
table_psam = None
s3_client  = None


def lambda_handler(event, context):
    
    global warm_start
    global dynamodb
    global table_psam
    global s3_client
    
    if warm_start is None:
        start = 'cold'
        warm_start = True
        
    else:
        start = 'warm'

    if dynamodb is None:
        dynamodb = boto3.resource('dynamodb', region_name='ap-southeast-2')
        
    if table_psam is None:
        table_psam = dynamodb.Table('cumberland-psam')
    
    if len(event.keys()) == 0:
        # non-proxy response
        return dict(error=dict(code='unknown-path1', 
            message='Path must be at least two levels deep: /cumberland/xxx or /cumberland-protected/xxx'))

    if event['path'].count('/') < 2:
        # non-proxy response
        return dict(error=dict(code='unknown-path2', 
            message='Path must be at least two levels deep: /cumberland/xxx or /cumberland-protected/xxx'))
    
    req_method = event['httpMethod']
    req_query  = event['queryStringParameters']
    req_path   = (urllib.parse.unquote(event['path']).split('/'))[1:]
    
    if req_query is None:
        req_query = {}
    
    if event['body'] is not None:
        req_body = json.loads(event['body'])
    
    else:
        req_body = None

    api_type = req_path[0]
    api_function = req_path[1]
    api_arguments = req_path[2:]
    
    ip_address = event['requestContext']['identity']['sourceIp']

    print(f'cumberland({start}, {ip_address}) type={api_type}, method={req_method}, function={api_function}, arguments={api_arguments}')

    if api_type == 'cumberland':
        
        if req_method != 'GET':
            return respond.error(code='wrong-method', message='Expecting method GET for cumberland')
            
        if api_function == 'test':
            return get_test(table_psam, api_arguments, event)
        
        elif api_function == 'planning-schemes':
            return get_planning_schemes(req_query) 
        
        elif api_function == 'list':
            return function.get_list(table_psam, api_arguments)
            
        elif api_function =='flagged':
            return function.get_flagged(table_psam, req_query)
        
        #elif api_function == 'scan':
        #    return function.get_scan(table_psam, req_query)

        elif api_function == 'amendment':
            return function.get_amendment(table_psam, api_arguments)
            
        return respond.error(code='not-implemented', message='Function not implemented.')

    elif api_type == 'cumberland-protected':

        if 'apiKeyId' not in event['requestContext']['identity']:
            return respond.error(code='forbidden', message='Invalid apiKeyId in x-api-key header')
            
        if api_function == 'amendment' and req_method == 'PUT':
            return function.protected_put_amendment(table_psam, api_arguments, req_body)
                
        elif api_function == 'amendment' and req_method == 'POST':
            return function.protected_post_amendment(table_psam, api_arguments, req_query, req_body)
            
        elif api_function == 'amendment' and req_method == 'DELETE':
            return function.protected_delete_amendment(table_psam, api_arguments)
            
        elif api_function == 'presigned-urls' and req_method == 'POST':
            
            if s3_client is None:
                s3_client = boto3.client('s3', region_name='ap-southeast-2')
                
            bucket_name = 'cumberland-files'

            return function.protected_post_presigned_urls(s3_client, req_body, bucket_name)

        return respond.error(code='not-implemented', message='Function not implemented.')
        
    return respond.error(code='unknown-path3', 
        message='Path must be at least two levels deep: /cumberland/xxx or /cumberland-protected/xxx')


def get_test(table_psam, api_arguments, event):
    
    table_psam_creation = str(table_psam.creation_date_time)[:19]
    
    if len(api_arguments) == 0:
        return respond.error(code='argument-expected', message='test expects one argument, e.g. info')
        
    if api_arguments[0] == 'info':
        return respond.result(dict(
            table_psam_creation=table_psam_creation,
            trace_id=event['headers']['X-Amzn-Trace-Id'],
            source_ip=event['requestContext']['identity']['sourceIp'],
            body=event['body'],
            #headers=event['headers'],
            http_method=event['httpMethod'],
            query=event['queryStringParameters'],
            #event=event,
            ))

    elif api_arguments[0] == 'error':
        return respond.error(code='test-error', message='this is an expected error')
        
    return respond.error(code='unknown-argument', message='unknown test argument')


def get_planning_schemes(req_query):
    # query: select=full|code|name|code-list|name-list, vpp=True|False
    # Return a dict of ps code -> ps name.
    
    select = req_query.get('select')
    vpp    = req_query.get('vpp')
    
    if select is None:
        return respond.error(code='unknown-select', 
            message='unknown select, expecting select=full|code|name|code-list|name-list and optionally vpp=True|False')
    
    if select == 'full':
        return respond.result(ps_data.planning_scheme_data)

    elif select == 'code':
        ps_list = {}

        for ps_code in ps_data.planning_scheme_data['planning_schemes']:
            ps_list[ps_code] = ps_data.planning_scheme_data['planning_schemes'][ps_code]['planning_scheme']

        if vpp is not None:
            if vpp.lower().startswith('t'):
                ps_list['vpp'] = 'Victoria Planning Provisions'

        return respond.result(ps_list)

    elif select == 'name':
        ps_list = {}

        for ps_code in ps_data.planning_scheme_data['planning_schemes']:
            ps_list[ps_data.planning_scheme_data['planning_schemes'][ps_code]['planning_scheme']] = ps_code

        if vpp is not None:
            if vpp.lower().startswith('t'):
                ps_list['Victoria Planning Provisions'] = 'vpp'

        return respond.result(ps_list)

    elif select == 'code-list':
        ps_list = []

        for ps_code in ps_data.planning_scheme_data['planning_schemes']:
            ps_list.append(ps_code)

        if vpp is not None:
            if vpp.lower().startswith('t'):
                ps_list.append('vpp')

        return respond.result(ps_list)

    elif select == 'name-list':
        ps_list = []

        for ps_code in ps_data.planning_scheme_data['planning_schemes']:
            ps_list.append(ps_data.planning_scheme_data['planning_schemes'][ps_code]['planning_scheme'])

        if vpp is not None:
            if vpp.lower().startswith('t'):
                ps_list.append('Victoria Planning Provisions')

        return respond.result(sorted(ps_list))
