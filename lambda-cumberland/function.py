import datetime

import boto3
from boto3.dynamodb.conditions import Key, Attr
from botocore.exceptions import ClientError

import respond

def decode_item_from_db(item_from_db, remove_none=False):
    # missing values default to none (may be called with partial values in a projection)

    item_for_api = dict(
        ps        = item_from_db.get('ps'),
        am        = item_from_db.get('am'),
        files     = item_from_db.get('files'),
        path      = item_from_db.get('path'),
        uploaded  = item_from_db.get('uploaded') == 'T',
        published = item_from_db.get('published') == 'T',
        pub_date  = item_from_db.get('pub_date'),
        )
    
    # # am is a DynamoDB sort key that must be unique
    # if item_from_db['am'].endswith('/maps'):
    #     item_for_api['am'] = item_from_db['am'][:-5]
    #     item_for_api['map'] = True

    # else:
    #     item_for_api['am'] = item_from_db['am']
    #     item_for_api['map'] = False
    
    if remove_none:    
        for attribute in list(item_for_api.keys()):
            if item_for_api[attribute] is None:
                del item_for_api[attribute]

    return item_for_api


def encode_item_from_api(item_from_api, ps=None, am=None):
    # ValueError if ps or am are missing from item_from_api and ps / am
    
    true_false = {True: 'T', False: 'F'}
    
    item_for_db = dict(
        ps        = item_from_api.get('ps', ps),
        am        = item_from_api.get('am', am),
        files     = item_from_api['files'],
        path      = item_from_api['path'],
        uploaded  = true_false[item_from_api.get('uploaded', False)],
        published = true_false[item_from_api.get('published', False)],
        pub_date  = item_from_api.get('pub_date', 'Unpublished'),
        )
    
    # if item_from_api.get('map', True):
    #     item_for_db['am'] = item_from_api.get('am', am) + '#maps'

    # else:
    #     item_for_db['am'] = item_from_api.get('am', am)

    return item_for_db


def get_list(table_psam, api_arguments):
    # arguments = ps
    # Returns am, published, pub_date, uploaded
    
    if len(api_arguments) == 1:
        ps = api_arguments[0]
        
    else:
        return respond.error(code='missing-argument', message='Missing Planning Scheme')
    
    start_key = None
    more_keys = True
    capacity_units = 0
    items_for_api = []
    
    query = dict(
        KeyConditionExpression=Key('ps').eq(ps),
        ReturnConsumedCapacity='INDEXES',
        ProjectionExpression='ps, am, published, pub_date, uploaded, #path',
        ExpressionAttributeNames={'#path':'path'},
        )

    while more_keys:
        if start_key is not None:
            query['ExclusiveStartKey'] = start_key

        response = table_psam.query(**query)

        for item in response['Items']:
            items_for_api.append(decode_item_from_db(item, remove_none=True))

        if 'LastEvaluatedKey' in response:
            start_key = response['LastEvaluatedKey']

        else:
            more_keys = False

        consumed_capacity = response['ConsumedCapacity']
        capacity_units += consumed_capacity['CapacityUnits']
        
    print(f'items={len(items_for_api)}, capacity_units={capacity_units}')
    return respond.result(items_for_api)


def get_flagged(table_psam, req_query):
    #query: uploaded=True|False, published=True|False, count=True|False

    published = req_query.get('published')
    uploaded  = req_query.get('uploaded')
    count     = req_query.get('count')

    query = dict(
                Select='SPECIFIC_ATTRIBUTES',
                ReturnConsumedCapacity='INDEXES',
            )

    if published is not None:
        if published.lower() not in ['true', 'false']:
            return respond.error(code='invalid-published', message=f'Got {published}, expected published=True|False.')
            
        query['IndexName'] = 'published-index'
        query['KeyConditionExpression'] = Key('published').eq(published[0].upper())
        query['ProjectionExpression'] = 'ps, am, pub_date'
        
    elif uploaded is not None:
        if uploaded.lower() not in ['true', 'false']:
            return respond.error(code='invalid-uploaded', message=f'Got {uploaded}, expected uploaded=True|False.')

        query['IndexName'] = 'uploaded-index'
        query['KeyConditionExpression'] = Key('uploaded').eq(uploaded[0].upper())
        query['ProjectionExpression'] = 'ps, am'

    else:
        return respond.error(code='missing-query', message='Expected uploaded=True|False or published=True|False.')

    if count is not None:
        if count.lower().startswith('t'):
            query['Select'] = 'COUNT'
            del query['ProjectionExpression']
        
    print(f'cumberland_get_flagged published={published} uploaded={uploaded} count={count}')
        
    start_key = None
    more_keys = True
    capacity_units = 0
    item_count = 0
    items_for_api = []

    while more_keys:
        if start_key is not None:
            query['ExclusiveStartKey'] = start_key

        response = table_psam.query(**query)
        
        if count:
            item_count += response['Count']
            
        else:
            for item in response['Items']:
                items_for_api.append(item)

        if 'LastEvaluatedKey' in response:
            start_key = response['LastEvaluatedKey']

        else:
            more_keys = False

        consumed_capacity = response['ConsumedCapacity']
        capacity_units += consumed_capacity['CapacityUnits']
    
    if count:
        print(f'count={item_count}, capacity_units={capacity_units}')
        return respond.result(dict(count=item_count))
        
    else:
        print(f'items={len(items_for_api)}, capacity_units={capacity_units}')
        return respond.result(items_for_api)


# def get_scan(table_psam, req_query):
    # return respond.error(code='test', message=f'query={req_query}') 
#     print(req_query)
    
#     # if len(api_arguments) < 1:
#     #     return respond.error(code='missing-argument', message='Missing Planning Scheme')

#     # ps = api_arguments[0]
    
#     start_key = None
#     more_keys = True
#     capacity_units = 0
#     items_for_api = []
    
#     projection_expression = 'ps, am, published, pub_date, uploaded'

#     while more_keys:
#         if start_key is None:
#             response =table_psam.scan(
#                 #KeyConditionExpression=Key('ps').eq(ps),
#                 ReturnConsumedCapacity='INDEXES',
#                 ProjectionExpression=projection_expression)

#         else:
#             response = table_psam.scan(
#                 ExclusiveStartKey=start_key,
#                 #KeyConditionExpression=Key('ps').eq(ps),
#                 ReturnConsumedCapacity='INDEXES',
#                 ProjectionExpression=projection_expression)

#         for item in response['Items']:
#             items_for_api.append(decode_item_from_db(item))

#         if 'LastEvaluatedKey' in response:
#             start_key = response['LastEvaluatedKey']

#         else:
#             more_keys = False

#         consumed_capacity = response['ConsumedCapacity']
#         capacity_units += consumed_capacity['CapacityUnits']
        
#     print(f'items={len(items_for_api)}, capacity_units={capacity_units}')
#     return respond.result(items_for_api)
    

def get_amendment(table_psam, api_arguments):

    if len(api_arguments) >= 2:
        ps = api_arguments[0]
        am = api_arguments[1]

        if len(api_arguments) == 3:
            am += '/' + api_arguments[2]  # maps

    else:
        return respond.error(code='missing-arguments', message='Missing Planning Scheme or Amendment')
    
    print(f'cumberland_get_amendment {ps}/{am}')

    response = table_psam.get_item(Key=dict(ps=ps, am=am))
    
    if 'Item' in response:
        return respond.result(decode_item_from_db(response['Item']))
        
    else:
        return respond.error(code='item-not-found', message='Planning Scheme and Amendment not found')


def protected_put_amendment(table_psam, api_arguments, req_body):
    # Create a new amendment (clause and maps are distinct items)
    
    if len(api_arguments) >= 2:
        ps = api_arguments[0]
        am = api_arguments[1]

        if len(api_arguments) == 3:
            am += '/' + api_arguments[2]  # maps

    else:
        return respond.error(code='missing-arguments', message='Missing Planning Scheme or Amendment')
    
    if req_body['ps'] != ps:
        return respond.error(code='ps-mismatch', message='ps in path does not match ps in body')
    
    if req_body['am'] != am:
        return respond.error(code='am-mismatch', message='am in path does not match am in body')
    
    item = encode_item_from_api(req_body, ps=ps, am=am)
    
    if item['am'][10] != ' ':
        return respond.error(code='invalid-amendment-format', message='Amendment character 10 must be a blank')
        
    print(f'cumberland_protected_put_amendment {ps}/{am} body-keys={len(req_body)}')

    try:
        response = table_psam.put_item(
            Item=item,
            ConditionExpression='attribute_not_exists(ps) AND attribute_not_exists(am)',
            )

    except ClientError as e:
        return respond.error(code='client-error', message=repr(e))

    return respond.error(code='success')


def protected_post_amendment(table_psam, api_arguments, req_query, req_body):
    # Update published or updated
    
    if len(api_arguments) >= 2:
        ps = api_arguments[0]
        am = api_arguments[1]

        if len(api_arguments) == 3:
            am += '/' + api_arguments[2]  # maps

    else:
        return respond.error(code='missing-arguments', message='Missing Planning Scheme or Amendment')
        
    uploaded  = req_query.get('uploaded')
    published = req_query.get('published')
    path      = req_query.get('path')

    expression = ''
    values = {}
    names = {}

    # used placeholders :f :p :d :t
    
    if req_body is None:
        body_keys = 'No req_body'
        
    else:
        expression += 'files = :f'
        values[':f'] = req_body
        body_keys = len(req_body)

    if published is not None:
        if published.lower() not in ['true', 'false']:
            return respond.error(code='invalid-published', message=f'Got {published}, expected published=True|False.')
            
        if len(expression) > 0:
            expression += ', '

        expression += 'published = :p, pub_date = :d'
        values[':p'] = published[0].upper()
        values[':d'] = str(datetime.datetime.now()).split('.')[0][:-3]
            
    if uploaded is not None:
        if uploaded.lower() not in ['true', 'false']:
            return respond.error(code='invalid-uploaded', message=f'Got {uploaded}, expected uploaded=True|False.')

        if len(expression) > 0:
            expression += ', '
        
        expression += 'uploaded = :u'
        values[':u'] = uploaded[0].upper()
    
    if path is not None:
        print(f'cumberland_protected_post_amendment {ps}/{am} updating path', path)
        if len(expression) > 0:
            expression += ', '
        
        expression += '#path = :t'
        values[':t'] = path
        names['#path'] = 'path'

    if published is None and uploaded is None and path is None:
        return respond.error(code='missing-query', message='Expected uploaded=True|False or published=True|False or path=some value.')

    print(f'cumberland_protected_post_amendment {ps}/{am} published={published} uploaded={uploaded} path={path} body_keys={body_keys}')

    try:
        if path is None:
            response = table_psam.update_item(
                Key=dict(ps=ps, am=am),
                UpdateExpression='set ' + expression,
                ExpressionAttributeValues=values)

        else:
            response = table_psam.update_item(
                Key=dict(ps=ps, am=am),
                UpdateExpression='set ' + expression,
                ExpressionAttributeNames=names,
                ExpressionAttributeValues=values)

    except ClientError as e:
        return respond.error(code='client-error', message=repr(e))

    return respond.error(code='success')


def protected_delete_amendment(table_psam, api_arguments):
    # Returns success if deleted or didn't exist
    
    if len(api_arguments) >= 2:
        ps = api_arguments[0]
        am = api_arguments[1]

        if len(api_arguments) == 3:
            am += '/' + api_arguments[2]  # maps

    else:
        return respond.error(code='missing-arguments', message='Missing Planning Scheme or Amendment')

    print(f'cumberland_protected_delete_amendment {ps}/{am}')

    try:
        response = table_psam.delete_item(
            Key=dict(ps=ps, am=am),
            )

    except ClientError as e:
        return respond.error(code='client-error', message=repr(e))

    return respond.error(code='success')
    
    
def protected_post_presigned_urls(s3_client, req_body, bucket_name):
    # returns dict of zero or more hashes to upload and presigned URL's.
    # https://boto3.amazonaws.com/v1/documentation/api/latest/guide/s3-presigned-urls.html
    
    presigned_urls = {}
    
    print(f'protected_post_presigned_urls req_body={req_body}')

    for hash in req_body:

        presigned_urls[hash] = s3_client.generate_presigned_url(
            ClientMethod='put_object',
            Params=dict(
                ACL='public-read',
                Bucket=bucket_name,
                ContentType='application/pdf',
                Key=hash + '.pdf',
                ),
            ExpiresIn=3600)

    return respond.result(presigned_urls)

    