# Test Results (16/03/2020)

End-to-end normal run (for VC177).

Only representative lines are shown for long lists.

## End-to-End Normal Run

Command:

```py H:\cumberland.py scan 2020-03-01```

Result:

```
0:00:00.01 XI001 Script (v12) started at 2020-03-16 09:25 (using config file H:cumberland.config)
0:00:00.03 SI001 Processing directories since: 2020-03-01
0:00:00.15 SI004 Processing Alpine
0:00:26.81 SI012 Adding directory Alpine/2020-03-11 VC177/maps to the database (files=93)
0:03:06.96 SI012 Adding directory Alpine/2020-03-11 VC177 to the database (files=429)
0:03:08.65 SI004 Processing Alpine Resorts
0:03:29.59 SI012 Adding directory Alpine Resorts/2020-03-11 VC177/maps to the database (files=67)
0:06:16.85 SI012 Adding directory Alpine Resorts/2020-03-11 VC177 to the database (files=403)
0:06:17.94 SI004 Processing Ararat
...
0:43:04.74 SI004 Processing Campaspe
0:43:42.23 SI012 Adding directory Campaspe/2020-03-11 VC177/maps to the database (files=177)
0:46:04.03 SI012 Adding directory Campaspe/2020-03-11 VC177 to the database (files=455)
...
1:02:35.31 SI004 Processing Darebin
1:03:00.76 SI012 Adding directory Darebin/2020-03-11 VC177/maps to the database (files=142)
1:05:05.17 SI012 Adding directory Darebin/2020-03-11 VC177 to the database (files=477)
...
1:25:36.82 SI004 Processing Greater Bendigo
1:26:57.85 SI012 Adding directory Greater Bendigo/2020-03-11 VC177/maps to the database (files=366)
1:29:57.40 SI012 Adding directory Greater Bendigo/2020-03-11 VC177 to the database (files=558)
...
2:03:45.34 SI004 Processing Knox
2:04:18.59 SI012 Adding directory Knox/2020-03-11 VC177/maps to the database (files=125)
2:06:31.71 SI012 Adding directory Knox/2020-03-11 VC177 to the database (files=506)
2:06:32.45 SI006 Ignore directories that don't start with a year (Knox): Knox
...
2:29:55.23 SI004 Processing Melton
2:30:15.55 SI012 Adding directory Melton/2020-03-11 VC177/maps to the database (files=105)
2:32:22.91 SI012 Adding directory Melton/2020-03-11 VC177 to the database (files=533)
2:32:41.70 SI012 Adding directory Melton/2020-03-12 C211melt/maps to the database (files=105)
2:34:27.68 SI012 Adding directory Melton/2020-03-12 C211melt to the database (files=533)
...
3:02:22.14 SI004 Processing Nillumbik
3:02:44.85 SI012 Adding directory Nillumbik/2020-03-11 VC177/maps to the database (files=155)
3:04:54.94 SI012 Adding directory Nillumbik/2020-03-11 VC177 to the database (files=486)
...
3:32:04.43 SI004 Processing Warrnambool
3:32:17.20 SI012 Adding directory Warrnambool/2020-03-11 VC177/maps to the database (files=126)
3:33:45.64 SI012 Adding directory Warrnambool/2020-03-11 VC177 to the database (files=469)
3:33:46.23 SI006 Ignore directories that don't start with a year (Warrnambool): Current Word Local Prov
...
3:53:26.41 SI004 Processing Yarriambiack
3:53:39.31 SI012 Adding directory Yarriambiack/2020-03-11 VC177/maps to the database (files=121)
3:55:00.51 SI012 Adding directory Yarriambiack/2020-03-11 VC177 to the database (files=416)
3:55:01.09 LI001 Processing planning scheme prefix: 2020-03-01
3:55:01.22 LI002 Amendments to process: 169

3:55:01.26 XI006 Counters:
3:55:01.28 XI007 Ignored not ps            2
3:55:01.29 XI007 Scanned ps+vpp            84
3:55:01.31 XI007 Scanned source am         21,229
3:55:01.32 XI007 Ignored am before         20,889
3:55:01.34 XI007 Am processed              169
3:55:01.36 XI007 Am to process             185
3:55:01.37 XI007 Ignored not am            53
3:55:01.39 XI007 Am already processed      16
3:55:01.40 XI007 Upload ignore not selected 169
```



OPTIONAL Command which can be run at any time in a separate powershell window to see progress:

```py H:\cumberland.py stats all```

Result:

```
0:00:00.01 XI001 Script (v12) started at 2020-03-16 10:59 (using config file H:cumberland.config)
0:00:00.59 TI001 Amendments uploaded:          41,464
0:00:00.68 TI002 Amendments not uploaded:      58
0:00:00.70 TI003 Total amendments (uploaded):  41,522
0:00:01.17 TI004 Amendments published:         41,461
0:00:01.24 TI005 Amendments not published:     61
0:00:01.26 TI006 Total amendments (published): 41,522
```



Command:

```py H:\cumberland.py upload all```

Result:

```
0:00:00.01 XI001 Script (v12) started at 2020-03-16 13:23 (using config file H:\cumberland.config)
0:00:00.35 LI002 Amendments to process: 169
0:00:16.49 LI012 Uploaded file O:\Planning Scheme Histories\Alpine Resorts\2020-03-11-VC177\2020-03-11-VC177\52_07.pdf
0:00:16.72 LI012 Uploaded file O:\Planning Scheme Histories\Alpine Resorts\2020-03-11-VC177\2020-03-11-VC177\amlist_s_alpr.pdf
0:00:16.82 LI010 Alpine Resorts/2020-03-11 VC177 Uploaded 2 files.
0:00:19.71 LI011 Alpine Resorts/2020-03-11 VC177/maps No files to upload for Alpine Resorts/2020-03-11-VC177/2020-03-11-VC177/Maps.
0:00:35.01 LI012 Uploaded file O:\Planning Scheme Histories\Alpine\2020-03-11-VC177\2020-03-11-VC177\amlist_s_alpi.pdf
0:00:35.09 LI010 Alpine/2020-03-11 VC177 Uploaded 1 files.
0:00:38.50 LI011 Alpine/2020-03-11 VC177/maps No files to upload for Alpine/2020-03-11-VC177/2020-03-11-VC177/Maps.
0:00:54.83 LI012 Uploaded file O:\Planning Scheme Histories\Ararat\2020-03-11-VC177\2020-03-11-VC177\amlist_s_arat.pdf
...
0:17:57.49 LI012 Uploaded file O:\Planning Scheme Histories\Macedon Ranges\2020-03-11-VC177\2020-03-11-VC177\amlist_s_macr.pdf
0:17:57.57 LI010 Macedon Ranges/2020-03-11 VC177 Uploaded 1 files.
0:18:12.87 LI011 Macedon Ranges/2020-03-11 VC177/maps No files to upload for Macedon Ranges/2020-03-11-VC177/2020-03-11-VC177/Maps.
0:18:32.96 LI012 Uploaded file O:\Planning Scheme Histories\Manningham\2020-03-11-VC177\2020-03-11-VC177\amlist_s_mann.pdf
0:18:33.06 LI010 Manningham/2020-03-11 VC177 Uploaded 1 files.
0:18:38.70 LI011 Manningham/2020-03-11 VC177/maps No files to upload for Manningham/2020-03-11-VC177/2020-03-11-VC177/Maps.
0:18:55.18 LI012 Uploaded file O:\Planning Scheme Histories\Mansfield\2020-03-11-VC177\2020-03-11-VC177\amlist_s_mans.pdf
0:18:55.27 LI010 Mansfield/2020-03-11 VC177 Uploaded 1 files.
0:19:03.81 LI011 Mansfield/2020-03-11 VC177/maps No files to upload for Mansfield/2020-03-11-VC177/2020-03-11-VC177/Maps.
...
0:38:38.03 LI012 Uploaded file O:\Planning Scheme Histories\Yarra\2020-03-11-VC177\2020-03-11-VC177\amlist_s_yara.pdf
0:38:38.11 LI010 Yarra/2020-03-11 VC177 Uploaded 1 files.
0:38:41.59 LI011 Yarra/2020-03-11 VC177/maps No files to upload for Yarra/2020-03-11-VC177/2020-03-11-VC177/Maps.
0:39:01.38 LI012 Uploaded file O:\Planning Scheme Histories\Yarriambiack\2020-03-11-VC177\2020-03-11-VC177\amlist_s_yari.pdf
0:39:01.48 LI010 Yarriambiack/2020-03-11 VC177 Uploaded 1 files.
0:39:07.57 LI011 Yarriambiack/2020-03-11 VC177/maps No files to upload for Yarriambiack/2020-03-11-VC177/2020-03-11-VC177/Maps.

0:39:07.60 XI006 Counters:
0:39:07.62 XI007 Upload psam processed     169
0:39:07.63 XI007 Already uploaded          57,698
0:39:07.65 XI007 Upload files              89
0:39:07.66 XI007 Upload psam completed     86
0:39:07.68 XI007 No files to upload        83
```



Command:

```py H:\cumberland.py publish all```

Result:

```
0:00:00.01 XI001 Script (v12) started at 2020-03-16 14:05 (using config file H:\cumberland.config)
0:00:14.07 PI002 Uploaded amendments: 41,633
0:00:14.22 PI008 Not published amendments: 172
0:00:49.13 PW003 Source data error (empty directory) for Maroondah/2013-02-21-C82/2013-02-21-C82
0:00:50.73 PW003 Source data error (empty directory) for Indigo/2016-03-10-C65
0:00:54.80 PW003 Source data error (empty directory) for Whitehorse/2018-02-08-C275
0:00:58.90 PI004 Amendments to publish: 169
0:00:58.91 PI005 Note: y=yes, n=no, a=publish all remaining, q=exit script.
PI006 Publish Alpine Resorts/2020-03-11 VC177 (y/n/a/q)? a
0:02:02.38 PI008 Alpine Resorts/2020-03-11 VC177 published.
0:02:02.47 PI008 Alpine Resorts/2020-03-11 VC177/maps published.
0:02:02.57 PI008 Alpine/2020-03-11 VC177 published.
0:02:02.65 PI008 Alpine/2020-03-11 VC177/maps published.
0:02:02.74 PI008 Ararat/2020-03-11 VC177 published.
0:02:02.83 PI008 Ararat/2020-03-11 VC177/maps published.
...
0:02:23.87 PI008 Yarra Ranges/2020-03-11 VC177 published.
0:02:23.95 PI008 Yarra Ranges/2020-03-11 VC177/maps published.
0:02:24.01 PI008 Yarra/2020-03-11 VC177 published.
0:02:24.09 PI008 Yarra/2020-03-11 VC177/maps published.
0:02:24.19 PI008 Yarriambiack/2020-03-11 VC177 published.
0:02:24.28 PI008 Yarriambiack/2020-03-11 VC177/maps published.

0:02:24.33 XI006 Counters:
0:02:24.34 XI007 Publish source data error 3
0:02:24.36 XI007 Published                 169
```
