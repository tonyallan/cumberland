#! python3

# analyse_map.py
# This code has been lifted from the interim-psh script with minimal changes
# D:\archive\psh-interim-saved-20200128\s3-psh-interim.py

import re


def map(mapname, planning_scheme):
    result = process_mapname(mapname, planning_scheme)
    
    map_number = str(result['map_number'])
    
    if 'map_type_code' in result:
        map_name = map_number + '-' + result['map_type_code']
        
    else:
        map_name = map_number
        
    map_type = result['map_type']
    
    if len(map_name) == 0:
        if map_type == 'Index':
            map_name = 'index'
    
    return dict(map=map_name, description=map_type)


def process_mapname(mapname, planning_scheme):
    """
    Doesn't use vpp_clause_index or dir_name.

    Process filenames in the maps directories:
    
        planning-scheme map-number MAP_SUFFIX MAP_PARTS_AND_SCHEDULES
        planning-scheme index
        planning-scheme index sheet sheet-number
    
        Examples:
            alpine01ho.pdf
            - planning scheme: alpine (full name, lowercase, no spaces)
            - map number:      01 (01-999)
            - map of:          ho -> Heritage Overlay
            alpine index.pdf
            wellington194eso3.pdf
            wellington index sheet 1.pdf
            morningtonpeninsula01eso24_27.pdf
            - map of:          ho -> Environmental Significance Overlay 
                                     schedules 24 to 27
            morningtonpeninsula03zn.pdf
            - map of:          zn -> Zones (there is always a 'zn' file).
            melbourne08ddo2&14.pdf
            melbourne08ddo2_14.pdf
            melbourne08ddo2_14_62.pdf         
            - map 08 design and development overlay schedules 2 and 14.
            melbourne08ddo1.pdf
            - map 08 design and development overlay schedule 1
            melbourne08ddopt8.pdf
            - map 08 design and development overlay part 8 (contains a number 
              of schedules)

        MAP_SUFFIX = 'ZN', 'AEO', 'CLPO'
        MAP_PARTS_AND_SCHEDULES = 'PT1', 'PT2', 'PT3'
            value starts with either 'Part' or 'Schedule'.
            
        Return a dict of results.
            error             = None (OK) or a processing error message
            map               = 'map' or 'index' or 'index_sheet'
            map_index_sheet   = the sheet number
            map_number        = the number of this map (01-999)
            map_type          = last bit of the name using the lookup table
            map_type_code     + above before the lookup
            s3_name           = object name to be used in Amazon S3
    """
    #   MAP SUFFIX/MAP    ZONES/OVERLAY
    MAP_SUFFIX = {
        'ZN':       'Zones',
        'ZONE':     'Zones',                                # Ballarat - 'map31-zone'
        'AEO':      'Airport Environs',
        'BMO':      'Bushfire Management',        
        'CLPO':     'City Link Project',
        'DCPO':     'Development Contributions Plan',
        'DDO':      'Design and Development',
        'DPO':      'Development Plan',
        'EAO':      'Environmental Audit',
        'EMO':      'Erosion Management',
        'ESO':      'Environmental Significance',
        'FO':       'Floodway',
        'HO':       'Heritage',
        'ICO':      'Infrastructure Contributions',         
        'ICPO':     'Infrastructure Contributions Plan',       
        'IPO':      'Incorporated Plan',
        'LSIO':     'Land Subject to Inundation',
        'LSIO-FO':  'Land Subject to Inundation/Floodway',
        'LSIO-RFO': 'Land Subject to Inundation/Rural Floodway',
        'MAEO':     'Melbourne Airport Environs',
        'NCO':      'Neighbourhood Character',
        'PAO':      'Public Acquistion',
        'PDO':      'Development Plan',  # allow for error in 'Greater Shepparton/1999-07-29-NFPS/PDF Maps/greatershepparton08pdo'
        'PO':       'Parking',       
        'RFO':      'Rural Floodway',
        'RO':       'Restructure',
        'RXO':      'Road Closure',
        'SBO':      'Special Building',
        'SCO':      'Specific Control',        
        'SLO':      'Significant Landscape',
        'SMO':      'Salinity Management',
        'SRO':      'State Resource',
        'VPO':      'Vegetation Protection',
        'WMO':      'Wildfire Management',
        'M':        '',                                     # for 'alpr'
        'SM':       ''                                      # for 'alpr'
    }

    MAP_PARTS_AND_SCHEDULES = [
        ('PT1',     'Part 1',                       'part-1'),
        ('PT2',     'Part 2',                       'part-2'),
        ('PT3',     'Part 3',                       'part-3'),
        ('PT4',     'Part 4',                       'part-4'),
        ('PT5',     'Part 5',                       'part-5'),
        ('PT6',     'Part 6',                       'part-6'),
        ('PT7',     'Part 7',                       'part-7'),
        ('PT8',     'Part 8',                       'part-8'),
        ('PT9',     'Part 9',                       'part-9'),          # added for completeness
        ('1_2',     'Schedules 1 and 2',            'schedule-1_2'),    # allow for morningtonpeninsula12slo1_2.pdf
        ('1_16',    'Schedules 1 through 16',       'schedule-1_16'),   # ordered to prevent triggering 16, etc.
        ('17_19',   'Schedules 17 through 19',      'schedule-17_19'),
        ('20_23',   'Schedules 20 through 23',      'schedule-20_23'),
        ('24_27',   'Schedules 24 through 27',      'schedule-24_27'),
        ('2_14_62', 'Schedules 2, 14 and 62',       'schedule-2_14_62'),   
        ('2_14',    'Schedules 2 and 14',           'schedule-2_14'),       
        ('2&14',    'Schedules 2 and 14',           'schedule-2_14'),
        ('3_5',     'Schedules 3 through 5',        'schedule-3_5'),
        ('10',      'Schedule 10',                  'schedule-10'),
        ('11',      'Schedule 11',                  'schedule-11'),
        ('12',      'Schedule 12',                  'schedule-12'),
        ('13',      'Schedule 13',                  'schedule-13'),
        ('14',      'Schedule 14',                  'schedule-14'),
        ('15',      'Schedule 15',                  'schedule-15'),
        ('16',      'Schedule 16',                  'schedule-16'),
        ('17',      'Schedule 17',                  'schedule-17'),
        ('18',      'Schedule 18',                  'schedule-18'),
        ('19',      'Schedule 19',                  'schedule-19'),
        ('20',      'Schedule 20',                  'schedule-20'),
        ('21',      'Schedule 21',                  'schedule-21'),
        ('22',      'Schedule 22',                  'schedule-22'),
        ('23',      'Schedule 23',                  'schedule-23'),
        ('24',      'Schedule 24',                  'schedule-24'),
        ('25',      'Schedule 25',                  'schedule-25'),
        ('26',      'Schedule 26',                  'schedule-26'),
        ('27',      'Schedule 27',                  'schedule-27'),
        ('28',      'Schedule 28',                  'schedule-28'),
        ('29',      'Schedule 29',                  'schedule-29'),
        ('30',      'Schedule 30',                  'schedule-30'),
        ('1',       'Schedule 1',                   'schedule-1'),      # ordered to prevent triggering instead of 10-19
        ('2',       'Schedule 2',                   'schedule-2'),
        ('3',       'Schedule 3',                   'schedule-3'),
        ('4',       'Schedule 4',                   'schedule-4'),
        ('5',       'Schedule 5',                   'schedule-5'),
        ('6',       'Schedule 6',                   'schedule-6'),
        ('7',       'Schedule 7',                   'schedule-7'),
        ('8',       'Schedule 8',                   'schedule-8'),
        ('9',       'Schedule 9',                   'schedule-9'),
        ]

    mapname = str(mapname)
    result = {}
    result['map_number'] = ''
    result['map_type'] = ''
    result['map_part'] =  ''
    result['map_schedule'] = ''
    result['file_type'] = 'pdf'
    # '.pdf' and any stray whitespace are also removed.
    original_mapname = mapname[:-4].strip()
    mapname = mapname[:-4].strip().lower()
    result['map_name'] = mapname

    #    alpine01ho
    #    alpine index
    #    wellington194eso3
    #    wellington index sheet 1
    #    morningtonpeninsula01eso24_27
    #    morningtonpeninsula25eso17_19.pdf
    #    Swan Hill/2004-06-11-VC24/Maps/swanhill06a-eso.pdf
    #    alprFALLSM1.pdf
    #    35_06s_alpi
    #    alpine resorts18 zn
    #    alpineresortsmtbuller&stirling03
    
    # odd exceptions
    # The old Alpine resorts format
    if 'alpr' in mapname:
        m = re.search('^alpr(BAW|BUL|FALL|HOT|LAKE)(.+)$', mapname, re.IGNORECASE)
        result['map_number'] = '%s-%s' % (m.group(1), m.group(2))
        result['map'] = 'map'
        #result['s3_name'] = 'map_%s.pdf' % result['map_number']
        return result
    
    if 'guide' in mapname:
        result['map'] = 'index'
        result['map_type'] = 'User Guide'
        #result['s3_name'] = 'user_guide.pdf'
        return result
    
    if mapname == 'centralgoldfields_ug':
        result['map'] = 'index'
        result['map_type'] = 'Index'
        #result['s3_name'] = 'index.pdf'
        return result
    
    # # in Stonnington/2019-12-03-VC165/2019-12-03-VC165/Maps
    # if mapname == 'stonnington index':
    #     result['map'] = 'index'
    #     result['map_type'] = 'Index'
    #     return result
    
    # 'Indigo/1999-01-28-NFPS/PDF Maps/indigouser'
    if mapname == 'indigouser':
        result['map'] = 'index'
        result['map_type'] = 'Index'
        #result['s3_name'] = 'index.pdf'
        return result
    
    if 'alpineresort' in mapname:
        m = re.search('^alpineresort[s]{0,1}([a-z\&]*)(\d{1,3})$', mapname, re.IGNORECASE)
        if m is not None:
            result['map_number'] = m.group(2)
            result['map'] = 'map'
            result['map_type'] = m.group(1)
            #result['s3_name'] = '%s.pdf' % mapname.replace('&', '_').replace(' ', '_')
            return result
        
    # southerngrampians33
    if 'southerngrampians' in mapname:
        m = re.search('^southerngrampians(\d{1,3})$', mapname, re.IGNORECASE)
        if m is not None:
            result['map_number'] = m.group(1)
            result['map'] = 'map'
            #result['s3_name'] = '%s.pdf' % m.group(1)
            return result

    # data errors
    if mapname in ['basscoast54eso', 'basscoast54isio', 'basscoast54slo']:
        #result['error'] = 'Map content does not match filename %s' % mapname
        result['map'] = 'unknown'
        #result['s3_name'] = 'map_%s.pdf' % mapname.replace('&', '_').replace(' ', '_')
        return result

    # 'Latrobe/2000-03-02-NFPS/PDF Maps/latrobeschedule02'
    # 'Latrobe/2000-03-02-NFPS/PDF Maps/latrobefacesheet'
    # 'Latrobe/2000-03-02-NFPS/PDF Maps/latrobelegend'
    # 'Latrobe/2000-03-02-NFPS/PDF Maps/latrobeschedule01'
    # 'Latrobe/2000-03-02-NFPS/PDF Maps/latrobeschedule03'
    if mapname in ['latrobefacesheet', 'latrobelegend']:
        #result['error'] = 'Map content does not match filename %s' % mapname
        result['map'] = 'unknown'
        #result['s3_name'] = 'map_%s.pdf' % mapname.replace('&', '_').replace(' ', '_')
        return result

    # 24/09/2019 - These maps are duplicates of latrobe index sheet1
    # Unknown map format for map filename: latrobe_index_sheet1 (Latrobe/2013-02-18-VC81/Maps)
    if mapname in ['latrobe_index_sheet1', 'latrobe_index_sheet2']:
        msg = f'Duplicate map filename: {mapname}'
        print('[analyse_map.py]', msg)
        result['error'] = str(msg)
        result['map'] = 'unknown'
        return result


    # 24/09/2019
    # Unknown map format for map filename: la trobe schedule01 (Latrobe/2004-06-10-C10/Maps) -- 01/02/03
    if 'la trobe schedule' in mapname:
        m = re.search('^la trobe schedule(\d{2})$', mapname, re.IGNORECASE)
        if m is not None:
            result['map']           = 'schedule'
            result['map_type']      = 'Schedule ' + str(m.group(1))     # map_text
            result['map_type_code'] = 'Schedule ' + str(m.group(1))     # map_type
            result['map_number']    = 'schedule-"%02d"' + m.group(1)    # map_id '000-%s
            return result

    # 24/09/2019
    if 'latrobeschedule' in mapname:
        m = re.search('^latrobeschedule(\d{2})$', mapname, re.IGNORECASE)
        if m is not None:
            result['map']           = 'schedule'
            result['map_type']      = 'Schedule ' + str(m.group(1))     # map_text
            result['map_type_code'] = 'Schedule ' + str(m.group(1))     # map_type
            result['map_number']    = 'schedule-"%02d"' + m.group(1)    # map_id '000-%s
            return result


    # really odd standards
    if original_mapname.startswith('Wellington'):

        if original_mapname.startswith('WellingtonSchedule to'):
            result['map'] = 'map'
            result['map_type'] = original_mapname[len('Wellington'):].replace('&', '&amp;')
            result['map_number'] = mapname[len('WellingtonSchedule to '):].replace('&', '&amp;')
            result['map_type_code'] = ''

        elif 'Index Sheet' in original_mapname:
            result['map'] = 'index_sheet'
            result['map_type'] = original_mapname[len('Wellington'):].replace('&', '&amp;')
            result['map_number'] = mapname[len('WellingtonIndex Sheet '):].replace('&', '&amp;')

        else:
            result['map'] = 'map'
            map_code = mapname[len('Wellington'):].replace('&', '&amp;')
            result['map_type'] = map_code
            result['map_type_code'] = map_code
            result['map_type_code'] = ''

            m = re.search('^(.+)\s([a-z\-]{2,8})(.*)$', map_code, re.IGNORECASE)

            if m is not None:
                map_type_code = m.group(2).upper()
                if map_type_code in MAP_SUFFIX:
                    result['map_type'] = MAP_SUFFIX[map_type_code]
                result['map_type_code'] = map_type_code
                result['map_number'] = m.group(1)

            else:
                result['map_number'] = map_code

        #result['s3_name'] = 'map_%s.pdf' % mapname.replace('&', '_').replace(' ', '_')
        return result

    #if planning_scheme == 'Yarra Ranges':
    #    result['map'] = 'unknown'
    #    result['s3_name'] = 'map_%s.pdf' % mapname.replace('&', '_').replace(' ', '_')
    #    return result        

    # http://127.0.0.1/data/Planning Scheme Histories/Yarra Ranges/2000-10-19-C1/Maps/emo1.pdf
    if planning_scheme == 'Yarra Ranges':
        if 'yarraranges' not in mapname:

            m = re.search('^(.+?)(\d{1,3})$', mapname, re.IGNORECASE)

            if m is not None:
                mapname = 'yarraranges%s%s' % (m.group(2), m.group(1))
            mapname = mapname.replace('zones', 'zone')

    # misc fixups
    # 'Moreland/2000-05-04-NFPS/PDF Maps/moreland13eso.'
    mapname = mapname.replace('moreland13eso.', 'moreland13eso')

    ## South Gippsland/2001-03-29-VC11/Maps/PDF Maps/southgippsland01eso5.pdf
    #mapname = mapname.replace('southgippsland', 'south_gippsland')

    # Swan Hill/2000-08-24-C2/Maps/Swan Hill08l sio.pdf
    mapname = mapname.replace('swan hill', 'swanhill')

    # Swan Hill/2000-08-24-C2/Maps/Swan Hill08l sio.pdf
    mapname = mapname.replace('swanhill08l sio', 'swanhill08lsio')

    # Swan Hill/2000-12-14-VC10/Maps/Swan Hill26 sio.pdf
    mapname = mapname.replace('swanhill26l sio', 'swanhill26lsio')

    # Swan Hill/1999-09-09-NFPS/PDF Maps/swanhill26sio
    mapname = mapname.replace('swanhill26sio', 'swanhill26lsio')

    # 26/09/2019 -- No suffix (SIO) found in the map: swanhill26 sio
    mapname = mapname.replace('swanhill26 sio', 'swanhill26lsio')
    
    # Melbourne/2002-05-16-C63/MAPS/MELBOURNE13 DDO PT7.pdf
    mapname = mapname.replace('melbourne13 ddo pt7', 'melbourne13 ddo7')
    
    if 'swanhill' in mapname:
        m = re.search('^[a-z]+(\d{1,3}[a-z]{1})\-\s*(.+)$', mapname, re.IGNORECASE)
        if m is None:
            m = re.search('^[a-z ]+(\d{1,3})\s*(.+)$', mapname, re.IGNORECASE)

    elif 'frenchisland' in mapname:
        m = re.search('^frenchisland(\d{1,3})(.+)$', mapname, re.IGNORECASE)

        if m is None:
            m = re.search('^frenchisland index$', mapname, re.IGNORECASE)

            if m is not None:
                result['map'] = 'index'
                result['map_type'] = 'Index'
                #result['s3_name'] = 'index.pdf'
                return result

        if m is None:
            m = re.search('^(frenchisland)(.+)$', mapname, re.IGNORECASE)

    # latrobe22c4sro1
    elif 'latrobe' in mapname:

        if mapname == 'latrobe index sheet':
            result['map'] = 'index_sheet'
            result['map_type'] = 'Index sheet'
            result['map_index_sheet'] = ''
            #result['s3_name'] = 'index-sheet.pdf'
            return result

        m = re.search('^latrobe(\d{0,3}[a-z]{0,1}\d{1,2})(.+)$', mapname, re.IGNORECASE)

        if m is None:
            m = re.search('^latrobe index(\d{1,2})$', mapname, re.IGNORECASE)

            if m is not None:
                result['map'] = 'index_sheet'
                result['map_type'] = 'Index sheet %s' % m.group(1)
                result['map_index_sheet'] = int(m.group(1))
                #result['s3_name'] = 'index-sheet-%s.pdf' % m.group(1)
                return result

            else:
                m = re.search('^latrobe index sheet(\d{1,2})$', mapname, re.IGNORECASE)
                if m is not None:
                    result['map'] = 'index_sheet'
                    result['map_type'] = 'Index sheet %s' % m.group(1)
                    result['map_index_sheet'] = int(m.group(1))
                    #result['s3_name'] = 'index-sheet-%s.pdf' % m.group(1)
                    return result

    elif 'la trobe' in mapname:
        m = re.search('^la trobe(\d{0,3}[a-z]{0,1}\d{1,2})(.+)$', mapname, re.IGNORECASE)

        if m is None:
            m = re.search('^la trobe index(\d{1,2})$', mapname, re.IGNORECASE)

            if m is not None:
                result['map'] = 'index_sheet'
                result['map_type'] = 'Index sheet %s' % m.group(1)
                result['map_index_sheet'] = int(m.group(1))
                #result['s3_name'] = 'index-sheet-%s.pdf' % m.group(1)
                return result

            else:
                m = re.search('^la trobe index sheet(\d{1,2})$', mapname, re.IGNORECASE)
                if m is not None:
                    result['map'] = 'index_sheet'
                    result['map_type'] = 'Index sheet %s' % m.group(1)
                    result['map_index_sheet'] = int(m.group(1))
                    #result['s3_name'] = 'index-sheet-%s.pdf' % m.group(1)
                    return result

    elif mapname.endswith('.ipg'):
        # Glenelg/2001-09-27-VC13/Maps map25-eso.ipg.pdf
        m = re.search('map(\d{1,3})\-(.+)\.ipg$', mapname, re.IGNORECASE)

    elif 'swanhill' in mapname:
        # Swan Hill/2001-03-22-C1/Maps/Swan Hill26A ho.pdf
        m = re.search('^swanhill([0-9A]{1,3})[\s\-]{0,1}(.+)$', mapname, re.IGNORECASE)

    else:
        m = re.search('^[a-z\s]+(\d{1,3})[\s\-]{0,1}(.+)$', mapname, re.IGNORECASE)

    if m is None:
        m = re.search('^.+index\-{0,1} sheet\s*(\d{1,2})$', mapname, re.IGNORECASE)

        if m is not None:
            result['map_number'] = ' '
            result['map'] = 'index_sheet'
            result['map_type'] = 'Index sheet %s' % m.group(1)
            result['map_index_sheet'] = int(m.group(1))

        elif mapname.endswith('index'):
            result['map'] = 'index'
            result['map_type'] = 'Index'

        else:
            msg = f'Unknown map format for map filename: {mapname}'
            print('[analyse_map.py]', msg)
            result['error'] = str(msg)

    else:
        if len(m.groups()) > 0:
            try:
                result['map_number'] = int(m.group(1))
            except:
                result['map_number'] = m.group(1)

        if len(m.groups()) > 1:
            last_bit = m.group(2).strip()
        else:
            last_bit = ''

        #    ho
        #    eso3
        #    eso24_27
            
        # MAP_PARTS_AND_SCHEDULES = 'PT1', 'PT2', 'PT3'
        # e.g. ('1','Schedule 1', 'schedule-1')
        # substitute from the table
        # value starts with either 'Part' or 'Schedule'. 
        #s3_part = None
        for key, text, s3 in MAP_PARTS_AND_SCHEDULES:

            if last_bit.upper().endswith(key):
                part_or_schedule = text

                if part_or_schedule.startswith('Part'):
                    result['map_part'] = part_or_schedule
                elif part_or_schedule.startswith('Schedule'):
                    result['map_schedule'] = part_or_schedule
                #s3_part = s3
                last_bit = str(last_bit[:-len(key)])

                break
        
        # 24/09/2019 Map SIO to LSIO...
        # No suffix (SIO-FO) found in the map: latrobe116sio-fo (Latrobe/2012-03-29-C70/Maps) 116 to 121
        # No suffix (SIO-FO) found in the map: colacotway15sio-fo
        if last_bit.upper() == 'SIO':
            last_bit = 'LSIO'

        if last_bit.upper() == 'SIO-FO':
            last_bit = 'LSIO-FO'

        if last_bit.upper() == 'SIO-RFO':
            last_bit = 'LSIO-RFO'

        # 26/09/2019
        # No suffix (A LSIO) found in the map: swanhill26a lsio
        # No suffix (A ZN) found in the map: swanhill26a zn
        if 'swanhill' in mapname:
            if last_bit.upper().startswith('A '):
                last_bit = last_bit[2:]

        #    ho
        #    eso
        # MAP_SUFFIX = 'ZN', 'AEO', 'CLPO'
        # substitute from the table
        if last_bit.upper() in MAP_SUFFIX:
            result['map_type'] = MAP_SUFFIX[last_bit.upper()]
            result['map_type_code'] = last_bit

        elif last_bit.upper()[1:] in MAP_SUFFIX:
            # handle bawbaw15aslo
            result['map_type'] = MAP_SUFFIX[last_bit.upper()[1:]]
            result['map_type_code'] = last_bit[1:]
            result['map_number'] = str(result['map_number']) + last_bit[:1]

        else:
            msg = "No suffix (%s) found in the map: %s" % \
                (last_bit.upper(), mapname)
            print('[analyse_map.py]', msg)
            result['error'] = msg
            
        # and finally note that we just processed a map
        result['map'] = 'map'     
    
    if 'error' not in result:
        #if result['map'] == 'map':
            # if s3_part is None:
            #     result['s3_name'] = 'map_%s_%s.pdf' % \
            #         (str(result['map_number']), result['map_type_code'])
            # else:
            #     result['s3_name'] = 'map_%s_%s_%s.pdf' % \
            #         (str(result['map_number']), result['map_type_code'], s3_part)
        if result['map'] == 'index_sheet':
            result['map_type'] = 'Index sheet %s' % result['map_index_sheet']
            #result['s3_name'] = 'index-sheet_%s.pdf' % result['map_index_sheet']

        elif result['map'] == 'index':
            result['map_type'] = 'Index'
            #result['s3_name'] = 'index.pdf'

    return result

if __name__ == '__main__':
    #for name in test:
    #print('[analyse_map.py]', process_filename_original('11_03-006S.pdf', clause_data.post_vc148_clause_data, '2020-02-11'))

    # result = {'clause': '11.03-6S', 'clause_id': '11.03-6S', 'clause_type': 'Regional and local places', 
    # 'file_type': 'pdf', 'filename': '11_03-006S', 'diag_ns': '1', 'diag-key': 'cnn3s', 'schedule': True, 'schedule_num': '006'}

    test_items = [
    # Alpine C10 (2003-06-05) Maps
        ['alpine01ho.pdf'],
        ['alpine01lsio.pdf'],
        ['alpine index.pdf'],
        ['alpine02ho.pdf'],
        ['alpine01zn.pdf'],
        ['alpine02lsio.pdf'],
        ['alpine02zn.pdf'],
    
        # Alpine VC168 (2020-02-11) Maps
        ['Alpine19bmo.PDF'],
        ['alpine19dpo.pdf'],
        ['alpine19ho.pdf'],
        ['alpine19slo.pdf'],
        ['alpine19zn.pdf'],
        ['Alpine20bmo.PDF'],
        ]

    for item in test_items:
        # only tests for planning scheme = Yarra Ranges
        planning_scheme = ''

        result = process_mapname(item[0], planning_scheme)

        print('[analyse_map.py]', item[0], result['map_number'], result['map_type'])