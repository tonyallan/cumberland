# respond.py
import datetime

def wrap(result, title, not_published=''):
    date_generated = str(datetime.datetime.now()).split('.')[0][:-3] # UTC

    return f"""
<!DOCTYPE html>
<html>
<head><title>Cumberland {title}</title></head>
<body>
<h1>{title}</h1>
{not_published}

<!-- BEGIN CONTENT -->
<!-- generated ({date_generated} UTC) by Lambda function cumberland-html -->
{result}
<!-- END CONTENT -->

</body>
</html>
    """


def result(result=None, title='Cumberland', not_published=''):
    
    print(f'[respond.py] result length={len(result)}')

    return dict(
        statusCode=200,
        body=wrap(result, title, not_published),
        headers={'content-type': 'text/html; charset=utf-8'},
    )


def error(code=None, message=None, status_code=200, title='Cumberland Error', type='Content Generation Error'):
    
    print(f'[respond.py] HTML {type} ({code}) {message}')

    return dict(
        statusCode=status_code,
        body=wrap(f'<h1>{type}</h1><p>Code: {code}</p><p>{message}</p>', title),
        headers={'content-type': 'text/html; charset=utf-8'},
    )
    