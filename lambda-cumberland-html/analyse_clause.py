#! python3

# This code has been lifted from the interim-psh script with minimal changes
# D:\archive\psh-interim-saved-20200128\s3-psh-interim.py

import re
import clause_data


def clause(filename, amendment_date):
    result = process_filename_original(filename, clause_data.post_vc148_clause_data,amendment_date)
    
    return dict(clause=result['clause'], description=result['clause_type'])


def process_filename_original(filename, post_vc148_clause_data, amendment_date_to_test):
    """
    Notes:
        Amendment VC148 - reforms to the Victoria Planning Provisions
        https://www.planning.vic.gov.au/schemes-and-amendments/Amendment-VC148-reforms

        Changes to the Victoria Planning Provisions by VC148
        https://www.planning.vic.gov.au/__data/assets/pdf_file/0012/330204/VPP-Structure-Post-VC148-Amendment.pdf

        amendment_date_to_test is used to see if the file is pre or post VC148.

    Note: the arguments 'planning_scheme' and 'vpp_clause_index' (now post_vc148_clause_data) are not used but  
    retained to ensure the same signature as process_mapname().
    
    Process the filename (lower case) and extract:
        
        - clause
            30-39 are Zones
                Extractive Industry - e.g. "37_01s01EI_hume.pdf"
            40-49 are Overlays
            50-59 are Particular Provisions
            60-69 are General Provisions
            70-79 are Definitions
            80 and 81 are Incorporated Documents++
            90-95 are VicSmart            
            e.g. "52_06.pdf" is "Clause 52.06 CAR PARKING"
            e.g. "52_28_5s_arat.pdf" is "SCHEDULE TO CLAUSE 52.28-5"
            
            If the suffix is "s_arat" (e.g. 52_06s_arat.pdf) a schedule.
            This might also be "S01_arat".
            
        - type (can be nothing)
            - cover
            - objectives
            - preliminary
            - purposes
            - contents (for local suufix is _arat. Can ignore.)
            - ug (user guide)
            - sppf (state planning policy framework)
            - lppf (Local Planning Policy Framework) - always clause 22.
            - mssnn_arat e.g. mss00_arat municipal strategic statement
            - lpp00_arat local planning policy
            - nn_mm (clause is nn.mm) or nn_mm_o (clause nn.mm-o)
            - nn_mmsxx - schedule 1 to clause 37.01   
                e.g. "37_01s01_arat.pdf" is "SCHEDULE 1 TO THE SPECIAL USE ZONE"
            
        amlist.pdf = coversheet
        amlist_s_arat.pdf = list of amendments for the scheme arat.
             Can ignore the arat because we already know which scheme
             we are processing.
             
    Result contains:
        filename        - name of the file being processed.
        file_type       - pdf|doc.
        duplicate       - None or full filename of original.
        error           - if there was an error processing this file
        diag_key        - pattern key used for this file
        diag_ns         - the non-standard name log (1) or OK (0)
        clause          - clause. One of cc, cc.cc, cc.cc (schedule),
                          cc.cc (schedule ss), or ss.ss-cc (schedule)
        clause_range    - True = a range of clauses has been specified.
        clause_part     - Part nn of clause.
        schedule        - True if file is a schedule
        schedule_num    - a string of the schedule e.g. '03' or '01a'
        schedule_range  - True = a range of schedules
        clause_type     - clause group.
    """
    # Extracted from the source of http://www.dse.vic.gov.au/planningschemes/VPPs/
    # on 09/02/2011
    CLAUSE_TEXT = {
        '_cover': 'Cover',        
        '00': 'Purpose and Vision',
        '01': 'Purpose of this Planning Scheme',
        '11': 'Settlement',
        '11.01': 'Victoria',        
        '11.02': 'Managing Growth',        
        '11.03': 'Planning for Places',        
        '12': 'Environmental and Landscape Values',
        '12.00': 'Environmental and Landscape Values',
        '12.01': 'Biodiversity',
        '12.02': 'Coastal Areas',
        '12.03': 'Water Bodies and Wetlands',
        '12.04': 'Alpine Areas',
        '12.05': 'Significant Environments and Landscapes',                
        '13': 'Environmental Risks and Amenity',
        '13.00': 'Environmental Risks',
        '13.01': 'Climate change impacts',        
        '13.02': 'Bushfire',        
        '13.03': 'Floodplains',        
        '13.04': 'Soil Degradation',        
        '13.05': 'Noise', 
        '13.06': 'Air Quality',
        '13.07': 'Amenity',                               
        '14': 'Natural Resource Management',
        '14.01': 'Agriculture - Protection of Agricultural Land',        
        '14.02': 'Water',        
        '14.03': 'Earth and Energy Resources',        
        '15': 'Built Environment and Heritage',
        '15.00': 'Built Environment and Heritage',        
        '15.01': 'Urban Environment - Urban Design',         
        '15.02': 'Sustainable Development',         
        '15.03': 'Heritage',         
        '16': 'Housing',
        '16.01': 'Residential Development - Integrated Housing',        
        '17': 'Economic Development',
        '17.01': 'Employment - Diversified Economy',        
        '17.02': 'Commercial - Business',        
        '17.03': 'Industry - Industrial Land Supply',
        '17.04': 'Tourism - Facilitating Tourism',                
        '18': 'Transport',
        '18.01': 'Integrated transport - Land Use and Transport Planning',        
        '18.02': 'Movement Networks',       
        '18.03': 'Ports',        
        '18.04': 'Airports',        
        '18.05': 'Freight',        
        '19': 'Infrastructure',
        '19.01': 'Energy',        
        '19.02': 'Community infrastructure',        
        '19.03': 'Development infrastructure',        
        '20': 'Local Planning Policy Framework',
        '21': 'Local Planning Policy Framework',
        '21.01': 'Municipal Strategic Statement',
        '21.02': 'Municipal Strategic Statement',
        '21.03': 'Municipal Strategic Statement',
        '21.04': 'Municipal Strategic Statement',
        '21.05': 'Municipal Strategic Statement',
        '21.06': 'Municipal Strategic Statement',        
        '21.07': 'Municipal Strategic Statement',        
        '21.08': 'Municipal Strategic Statement',        
        '21.09': 'Municipal Strategic Statement',        
        '21.10': 'Municipal Strategic Statement',        
        '21.11': 'Municipal Strategic Statement',        
        '21.12': 'Municipal Strategic Statement',
        '21.13': 'Municipal Strategic Statement',        
        '21.14': 'Municipal Strategic Statement',        
        '21.15': 'Municipal Strategic Statement',
        '21.16': 'Municipal Strategic Statement',        
        '21.17': 'Municipal Strategic Statement',                                                        
        '22': 'Local Planning Policies',
        '22.01': 'Local Planning Policies',
        '23.00': 'Operation of the Local Planning Policy Framework (Transitional)', 
        '23.01': 'Relationship to the Planning Policy Framework',
        '23.02': 'Operation of the Municipal Strategic Statement',        
        '23.03': 'Operation of the Local Planning Policies',                       
        '30': 'Zones (face sheet)',
        '31': '[No content/Zones]',
        '32': 'Residential Zones (face sheet)',
        '32.03': 'Low Density Residential Zone (LDRZ)',
        '32.04': 'Mixed Use Zone (MUZ)',
        '32.05': 'Township Zone (TZ)',
        '32.06': 'Residential 3 Zone (R3Z)',
        '32.07': 'Residential Growth Zone (RGZ)',
        '32.08': 'General Residential Zone (GRZ)',
        '32.09': 'Neighbourhood Residential Zone (NRZ)',                        
        '33': 'Industrial Zones (face sheet)',
        '33.01': 'Industrial 1 Zone (IN1Z)',
        '33.02': 'Industrial 2 Zone (IN2Z)',
        '33.03': 'Industrial 3 Zone (IN3Z)',
        '34': 'Commercial Zones (face sheet)',
        '34.01': 'Commercial 1 Zone (C1Z)',
        '34.02': 'Commercial 2 Zone (C2Z)',
        '35': 'Rural Zones (face sheet)',
        '35.03': 'Rural Living Zone (RLZ)',
        '35.04': 'Green Wedge Zone (GWZ)',
        '35.05': 'Green Wedge A Zone (GWAZ)',
        '35.06': 'Rural Conservation Zone (RCZ)',
        '35.07': 'Farming Zone (FZ)',
        '35.08': 'Rural Activity Zone (RAZ)',
        '36': 'Public Land Zones (face sheet)',
        '36.01': 'Public Use Zone (PUZ)',
        '36.02': 'Public Park and Recreation Zone (PPRZ)',
        '36.03': 'Public Conservation and Resource Zone (PCRZ)',
        '36.04': 'Road Zone (RDZ)',
        '37': 'Special Purpose Zones (face sheet)',
        '37.01': 'Special Use Zone (SUZ)',
        '37.02': 'Comprehensive Development Zone (CDZ)',
        '37.03': 'Urban Floodway Zone (UFZ)',
        '37.04': 'Capital City Zone (CCZ)',
        '37.05': 'Docklands Zone (DZ)',
        '37.06': 'Priority Development Zone (PDZ)',
        '37.07': 'Urban Growth Zone (UGZ)',
        '37.08': 'Activity Centre Zone (ACZ)',
        '40': 'Overlays (face sheet)',
        '41': 'Operation of Overlays',
        '42': 'Environmental and Landscape Overlays (Face Sheet)',
        '42.01': 'Environmental Significance Overlay (ESO)',
        '42.02': 'Vegetation Protection Overlay (VPO)',
        '42.03': 'Significant Landscape Overlay (SLO)',
        '43': 'Heritage and Built Form Overlays (face sheet)',
        '43.01': 'Heritage Overlay (HO)',
        '43.02': 'Design and Development Overlay (DDO)',
        '43.03': 'Incorporated Plan Overlay (IPO)',
        '43.04': 'Development Plan Overlay (DPO)',
        '43.05': 'Neighbourhood Character Overlay (NCO)',
        '44': 'Land Management Overlays (face sheet)',
        '44.01': 'Erosion Management Overlay (EMO)',
        '44.02': 'Salinity Management Overlay (SMO)',
        '44.03': 'Floodway Overlay (FO or RFO)',
        '44.04': 'Land Subject to Inundation Overlay (LSIO)',
        '44.05': 'Special Building Overlay (SBO)',
        '44.06': 'Bushfire Management Overlay (BMO) prior to 18 November 2011 Wildfire Management Overlay (WMO)',
        '44.07': 'State Resource Overlay (SRO)',
        '45': 'Other Overlays (face sheet)',
        '45.01': 'Public Acquisition Overlay (PAO)',
        '45.02': 'Airport Environs Overlay (AEO)',
        '45.03': 'Environmental Audit Overlay (EAO)',
        '45.04': 'Road Closure Overlay (RXO)',
        '45.05': 'Restructure Overlay (RO)',
        '45.06': 'Development Contribution Overlay (DCPO)',
        '45.07': 'City Link Project Overlay (CLPO)',
        '45.08': 'Melbourne Airport Environs Overlay (MAEO)',
        '45.09': 'Parking Overlay (PO)',
        '45.11': 'Infrastructure Contributions Overlay (ICO)', 
        '45.12': 'Specific Controls Overlay (SCO)',                      
        '50': 'Particular Provisions (face sheet)',
        '51': 'Provisions that apply only to Specified Area',
        '51.01': 'Specific Sites and Exclusions',
        '51.02': 'Metropolitan Green Wedge Land: Core Planning Provisions',
        '51.03': 'Upper Yarra Valley and Dandenong Ranges Strategy',
        '51.04': 'Melbourne Airport Environs Strategy Plan',
        '51.05': 'Williamstown Shipyards Site Strategy Plan',
        '52': 'Provisions That Require, Enable or Exempt a Permit',
        '52.01': '[No content/SUBDIVISION]',
        '52.02': 'Easements, Restrictions and Reserves',
        '52.03': '[No content/SPECIFIC SITES AND EXCLUSIONS]',
        '52.04': 'Satellite Dish',
        '52.05': 'Signs',
        '52.06': 'Car Parking',
        '52.07': '[No content/LOADING AND UNLOADING OF VEHICLES]',
        '52.08': 'Earth and Energy Recources Industry',
        '52.09': 'Stone Extraction and Extractive Industry Interest Areas',
        '52.10': '[No content/USES WITH ADVERSE AMENITY POTENTIAL]',
        '52.11': 'Home Based Business',
        '52.12': 'Bushfire Protection Exemptions',
        '52.13': '2009 Bushfire - Recovery Exceptions',
        '52.14': '2009 Bushfire - Replacement Buildings',       
        '52.15': 'Heliport and Helicopter Landing Sites',
        '52.16': 'Native Vegetation Precinct Plan',
        '52.17': 'Native Vegetation',
        '52.18': '[No content/TIMBER PRODUCTION]',
        '52.19': 'Telecommunications Facility',
        '52.20': '[No content/CONVENIENCE RESTAURANT AND TAKE-AWAY FOOD PREMISES]',
        '52.21': 'Private Tennis Court',
        '52.22': 'Crisis Accommodation',
        '52.23': 'Shared Housing',
        '52.24': 'Community Care Unit',
        '52.25': 'Crematorium',
        '52.26': '[No content/CATTLE FEEDLOT]',
        '52.27': 'Licensed Premises',
        '52.28': 'Gaming',
        '52.29': 'Land Adjacent to a Road Zone, Category 1 or Public Acquisition Overlay for a Category 1 Road',
        '52.30': '[No content/FREEWAY SERVICE CENTRE]',
        '52.31': '[No content]',
        '52.32': 'Wind Energy Facility',
        '52.33': 'Post Boxes and Dry Stone Walls',
        '52.34': 'Bicycle Facilities',
        '53': 'General Requirements and Performance Standards',
        '53.01': 'Public Open Space Contribution and Subdivision',        
        '53.02': 'Bushfire Planning',        
        '53.03': 'Brothels',
        '53.04': 'Convenience Restaurant and Take-away Food Premises',
        '53.05': 'Freeway Service Centre',
        '53.06': 'Live Music and Entertainment Noise',
        '53.07': 'Shipping Container Storage',
        '53.08': 'Cattle Feedlot',
        '53.09': 'Broiler Farm',
        '53.10': 'Users with Adverse Amenity Potential', 
        '53.11': 'Timber Production',
        '53.12': 'Racing Dog Keeping and Training',
        '53.13': 'Renewable Energy Facility (other than wind energy facility and geothermal energy extraction)', 
        '53.14': 'Resource Recovery', 
        '53.15': 'Statement of Underlying Provisions',       
        '54': 'One dwelling on a lot',
        '54.01': 'Neighbourhood and site description and design response',
        '54.02': 'Neighbourhood character',
        '54.03': 'Site layout and building massing',
        '54.04': 'Amenity impacts',
        '54.05': 'On-site amenity and facilities',
        '54.06': 'Detailed design',
        '55': 'Two or more dwellings on a lot and residential buildings',
        '55.01': 'Neighbourhood and site description and design response',
        '55.02': 'Neighbourhood character and infrastructure',
        '55.03': 'Site layout and building massing',
        '55.04': 'Amenity impacts',
        '55.05': 'On-site amenity and facilities',
        '55.06': 'Detailed design',
        '55.07': 'Apartment Developments',
        '56': 'Residential subdivision',
        '56.01': 'Subdivision Site and context description and design response',
        '56.02': 'Policy Implementation',
        '56.03': 'Livable and Sustainable Communities',
        '56.04': 'Lot Design',
        '56.05': 'Urban Landscape',
        '56.06': 'Access and Mobility Management',
        '56.07': 'Integrated Water Management',
        '56.08': 'Site Management',
        '56.09': 'Utilities',
        '57': '[No content]',
        '58': 'Apartment Developments',
        '58.01': 'Urban context report and design response',
        '58.02': 'Urban context',
        '58.03': 'Site layout',
        '58.04': 'Amenity impacts',
        '58.05': 'On-site amenity and facilities',
        '58.06': 'Detail design',
        '58.07': 'Internal amenity',
        '59': 'VicSmart Planning Applications and Requirements',
        '59.01': 'Realign the common boundary between two lots',
        '59.02': 'Subdivision of buildings and car parking spaces',        
        '59.03': 'Front Fence in a Residential Zone',        
        '59.04': 'Buildings and works in a zone (except a Rural Zone)',        
        '59.05': 'Buildings and works in an Overlay',        
        '59.06': 'Remove, destroy or lop a tree',        
        '59.07': 'Applications under a Heritage Overlay',        
        '59.08': 'Applications under a Special Building Overlay',        
        '59.09': 'Signs',
        '59.10': 'Car parking',             
        '59.11': '[No content]',        
        '59.12': 'Two lot subdivision in a Rural Zone',        
        '59.13': 'Building and works in a Rural Zone',        
        '59.14': 'Extension to one dwelling on a lot in a Residential Zone', 
        '59.15': 'Local VicSmart Applications', 
        '59.16': 'Information requirements and decision guidelines for local VicSmart applications', 
        '60': 'General Provisions (face sheet)',
        '61': '[No content/General Provisions]',
        '62': 'General Exemptions',
        '62.01': 'Uses not requiring a permit',        
        '62.02': 'Buildings and works',        
        '62.03': 'Events on public land',        
        '62.04': 'Subdivisions not requiring a permit',        
        '62.05': 'Demolition',        
        '63': 'Existing Uses',
        '63.01': 'Extent of existing use rights',        
        '63.02': 'Characterisation of use',        
        '63.03': 'Effect of definitions on existing use rights',        
        '63.04': 'Section 1 uses',        
        '63.05': 'Section 2 and 3 uses',        
        '63.06': 'Expiration of existing use rights',        
        '63.07': 'Compliance with codes of practice',        
        '63.08': 'Alternative use',        
        '63.09': 'Shop conditions',        
        '63.10': 'Damaged or destroyed buildings or works',        
        '63.11': 'Proof of continuous use',       
        '63.12': 'Decision guidelines',        
        '64': 'General Provisions For Use and Development of Land',
        '64.01': 'Land used for more than one use',        
        '64.02': 'Land used in conjunction with another use',        
        '64.03': 'Subdivision of land in more than one zone',        
        '65': 'Decision Guidelines',
        '65.01': 'Approval of an application or plan',        
        '65.02': 'Approval of an application to subdivide land',        
        '66': 'Referrals and Notice Provisions',
        '66.01': 'Subdivision referrals',        
        '66.02': 'Use and development referrals',        
        '66.03': 'Referral of permit applications under other State standard provisions',        
        '66.04': 'Referral of permit applications under local provisions',
        '66.05': 'Notice of permit applications under State standard provisions',        
        '66.06': 'Notice of permit applications under local provisions',
        '67': 'Applications Under Section 96 of the Act',
        '67.01': 'Exemptions from Section 96(1) and 96(2) of the Act',
        '67.02': 'Notice requirements',
        '67.03': 'Notice requirements - native vegetation',
        '67.04': 'Notice exemption',
        '70': 'Operational Provisions',
        '71': 'Operation of this Planning Scheme',
        '71.01': 'Operation of the Municipal Planning Strategy',
        '71.02': 'Operation of the Planning Policy Framework',
        '71.03': 'Operation of Zones',
        '71.04': 'Operation of Overlays',
        '71.05': 'Operation of the Particular Provisions',
        '71.06': 'Operation of VicSmart Applications and processes',
        '72': 'Administration and Enfprcement of this Planning Scheme',
        '72.01': 'Responsible Authority for this Planning Scheme',
        '72.02': 'What area is covered by this Planning Scheme?',
        '72.03': 'What does this Planning Scheme consist of?',
        '72.04': 'Documents incorporated in this Planning Scheme',
        '72.05': 'When did this Planning Scheme begin?',
        '72.06': 'Effect of this Planning Scheme',
        '72.07': 'Determination of boundaries',
        '72.08': 'Background documents',                
        '73': 'Meaning of Terms',
        '73.01': 'General terms',
        '73.02': 'Sign terms',
        '73.03': 'Land use terms',                
        '73.04': 'Nesting Diagrams',               
        '74': 'Strategic Implementation',
        '74.01': 'Application of Zones, Overlays and Provisions',
        '74.02': 'Further strategic work',
    }

    # VC147 14-09-2018 (source=Planning Scheme Suffix & regions Clauses10-19.xlsx)
    # H|CH|MM|G|G21|GSC|LMN|LMS|WSM
    REGION = dict(
        H   = 'Hume',
        CH  = 'Central Highlands',
        MM  = 'Metropolitan Melbourne',
        G   = 'Gippsland',
        G21 = 'Geelong',
        GSC = 'Great South Coast',
        LMN = 'Loddon Mallee North',
        LMS = 'Loddon Mallee South',
        WSM = 'Wimmera Southern Mallee')


    filename = str(filename)
    result = {}
    result['clause'] = ''
    result['clause_id'] = ''
    result['clause_type'] = ''

    if '.pdf' in filename.lower():
        file_type = 'pdf'
    elif '.doc' in filename.lower():
        file_type = 'doc'
    else:
        result['file_type'] = 'error'
        result['error'] = 'Invalid file type (expecting pdf or doc).'
        result['filename'] = filename
        return result

    result['file_type'] = file_type
    
    # exceptions
    if filename in [
            '1997-11-01-NFPS.pdf', 
            '1998-03-13-C1.pdf',
            '1998-03-26-VC1.pdf',
            '1998-07-09-VC2.pdf',
            '1998-09-24-VC3.pdf',
            ]:
        #result['s3_name'] = 'planning_scheme.pdf'
        #result['filename'] = 'planning_scheme'
        result['clause'] = 'all'
        return result
    
    # fix-ups

    # 'Port of Melbourne/2008-01-10-NFPS/amlist_s_pmelb'
    if filename == 'amlist_s_pmelb.pdf':
        filename = 'amlist_s.pdf'

    # 'Mornington Peninsula/1999-05-06-NFPS/psamlist_morn'
    if filename == 'psamlist_morn.pdf':
        filename = 'amlist.pdf'

    # 'Pyrenees/2010-06-18-VC62/35_07s_p'
    if filename == '35_07s_p.pdf':
        filename = '35_07s_pyrn.pdf'

    # Baw Baw/2013-05-02-C99/21_mss09_bawb - Copy.pdf
    if filename == '21_mss09_bawb - Copy.pdf':
        filename = '21_mss09.pdf'

    # 01/10/2019 - Maribyrnong/2014-09-04-VC120/43_03 - Copy.pdf
    if filename == '43_03 - Copy.pdf':
        filename = '43_03.pdf'

    # 01/10/2019 - Maribyrnong/2014-09-04-VC120/43_04 - Copy.pdf
    if filename == '43_04 - Copy.pdf':
        filename = '43_04.pdf'

    # 01/10/2019 - Maribyrnong/2014-09-04-VC120/44 - Copy.pdf
    if filename == '44 - Copy.pdf':
        filename = '44.pdf'

    # 04/10/2019 (second fix) Colac Otway/2019-09-26-VC164/44_06s01_cola.pdf.pdf
    if '.pdf.pdf' in filename:
        filename = filename[:-4]
    
    # clean-up the filename by removing the ps abreviation (either as a prefix
    # or a suffix - with or without a leading underscore)
    # Allow for the case of 'Melbourne/2002-08-02-C69/21_mss_PartA.pdf'
    #    22_lpp01_alpi
    if re.search('^(.*)part[a-z]{1}\.(pdf|doc)$', filename, re.IGNORECASE) is None:
        if re.search('^([a-z]{4})(\d{2})(.*)\.(pdf|doc)$', filename, re.IGNORECASE) is not None:
            filename = filename[4:]
            result['diag_ns'] = 100
        elif re.search('^(\d{2})(.*)\_yarra\.(pdf|doc)$', filename, re.IGNORECASE) is not None:
            filename = filename[:-6]
            result['diag_ns'] = 101
        elif re.search('^(\d{2})(.*)\_([a-z]{4}) \.(pdf|doc)$', filename, re.IGNORECASE) is not None:
            filename = filename[:-6]
            result['diag_ns'] = 102
        elif re.search('^(\d{2})(.*)\_ ([a-z]{4})\.(pdf|doc)$', filename, re.IGNORECASE) is not None:
            filename = filename[:-6]
            result['diag_ns'] = 103
        elif re.search('^(\d{2})(.*)\_well\_\.(pdf|doc)$', filename, re.IGNORECASE) is not None:
            filename = filename[:-6]
            result['diag_ns'] = 104
        elif re.search('^(\d{2})(.*)\_([a-z]{4})\.(pdf|doc)$', filename, re.IGNORECASE) is not None:
            filename = filename[:-5]
        elif re.search('^(\d{2})(.*)([a-z]{4})\.(pdf|doc)$', filename, re.IGNORECASE) is not None:
            filename = filename[:-4]
        elif re.search('^(\d{2})\_(.*)([a-z]{4})\.(pdf|doc)$', filename, re.IGNORECASE) is not None:
            filename = filename[:-4]

    # '.(pdf|doc)' is also removed.
    filename = filename[:-4]


    result['filename'] = filename


    # exsamples after '.pdf' removed:
    # cover  - 00_cover_alpi
    # cover  - _cover_alpi           
    # obj    - 01_objectives
    # pre    - 01_preliminary
    # purp   - 02_purposes
    # ug     - 02_ug
    # cont   - 03_contents_alpi
    # ug     - 04_ug
    # ccssei - 37_01s01EI_hume
    # c      - 31
    # cc     - 52_06
    # ccs2a  - 52_28_5s_arat
    # css2b  - Glen Eira/1999-08-05 NFPS/52_28-5s_glen.pdf
    #          Casey/2000-08-17-VC8/52_05-5s_case.pdf
    # cs     - 61s_alpi
    # css    - 95s01_ggee
    # lppfs  - 20_lppf.pdf 
    # ccss   - Bass Coast/2006-11-14-VC44/43_04s04_basc.pdf
    # cc3    - Ballarat/2008-04-07-VC47/52.17.pdf
    # ccssa  - Casey/2009-09-03-C123/45_06s07a_case.pdf)
    # lpccc  - Alpine Resorts/2006-08-24-C15/22_lpp01_01_alpr.pdf
    # ccs1b  - Alpine Resorts/1998-12-17-VC6/alpr52_01s.pdf
    # - Casey/2007-01-11-C89/43_02s01case.pdf
    # cc4    - Yarra/2004-12-23-VC32/22_10_yarra.pdf
    # ccss1a - Greater Geelong/2008-02-04-VC46/35_06s010_ggee.pdf
    # Alpine Resorts/1998-12-17-VC6/alpr71pt1.pdf
    # Alpine Resorts/1999-03-25-VC5/alpr52_28_5s.pdf
    #        Ballarat/1999-06-03-C3/ball37_01s01.pdf
    # Bayside/2000-02-24-NFPS/43_02s_01bays.pdf
    # Alpine Resorts/1999-07-13-C3/22_alpr.pdf
    # Ballarat/1999-06-24-C2/21_mss_ball.pdf
    # Ballarat/2002-10-08-VC16/61_s_ball.pdf
    #     - Brimbank/2001-11-22-VC14/22_01_00_brim.pdf
    # lppfc1 - Campaspe/2000-03-16-C4/21_lppf_camp.pdf
    # ccss3  - East Gippsland/2003-01-30-C20/42_01s01-94_egip.pdf
    # ccssei1- Frankston/2006-01-19-VC37/37_01s2EI_fran.pdf
    # msscc2 - French Island and Sandstone Island/2000-05-25-VC9/21_ss10_fisi.pdf
    # ccss1b - Glenelg/2008-09-04-C39/43_04s05A_gelg.pdf
    # (from Angela) For example:   21_mss04_02A_cora, 
    #                              43_02s6A_ston.   
    # ccs1c  - Greater Bendigo/2006-01-19-VC37/32_01s_g.pdf
    # msscc1 - Greater Dandenong/1999-03-26-NFPS/gdan21_mss01.pdf

    patterns = [
        ('am',      0, 0, '^amlist$'),
        ('am1',     1, 1, '^amlist_([a-z]{4})$'),
        ('am2',     0, 1, '^amlist_s$'),
        ('am3',     0, 1, '^psamlist$'),
        ('ams',     1, 0, '^amlist_s_([a-z]{4})$'),                             # the ps code bleads through

        ('coverc',  1, 0, '^(\d{2})\__cover$'),                                 
        ('cover4',  1, 0, '^\_cover\_([a-z]{4})$'),                             # VC148 _cover_card
        ('cover5',  0, 0, '^00_cover$'),                                        # Alpine Resorts/2012-12-18-VC93/00_cover_alpr.pdf

        ('objc',    1, 0, '^(\d{2})\_objectives$'),                             # 12 instances
        ('objc1',   1, 1, '^(\d{2})\_objective$'),                              # 1 chars stripped off above.
        ('objc2',   1, 1, '^(\d{2})\_object$'),                                 # 3 chars stripped off above.
        ('objc3',   1, 1, '^(\d{2})\_objec$'),                                  # 8 chars stripped off above (for 'preliminary').
        ('preliminaryc',    1, 0, '^(\d{2})\_preliminary$'),                    # 8 chars stripped off above.
        ('prelimi', 1, 0, '^(01)_prelimi$'),                                    # Alpine Resorts/2017-05-25-VC133/01_preliminary.pdf
        ('purpc',   1, 1, '^(\d{2})\_purposes$'),                               # 26 instances
        ('purpc1',  1, 1, '^(\d{2})\_purpose$'),
        ('purpc2',  1, 0, '^(\d{2})\_purp$'),                                   # 4 chars stripped off above.
        ('contc',   1, 0, '^(\d{2})\_contents$'),
        ('contcA',  1, 0, '^(\d{2})\_contentsA$'),                              # 'Strathbogie/2001-09-27-VC13/03_contentsA'
        ('contc1',  1, 1, '^(\d{2})\_cont$'),                                   # 68 instances
        ('ugc',     1, 0, '^(\d{2})\_ug$'),
        ('sppfc',   1, 0, '^(\d{2})\_sppf$'),
        ('lppfc',   1, 0, '^(\d{2})\_lppf$'),                                   # 26 instances
        ('lppfcc',  2, 0, '^(\d{2})\_(\d{2})\_lppf$'),                          # 4 instances
        ('mssc',    1, 0, '^(\d{2})\_mss$'),
        ('mssca',   2, 1, '^(\d{2})\_msspart([a-z]{1})$'),
        ('mssca1',  2, 1, '^(\d{2})\_mss\_part([a-z]{1})$'),
        ('msscc',   2, 0, '^(\d{2})\_mss(\d{2})$'),
        ('msscc1',  2, 1, '^(\d{2})\_mss(\d{1})$'),                             # 16 instances
        ('msscc2',  2, 1, '^(\d{2})mss(\d{2})$'),
        ('msscc3',  2, 1, '^(\d{2})\_ss(\d{2})$'),
        ('msscc4',  2, 1, '^(\d{2})\_mss\_(\d{2})$'),                           # 24 instances
        ('mssccA',  2, 1, '^(\d{2})\_mss(\d{2})A$'),
        ('msscca',  2, 0, '^(\d{2})\_mss(\d{2}[a-z]{1})$'),                     # 21 instances
        ('mssccc',  3, 0, '^(\d{2})\_mss(\d{2})\_(\d{2})$'),
        ('msscccA', 3, 1, '^(\d{2})\_mss(\d{2})\_(\d{2})A$'),                   # 'Corangamite/2004-07-15-C8/21_mss04_02A'
        ('mssccc1', 3, 1, '^(\d{2})\_mss(\d{2})\_(\d{1})$'),
        ('msscccc', 4, 1, '^(\d{2})\_mss(\d{2})\_(\d{2})\_(\d{2})$'),
        ('mssccccA',4, 1, '^(\d{2})\_mss(\d{2})\_(\d{2})\_(\d{2})A$'),          # 'Indigo/2005-06-23-C26/21_mss04_01_01A'
        #('lppc',    1, 0, '^(\d{2})$'),                                        # ... conflicts with rule 'c' below.
        ('lppc1',   1, 1, '^(\d{2})\_lpp$'),                                    # 44 instances
        ('lppcc',   2, 0, '^(\d{2})\_lpp(\d{2})$'),
        ('lppccc',  3, 0, '^(\d{2})\_lpp(\d{2})\_(\d{2})$'),
        ('ccsei',   3, 0, '^(\d{2})\_(\d{2})s(\d{2})EI$'),
        ('ccsei1',  3, 0, '^(\d{2})\_(\d{2})s(\d{1})EI$'),                      # 1 char schedule
        
        ('c',       1, 0, '^(\d{2})$'),
        ('cc',      2, 0, '^(\d{2})\_(\d{2})$'),
        ('ccUG',    2, 1, '^(\d{2})\_(\d{2})\_UG$'),                            # Alpine Resorts/2018-03-28-VC145/02_00_UG.pdf
        ('cc1',     2, 1, '^(\d{2})\_(\d{2})\_$'),                              # 11 instances
        ('cc2',     2, 1, '^(\d{2})\.(\d{2})$'),
        ('cc3',     2, 1, '^(\d{2})\_(\d{2})lpp$'),                             # 'Kingston/2005-05-19-C51/22_14lpp'
        ('cp',      2, 0, '^(\d{2})pt(\d{1})$'),                                # 42 instances. part
        ('cr',      2, 0, '^(\d{2})\-(\d{2})$'),                                # 58 instances. range
        ('ccc',     3, 0, '^(\d{2})\_(\d{2})\_(\d{2})$'),
        ('cccE',    3, 1, '^(\d{2})\_(\d{2})\_(\d{2})\s{1}$'),                  # 'Indigo/2006-01-19-VC37/22_03_09 '
        ('ccc1',    3, 1, '^(\d{2})\_(\d{2})\_(\d{1})$'),

        ('s',       1, 0, '^(\d{2})s$'),
        ('s1',      1, 1, '^(\d{2})\_s$'),                                      # 17 instances
        ('cs',      2, 0, '^(\d{2})\_(\d{2})s$'),
        ('c95s',    2, 0, '^(95)s(\d{2})$'),                                    # 01/10/2019 Greater Geelong/2016-07-28-C317/95s01_ggee.pdf
        ('css',     2, 2, '^(\d{2})s(\d{1,2})$\_(\d{2})'),
        ('cs1',     2, 1, '^(\d{2})s\_(\d{2})$'),
        ('cs2',     2, 1, '^(\d{2})s\_(\d{2})s$'),                              # 46 instances
        ('csA',     2, 1, '^(\d{2})\_(\d{2})sA$'),                              # 'Boroondara/2006-01-19-VC37/52_01sA'
        ('ccs',     3, 0, '^(\d{2})\_(\d{2})s(\d{1,2})$'),
        ('ccsA',    3, 1, '^(\d{2})\_(\d{2})s(\d{1,2})A$'),                     # 'Stonnington/2006-03-02-C47/43_02s6A'
        ('ccsa',    4, 0, '^(\d{2})\_(\d{2})s(\d{2})([a-z]{1})$'),
        ('ccs1',    3, 1, '^(\d{2})\_(\d{2})s0(\d{1,2})$'),
        ('ccs2',    3, 1, '^(\d{2})\_(\d{2})\-(\d{1,2})s$'),                    # 70 instances
        ('ccs3',    3, 1, '^(\d{2})\_(\d{2})\_(\d{1,2})s$'),
        ('ccs4',    3, 1, '^(\d{2})\_(\d{2})\_s(\d{1,2})$'),
        ('ccs5',    3, 1, '^(\d{2})\_(\d{2})s(\d{1,2})\.$'),
        ('ccs6',    3, 1, '^(\d{2})\_(\d{2})s\_(\d{1,2})$'),                    # Bayside/2019-09-26-VC164/43_02s_18_bays.pdf
        ('ccsr',    4, 0, '^(\d{2})\_(\d{2})s(\d{1,2})-(\d{1,2})$'),            # range

        ('cnn1r',   3, 0, '^(\d{2})\_(\d{2})\-(\d{1})r$'),                      # VC148 -- 11_03-1R
        ('cnn2r',   3, 1, '^(\d{2})\_(\d{2})\-(\d{2})r$'),                      # VC148 -- 11_03-01R 
        ('cnnmm',   5, 1, '^(\d{2})\_(\d{2})\-(\d{1,2})r\-(\d{0,1})(H|CH|MM|G|G21|GSC|LMN|LMS|WSM)$'),  # VC148 -- 11_01-1R-1MM, 11_01-01R-1MM
        ('cnn1rr',  4, 0, '^(\d{2})\_(\d{2})\-(\d{1})r\-(.+?)$'),               # VC148 -- 11_01-1R-???
        ('cnn2rr',  4, 1, '^(\d{2})\_(\d{2})\-(\d{2})r\-(.+?)$'),               # VC148 -- 18_02-02R-1
        ('cnn3s',   3, 1, '^(\d{2})\_(\d{2})\-(\d{3})s$'),                      # VC148 -- 11_03-002S and 14_03-001S

        ('cnn3l',   3, 1, '^(\d{2})\_(\d{2})\-(\d{1})l$'),                      # Murrindindi/2019-09-26-VC164/11_01-1L.pdf

        # After more specific rules
        ('cx',      2, 1, '^(\d{2})([a-z]{1})$'),                               # 30/09/2019, 04/10/2019 Alpine/2018-07-31-VC148/10G.pdf or 10K.pdf etc. 
        ('ccx',     3, 1, '^(\d{2})\_(\d{2})([a-z]{1})$'),                      # 30/09/2019, 04/10/2019 Alpine Resorts/2018-07-31-VC148/11_00K.pdf

        ]
    
    matched = False

    # Filename now looks like ... 43_02s_18 or 11_01-1L

    for key, count, log, pattern in patterns:
        m = re.search(pattern, filename, re.IGNORECASE)

        if m is not None:

            # check that we ended up the the correct number of parameters
            if len(m.groups()) != count:
                msg = 'Invalid count (%i) for %s, expecting %i (rule %s)' % \
                    (len(m.groups()), filename, count, key)
                #print('[analyse_clause.py]', msg)
                result['error'] = str(msg)
            matched = True
            
            # log is for a message if the pattern is not standard (0=OK)
            if log != 0:
                result['diag_ns'] = str(log)

            break
    
    if m is None:
        msg = 'Pattern not found for: %s' % filename
        result['error'] = msg
        return result
    
    result['diag-key'] = key
    clause = True

    if key in ['coverc', 'objc', 'objc1', 'objc2', 'objc3', 'preliminaryc', 
        'purpc', 'purpc1', 'purpc2', 'contc', 'contc1', 'ugc', 'sppfc', 'c', 'cx']:
        result['clause']    = m.group(1)
        result['clause_id'] = m.group(1)
        #result['s3_name'] = '%s' % result['clause']

    elif key in ['cover4', 'cover5']:
        clause = False
        result['clause']      = 'Cover Page'
        result['clause_type'] = 'Planning Scheme Cover Page'
        result['clause_id']   = None

    elif key == 'contcA':
        result['clause']    = m.group(1)
        result['clause_id'] = m.group(1)
        #result['s3_name'] = '%s-A' % result['clause']

    elif key in ['lppfc']:
        result['clause']    = m.group(1)
        result['clause_id'] = m.group(1)
        #result['s3_name'] = result['clause']

    elif key == 'lppfcc':
        if m.group(2) == '00':
            result['clause']    = m.group(1)
            result['clause_id'] = m.group(1)
        else:
            result['clause']    = '%s.%s' % (m.group(1), m.group(2))
            result['clause_id'] = '%s.%s' % (m.group(1), m.group(2))
        #result['s3_name'] = result['clause']

    elif key == 'mssc':
        result['clause']    = m.group(1)
        result['clause_id'] = m.group(1)
        result['mss'] = True
        #result['s3_name'] = result['clause']

    elif key in ['msscc', 'msscc1', 'msscc2', 'msscc3', 'msscc4', 'msscca']:
        if m.group(2) == '00':
            result['clause']    = m.group(1)
            result['clause_id'] = m.group(1)
        else:
            result['clause']    = '%s.%s' % (m.group(1), m.group(2))
            result['clause_id'] = '%s.%s' % (m.group(1), m.group(2))
        result['mss'] = True
        #result['s3_name'] = result['clause']

    elif key == 'mssccA':
        if m.group(2) == '00':
            result['clause']    = m.group(1)
            result['clause_id'] = m.group(1)
        else:
            result['clause']    = '%s.%s' % (m.group(1), m.group(2))
            result['clause_id'] = '%s.%s' % (m.group(1), m.group(2))
        result['mss'] = True
        #result['s3_name'] = '%s-A' % result['clause']

    elif key in ['mssca', 'mssca1']:
        result['clause']    = '%s (part %s)' % (m.group(1), m.group(2))
        result['clause_id'] = m.group(1)
        result['mss']       = True
        #result['s3_name'] = result['clause']

    elif key in ['mssccc', 'mssccc1']:
        result['clause']    = '%s.%s-%s' % (m.group(1), m.group(2), m.group(3))
        result['clause_id'] = '%s.%s' % (m.group(1), m.group(2))
        result['mss']       = True
        #result['s3_name'] = result['clause']

    elif key == 'msscccA':
        result['clause']    = '%s.%s-%s' % (m.group(1), m.group(2), m.group(3))
        result['clause_id'] = '%s.%s' % (m.group(1), m.group(2))
        result['mss']       = True
        #result['s3_name'] = '%s-A' % result['clause']

    elif key == 'msscccc':
        result['clause']    = '%s.%s-%i-%i' % (m.group(1), m.group(2), int(m.group(3)), int(m.group(4)))
        result['clause_id'] = '%s.%s' % (m.group(1), m.group(2))
        result['mss']       = True
        #result['s3_name'] = result['clause']

    elif key == 'mssccccA':
        result['clause']    = '%s.%s-%i-%i' % (m.group(1), m.group(2), int(m.group(3)), int(m.group(4)))
        result['clause_id'] = '%s.%s' % (m.group(1), m.group(2))
        result['mss']       = True
        #result['s3_name'] = '%s-A' % result['clause']

    elif key in ['lppc', 'lppc1']:
        result['clause']    = m.group(1)
        result['clause_id'] = m.group(1)
        result['lpp']       = True
        #result['s3_name'] = result['clause']

    elif key == 'lppcc':
        if m.group(2) == '00':
            result['clause']    = m.group(1)
            result['clause_id'] = m.group(1)
        else:
            result['clause']    = '%s.%s' % (m.group(1), m.group(2))
            result['clause_id'] = '%s.%s' % (m.group(1), m.group(2))
        result['lpp']           = True
        #result['s3_name'] = result['clause']

    elif key == 'lppccc':
        result['clause']    = '%s.%s-%s' % (m.group(1), m.group(2), m.group(3))
        result['clause_id'] = '%s.%s' % (m.group(1), m.group(2))
        result['lpp']       = True
        #result['s3_name'] = result['clause']

    elif key in ['ccsei', 'ccsei1']:
        result['clause']       = '%s.%s (schedule %i)' % (m.group(1), m.group(2), int(m.group(3)))
        result['clause_id']    = '%s.%s' % (m.group(1), m.group(2))
        result['schedule']     = True
        result['schedule_num'] = m.group(3)
        result['ei'] = True
        #result['s3_name'] = '%s.%s-schedule-%i' % (m.group(1), m.group(2), int(m.group(3)))

    elif key in ['cc', 'cc1', 'cc2', 'ccUG', 'ccx']:
        if m.group(2) == '00':
            result['clause']    = m.group(1)
            result['clause_id'] = m.group(1)
        else:
            result['clause']    = '%s.%s' % (m.group(1), m.group(2))
            result['clause_id'] = '%s.%s' % (m.group(1), m.group(2))
        #result['s3_name'] = result['clause']

    elif key == 'cp':
        result['clause']      = '%s (part %i)' % (m.group(1), int(m.group(2)))
        result['clause_id']   = m.group(1)
        result['clause_part'] = int(m.group(2))
        #result['s3_name'] = '%s-part-%i' % (m.group(1), int(m.group(2)))

    elif key == 'cr':
        result['clause']       = '%s to %s' % (m.group(1), m.group(2))
        result['clause_id']    = None
        result['clause_range'] = True
        #result['s3_name'] = '%s-%s' % (m.group(1), m.group(2))

    elif key in ['ccc', 'cccE', 'ccc1']:
        result['clause']    = '%s.%s-%i' % (m.group(1), m.group(2), int(m.group(3)))
        result['clause_id'] = '%s.%s' % (m.group(1), m.group(2))
        #result['s3_name'] = result['clause']

    elif key in ['s', 's1']:
        result['clause']    = '%s (schedule)' % m.group(1)
        result['clause_id'] = m.group(1)
        result['schedule']  = True
        #result['s3_name'] = '%s-schedule' % m.group(1)

    elif key in ['cs', 'css', 'cs1', 'cs2', 'cc3', 'c95s']:
        result['clause']    = '%s.%s (schedule)' % (m.group(1), m.group(2))
        result['clause_id'] = '%s.%s' % (m.group(1), m.group(2))
        result['schedule']  = True
        #result['s3_name'] = '%s.%s-schedule' % (m.group(1), m.group(2))

    elif key == 'csA':
        result['clause']    = '%s.%s (schedule)' % (m.group(1), m.group(2))
        result['clause_id'] = '%s.%s' % (m.group(1), m.group(2))
        result['schedule']  = True
        #result['s3_name'] = '%s.%s-A-schedule' % (m.group(1), m.group(2))

    elif key in ['ccs', 'ccs1', 'ccs3', 'ccs4', 'ccs5', 'ccs6']:
        result['clause']    = '%s.%s (schedule %i)' % (m.group(1), m.group(2), int(m.group(3)))
        result['clause_id'] = '%s.%s' % (m.group(1), m.group(2))
        result['schedule']  = True
        result['schedule_num'] = m.group(3)
        #result['s3_name'] = '%s.%s-schedule-%i' % (m.group(1), m.group(2), int(m.group(3)))

    elif key == 'ccsA':
        result['clause']    = '%s.%s (schedule %i)' % (m.group(1), m.group(2), int(m.group(3)))
        result['clause_id'] = '%s.%s' % (m.group(1), m.group(2))
        result['schedule']  = True
        result['schedule_num'] = m.group(3)
        #result['s3_name'] = '%s.%s-schedule-%i-A' % (m.group(1), m.group(2), int(m.group(3)))

    elif key in ['ccsa']:
        result['clause']       = '%s.%s (schedule %i%s)' % (m.group(1), m.group(2), int(m.group(3)), m.group(4))
        result['clause_id']    = "%s.%s" % (m.group(1), m.group(2))
        result['schedule']     = True
        result['schedule_num'] = m.group(3) + m.group(4)
        #result['s3_name'] = '%s.%s-schedule-%i%s' % (m.group(1), m.group(2), int(m.group(3)), m.group(4))

    elif key in ['ccs2']:
        result['clause']       = '%s.%s-%s (schedule)' % (m.group(1), m.group(2), m.group(3))
        result['clause_id']    = '%s.%s' % (m.group(1), m.group(2))
        result['schedule']     = True
        result['schedule_num'] = m.group(3)
        #result['s3_name'] = '%s.%s-%s-schedule' % (m.group(1), m.group(2), m.group(3))

    elif key in ['ccsr']:
        result['clause']         = '%s.%s (schedules %i to %i)' % (m.group(1), m.group(2), int(m.group(3)), int(m.group(4)))
        result['clause_id']      = '%s.%s' % (m.group(1), m.group(2))
        result['schedule']       = True
        result['schedule_range'] = True
        result['schedule_num']   = '%s-%s' % (m.group(3), m.group(4))
        #result['s3_name'] = '%s.%s-schedules-%i-%i' % (m.group(1), m.group(2), int(m.group(3)), int(m.group(4)))

    elif key in ['am', 'am1']:
        #result['s3_name'] = 'amendment-list'
        clause = False
        result['clause']      = 'AmList'
        result['clause_type'] = 'Amendment List'
        result['clause_id']   = None

    elif key in ['am2', 'ams']:
        #result['s3_name'] = 'amendment-list-schedule'
        clause = False
        result['clause']      = 'List of Ams'
        result['clause_type'] = 'List of Amendments'
        result['clause_id']   = None

    elif key == 'am3':
        #result['s3_name'] = 'amendment-list-ps'
        clause = False
        result['clause']      = 'AL'
        result['clause_type'] = 'Amendment List'
        result['clause_id']   = None
        #result['s3_name'] += '.' + file_type

    elif key in ['cnn1r', 'cnn2r', 'cnn1rr', 'cnn2rr']:
        result['clause']       = '%s.%s-%iR' % (m.group(1), m.group(2), int(m.group(3)))
        result['clause_id']    = result['clause']

    elif key in ['cnnmm']:
        result['clause']          = '%s.%s-%iR-%s%s' % (m.group(1), m.group(2), int(m.group(3)), m.group(4), m.group(5))
        result['clause_id']       = result['clause']
        result['regional_mm']     = '%s.%s'     % (m.group(1), m.group(2))
        result['regional_region'] = m.group(5)

    elif key in ['cnn3s']:
        result['clause']       = '%s.%s-%iS' % (m.group(1), m.group(2), int(m.group(3)))
        result['clause_id']    = result['clause']
        result['schedule']     = True
        result['schedule_num'] = m.group(3)

    elif key in ['cnn3l']:
        result['clause']       = '%s.%s-%iL' % (m.group(1), m.group(2), int(m.group(3)))
        result['clause_id']    = result['clause']
        result['schedule']     = True
        result['schedule_num'] = m.group(3)


    # Support pre and post VC148 clause names
    if clause:
        check_vc148_date = amendment_date_to_test

        if check_vc148_date < '2018-07-31':
            # Pre VC148 CLAUSE_TEXT and the old group table are used.
            try:
                clause_group = int(m.group(1))
            except:
                msg = "Clause group not an integer for %s" % result
                result['error'] = str(msg)
                print('[analyse_clause.py]', msg)
                return result

            if clause_group == 0:
                result['clause_type'] = 'Cover'
            elif clause_group == 1:
                result['clause_type'] = 'Preliminary'
            elif clause_group in range(2,10+1):
                result['clause_type'] = 'User Guide'
            elif clause_group == 3:
                result['clause_type'] = 'Contents'
            elif clause_group == 4:
                result['clause_type'] = 'User Guide'
            elif clause_group in range(9):
                result['clause_type'] = 'Plan Melbourne'
            elif clause_group in range(10,19+1):
                result['clause_type'] = 'State Planning Policy Framework'
            elif clause_group in [20, 21, 22]:           # TA added 20 to match data - 20_lppf.pdf
                result['clause_type'] = 'Local Planning Policy Framework'
            elif clause_group in range(30,39+1):
                result['clause_type'] = 'Zone'
            elif clause_group in range(40,49+1):
                result['clause_type'] = 'Overlay'
            elif clause_group in range(50,59+1):
                result['clause_type'] = 'Particular Provision'
            elif clause_group in range(60,69+1):
                result['clause_type'] = 'General Provision'
            elif clause_group in range(70,79+1):
                result['clause_type'] = 'Definitions'
            elif clause_group in range(90,99+1):
                result['clause_type'] = 'VicSmart'
                
            if 'clause_type' not in result:
                msg = "Unmatched clause group (%s) for %s" % (clause_group, filename)
                result['error'] = msg
                result['clause_type'] = ''
                print('[analyse_clause.py]', msg)

            if result['clause_id'] is not None:
                if result['clause_id'] != '':
                    if result['clause_id'] in CLAUSE_TEXT:
                        result['clause_type'] = CLAUSE_TEXT[result['clause_id']]

        else:
            # Post VC148 the new group table and  (now post_vc148_clause_data) are used.
            try:
                clause_group = int(m.group(1))
            except:
                msg = "Clause group not an integer for %s" % result
                result['error'] = str(msg)
                print('[analyse_clause.py]', msg)
                return result

            if clause_group == 0:
                result['clause_type'] = 'Purpose and Vision'        
            elif clause_group == 1:
                result['clause_type'] = 'Purposes of this Planning Scheme'
            elif clause_group in range(10,19+1):
                result['clause_type'] = 'Planning Policy Framework'
            elif clause_group in range(20,29+1):
                result['clause_type'] = 'Local Planning Policy Framework'
            elif clause_group in range(30,39+1):
                result['clause_type'] = 'Zones'
            elif clause_group in range(40,49+1):
                result['clause_type'] = 'Overlays'
            elif clause_group in range(50,59+1):
                result['clause_type'] = 'Particular Provisions'
            elif clause_group in range(60,69+1):
                result['clause_type'] = 'General Provisions'
            elif clause_group in range(70,79+1):
                result['clause_type'] = 'Operational Provisions'
                
            if 'clause_type' not in result:
                msg = "Unmatched clause group (%s) for %s" % (clause_group, filename)
                result['error'] = msg
                result['clause_type'] = ''
                print('[analyse_clause.py]', msg)

            if result['clause_id'] is not None:
                    if result['clause_id'] != '':

                        if 'regional_mm' in result:
                            # Clause Group - 10 to 19
                            if result['regional_mm'] in post_vc148_clause_data['clauses']:
                                result['clause_type'] = post_vc148_clause_data['clauses'][result['regional_mm']]['title'] + \
                                    ' - ' + REGION[result['regional_region']]

                        else:
                            if result['clause_id'] in   post_vc148_clause_data['clauses']:
                                result['clause_type'] = post_vc148_clause_data['clauses'][result['clause_id']]['title']

    return result


if __name__ == '__main__':
    #for name in test:
    #print('[analyse_clause.py]', process_filename_original('11_03-006S.pdf', clause_data.post_vc148_clause_data, '2020-02-11'))

    # result = {'clause': '11.03-6S', 'clause_id': '11.03-6S', 'clause_type': 'Regional and local places', 
    # 'file_type': 'pdf', 'filename': '11_03-006S', 'diag_ns': '1', 'diag-key': 'cnn3s', 'schedule': True, 'schedule_num': '006'}

    test_items = [
        ['00.pdf', '2020-02-11'],
        ['01.pdf', '2020-02-11'],
        ['10_00.pdf', '2020-02-11'],
        ['11_00.pdf', '2020-02-11'],
        ['11_01-001S.pdf', '2020-02-11'],
        ['11_01-01R-5G.pdf', '2020-02-11'],
        ['11_01-01R-7H.pdf', '2020-02-11'],
        ['11_01.pdf', '2020-02-11'],
        ['11_02-001S.pdf', '2020-02-11'],
        ['53_06s_alpr.pdf', '2020-02-11'],
        ['53_06_alpr.pdf', '2020-02-11'],
        ['72_02.pdf', '2020-02-11'],
        ['72_02s_alpr.pdf', '2020-02-11'],
        
        ['02_purposes.pdf', '2001-03-08'],
        ['01_objective.pdf', '2001-03-08'],
        ['11_sppf.pdf 11_sppf.pdf', '2001-03-08'],
        ['12_sppf.pdf 12_sppf.pdf', '2001-03-08'],
        ['04_ug.pdf', '2001-03-08'],
        ['03_contents_alpr.pdf', '2001-03-08'],
        ['13_sppf.pdf', '2001-03-08'],
        ['14_sppf.pdf', '2001-03-08'],
        ['15_sppf.pdf', '2001-03-08'],
        ['52_28_5s_alpr.pdf', '2001-03-08'],
        ['52_28_6s_alpr.pdf', '2001-03-08'],
        ['52_30.pdf', '2001-03-08'],
            ]

    for item in test_items:
        result = process_filename_original(item[0], clause_data.post_vc148_clause_data, item[1])
        print('[analyse_clause.py]', item[0], item[1], result['clause'], result['clause_type'])