# content.py

import datetime
import json
import urllib.parse

import respond, analyse_clause, analyse_map

def cumberland_api(http, api_url, api_function, arguments=[], query=None):
    
    if not isinstance(arguments, str):
        arguments = '/'.join(arguments)
        
    if len(arguments) > 0:
        url = api_url + '/' + api_function + '/' + arguments
        
    else:
        url = api_url + '/' + api_function

    if query is not None:
        url += '?' + urllib.parse.urlencode(query)
        
    print('[content.py] cumberland_api', url)
    
    response = http.request('GET', url)
    data = json.loads(response.data.decode('utf-8'))

    if 'result' in data:
        return data['result']
        
    print('[content.py] error', result)
    return None


def gen_cols(tag, data, header_align=None):
    col_data = ''
    
    for i, cell in enumerate(data):
        
        if header_align is None:
            cell_align = 'left'
            
        else:
            cell_align = header_align[i]

        col_data += f'<{tag} align="{cell_align}">{cell}</{tag}>'
        
    return '  <tr>' + col_data + '</tr>\n'


def gen_table(data, header_row, header_align=None):
    table_content = gen_cols('th', header_row, header_align)
    
    for row in data:
        table_content += gen_cols('td', row, header_align)
        
    return '<table border="0">\n' + table_content + '</table>'


def index(http, api_url, req_method, ps, am, content_path):

    ps_list = cumberland_api(http, api_url, 'planning-schemes', query=dict(select='name-list', vpp=True))
    
    header_row = ['Planning Scheme']
    
    table_data = []
    
    for ps in ps_list:
        # Using underscore instead of plus or space.
        ps_url = ps.replace(' ', '_')
        
        if ps == 'Delatite':
            ps += ' (historical)'
        
        link = f'<a href="{content_path}/{ps_url}">{ps}</a>'
        
        table_data.append([link])

    table_html = gen_table(table_data, header_row)
    
    return respond.result(table_html, title='Planning Scheme Index')
    

def planning_scheme(http, api_url, req_method, ps, am):
    
    print('[content.py] planning_scheme', ps)
    
    if ps == 'Delatite':
        ps_name = 'Delatite (historical)'
        
    else:
        ps_name = ps

    items = cumberland_api(http, api_url, 'list', arguments=ps)

    am_list = {}
    
    for item in items:

        if item['published']:
            
            am = item['am']
            
            # Using underscore instead of plus or space.
            url = (ps + '/' + am).replace(' ', '_')
        
            (am_date, am_name) = am.split(' ', 1)
            
            am_date = datetime.datetime.strptime(am_date,
                '%Y-%m-%d').strftime('%d %b %Y') # '%d %B %Y'

            if '/maps' in am:
                am_name = am_name[:-5]
                
            if am_name not in am_list:
                am_list[am_name] = dict()
                
            if '/maps' in am:
                am_list[am_name]['maps'] = f'<a href="{url}">Maps</a>'
                
            else:
                am_list[am_name]['clauses'] = f'<a href="{url}">{am_name}</a>'
                am_list[am_name]['date'] = am_date
                
    header_row   = ['Amendment Date', 'Amendment', 'Maps']
    header_align = ['right',          'left',      'center']
    
    table_data = []
    
    # Every entry should contain date, clauses and mostly maps.
    for am in am_list:
        if 'maps' in am_list[am]:
            
            # Catch the error case where maps exist not not date or clauses.
            if 'clauses' in am_list[am]:
                table_data.append([am_list[am]['date'], am_list[am]['clauses'], am_list[am]['maps']])
            
            else:
                table_data.append(['Unknown', 'Unknown', am_list[am]['maps']])
            
        else:
            table_data.append([am_list[am]['date'], am_list[am]['clauses'], ' '])

    table_html = gen_table(table_data, header_row, header_align=header_align)

    return respond.result(table_html, title=ps)


def amendment(http, api_url, req_method, req_query, ps, am):
    
    #pub_date = req_query.get('pub_date', '')
    
    print('[content.py] amendment', ps, am)
    
    if ps == 'Delatite':
        ps_name = 'Delatite (historical)'
        
    else:
        ps_name = ps
        
    if '/maps' in am:
        am_name = am[:-5]
        maps = ' Maps'
        header_row   = ['Map Number', 'Map Type', 'Size']
        header_align = ['left',       'left',     'right']
        
    else:
        am_name = am
        maps = ''
        header_row   = ['Clause',     'Clause Type', 'Size']
        header_align = ['left',       'left',        'right']
        
    (am_date, am_name2) = am_name.split(' ', 1)
        
    title = f'{ps} {am_name2} ({am_date}) {maps}'


    item = cumberland_api(http, api_url, 'amendment', arguments=[ps, am])
    files = item['files']
    pub_date = item['pub_date'].split(' ')[0]

    table_data = []
    
    for file in files:

        if '/maps' in am:
            result = analyse_map.map(file['name'], ps)
            name = result['map']
            description = result['description']
            
        else:
            result = analyse_clause.clause(file['name'], am_date)
            name = result['clause']
            description = result['description']
        
        url = 'https://cumberland-files.s3.amazonaws.com/' + file['hash'] + '.pdf'
        #name = file['name']
        
        file_size_mb = file['size'] / 1000000
        file_size = f'{file_size_mb:,.02f} MB'
 
        table_data.append([f'<a href="{url}">{name}</a>', description, file_size])

    table_html = gen_table(table_data, header_row, header_align=header_align)
    
    html = table_html + '<p>Published to the web: ' + pub_date + '</p>'

    return respond.result(html, title=title)
    
    