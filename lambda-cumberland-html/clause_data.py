#! python3

# clause_data.py
#Victoria Planning Provisions JSON Index post VC148

post_vc148_clause_data = {
    "metadata": {
        "notes": "Fixed bad data: 73.00 should be 73; extra zero removed in 19.02-06S 19.03-03S 19.03-04S.", 
        "source": "https://www.planning.vic.gov.au/schemes-and-amendments/browse-planning-scheme/planning-scheme?f.Scheme%7CplanningSchemeName=VPPS", 
        "timestamp": "2019-09-17T16:44:06", 
        "title": "Victoria Planning Provisions JSON Index post VC148"
    },
    "clauses": {
        "00": {
            "id": "00", 
            "level": 1, 
            "title": "Purpose and Vision"
        }, 
        "01": {
            "id": "01", 
            "level": 1, 
            "title": "Purposes of this Planning Scheme"
        }, 
        "10": {
            "id": "10", 
            "level": 1, 
            "title": "Planning Policy Framework"
        }, 
        "11": {
            "id": "11", 
            "level": 1, 
            "title": "Settlement"
        }, 
        "11.01": {
            "id": "11.01", 
            "level": 2, 
            "title": "Victoria"
        }, 
        "11.01-1R": {
            "id": "11.01-1R", 
            "level": 3, 
            "title": "Settlement - Wimmera Southern Mallee"
        }, 
        "11.01-1S": {
            "id": "11.01-1S", 
            "level": 3, 
            "title": "Settlement"
        }, 
        "11.02": {
            "id": "11.02", 
            "level": 2, 
            "title": "Managing Growth"
        }, 
        "11.02-1S": {
            "id": "11.02-1S", 
            "level": 3, 
            "title": "Supply of urban land"
        }, 
        "11.02-2S": {
            "id": "11.02-2S", 
            "level": 3, 
            "title": "Structure planning"
        }, 
        "11.02-3S": {
            "id": "11.02-3S", 
            "level": 3, 
            "title": "Sequencing of development"
        }, 
        "11.03": {
            "id": "11.03", 
            "level": 2, 
            "title": "Planning for Places"
        }, 
        "11.03-1R": {
            "id": "11.03-1R", 
            "level": 3, 
            "title": "Activity centres - Metropolitan Melbourne"
        }, 
        "11.03-1S": {
            "id": "11.03-1S", 
            "level": 3, 
            "title": "Activity centres"
        }, 
        "11.03-2S": {
            "id": "11.03-2S", 
            "level": 3, 
            "title": "Growth areas"
        }, 
        "11.03-3S": {
            "id": "11.03-3S", 
            "level": 3, 
            "title": "Peri-urban areas"
        }, 
        "11.03-4S": {
            "id": "11.03-4S", 
            "level": 3, 
            "title": "Coastal settlement"
        }, 
        "11.03-5R": {
            "id": "11.03-5R", 
            "level": 3, 
            "title": "The Great Ocean Road region"
        }, 
        "11.03-5S": {
            "id": "11.03-5S", 
            "level": 3, 
            "title": "Distinctive areas and landscapes"
        }, 
        "11.03-6S": {
            "id": "11.03-6S", 
            "level": 3, 
            "title": "Regional and local places"
        }, 
        "12": {
            "id": "12", 
            "level": 1, 
            "title": "Environmental and Landscape Values"
        }, 
        "12.01": {
            "id": "12.01", 
            "level": 2, 
            "title": "Biodiversity"
        }, 
        "12.01-1R": {
            "id": "12.01-1R", 
            "level": 3, 
            "title": "Protection of biodiversity - Wimmera Southern Mallee"
        }, 
        "12.01-1S": {
            "id": "12.01-1S", 
            "level": 3, 
            "title": "Protection of biodiversity"
        }, 
        "12.01-2S": {
            "id": "12.01-2S", 
            "level": 3, 
            "title": "Native vegetation management"
        }, 
        "12.02": {
            "id": "12.02", 
            "level": 2, 
            "title": "Coastal areas"
        }, 
        "12.02-1S": {
            "id": "12.02-1S", 
            "level": 3, 
            "title": "Protection of coastal areas"
        }, 
        "12.02-2S": {
            "id": "12.02-2S", 
            "level": 3, 
            "title": "Coastal Crown land"
        }, 
        "12.02-3S": {
            "id": "12.02-3S", 
            "level": 3, 
            "title": "Bays"
        }, 
        "12.03": {
            "id": "12.03", 
            "level": 2, 
            "title": "Water Bodies and Wetlands"
        }, 
        "12.03-1R": {
            "id": "12.03-1R", 
            "level": 3, 
            "title": "High value water body assets - Gippsland"
        }, 
        "12.03-1S": {
            "id": "12.03-1S", 
            "level": 3, 
            "title": "River corridors, waterways, lakes and wetlands"
        }, 
        "12.04": {
            "id": "12.04", 
            "level": 2, 
            "title": "Alpine Areas"
        }, 
        "12.04-1S": {
            "id": "12.04-1S", 
            "level": 3, 
            "title": "Sustainable development in alpine areas"
        }, 
        "12.05": {
            "id": "12.05", 
            "level": 2, 
            "title": "Significant Environments and Landscapes"
        }, 
        "12.05-1S": {
            "id": "12.05-1S", 
            "level": 3, 
            "title": "Environmentally sensitive areas"
        }, 
        "12.05-2R": {
            "id": "12.05-2R", 
            "level": 3, 
            "title": "Landscapes - Central Highlands"
        }, 
        "12.05-2S": {
            "id": "12.05-2S", 
            "level": 3, 
            "title": "Landscapes"
        }, 
        "13": {
            "id": "13", 
            "level": 1, 
            "title": "Environmental Risks and Amenity"
        }, 
        "13.01": {
            "id": "13.01", 
            "level": 2, 
            "title": "Climate Change Impacts"
        }, 
        "13.01-1S": {
            "id": "13.01-1S", 
            "level": 3, 
            "title": "Natural hazards and climate change"
        }, 
        "13.01-2S": {
            "id": "13.01-2S", 
            "level": 3, 
            "title": "Coastal inundation and erosion"
        }, 
        "13.02": {
            "id": "13.02", 
            "level": 2, 
            "title": "Bushfire"
        }, 
        "13.02-1S": {
            "id": "13.02-1S", 
            "level": 3, 
            "title": "Bushfire planning"
        }, 
        "13.03": {
            "id": "13.03", 
            "level": 2, 
            "title": "Floodplains"
        }, 
        "13.03-1S": {
            "id": "13.03-1S", 
            "level": 3, 
            "title": "Floodplain management"
        }, 
        "13.04": {
            "id": "13.04", 
            "level": 2, 
            "title": "Soil Degradation"
        }, 
        "13.04-1S": {
            "id": "13.04-1S", 
            "level": 3, 
            "title": "Contaminated and potentially contaminated land"
        }, 
        "13.04-2S": {
            "id": "13.04-2S", 
            "level": 3, 
            "title": "Erosion and landslip"
        }, 
        "13.04-3S": {
            "id": "13.04-3S", 
            "level": 3, 
            "title": "Salinity"
        }, 
        "13.05": {
            "id": "13.05", 
            "level": 2, 
            "title": "Noise"
        }, 
        "13.05-1S": {
            "id": "13.05-1S", 
            "level": 3, 
            "title": "Noise abatement"
        }, 
        "13.06": {
            "id": "13.06", 
            "level": 2, 
            "title": "Air Quality"
        }, 
        "13.06-1S": {
            "id": "13.06-1S", 
            "level": 3, 
            "title": "Air quality management"
        }, 
        "13.07": {
            "id": "13.07", 
            "level": 2, 
            "title": "Amenity and Safety"
        }, 
        "13.07-1S": {
            "id": "13.07-1S", 
            "level": 3, 
            "title": "Land use compatibility"
        }, 
        "13.07-2S": {
            "id": "13.07-2S", 
            "level": 3, 
            "title": "Major hazard facilities"
        }, 
        "14": {
            "id": "14", 
            "level": 1, 
            "title": "Natural Resource Management"
        }, 
        "14.01": {
            "id": "14.01", 
            "level": 2, 
            "title": "Agriculture"
        }, 
        "14.01-1R": {
            "id": "14.01-1R", 
            "level": 3, 
            "title": "Protection of agricultural land - Metropolitan Melbourne"
        }, 
        "14.01-1S": {
            "id": "14.01-1S", 
            "level": 3, 
            "title": "Protection of agricultural land"
        }, 
        "14.01-2R": {
            "id": "14.01-2R", 
            "level": 3, 
            "title": "Agricultural productivity - Wimmera Southern Mallee"
        }, 
        "14.01-2S": {
            "id": "14.01-2S", 
            "level": 3, 
            "title": "Sustainable agricultural land use"
        }, 
        "14.01-3S": {
            "id": "14.01-3S", 
            "level": 3, 
            "title": "Forestry and timber production"
        }, 
        "14.02": {
            "id": "14.02", 
            "level": 2, 
            "title": "Water"
        }, 
        "14.02-1S": {
            "id": "14.02-1S", 
            "level": 3, 
            "title": "Catchment planning and management"
        }, 
        "14.02-2S": {
            "id": "14.02-2S", 
            "level": 3, 
            "title": "Water quality"
        }, 
        "14.03": {
            "id": "14.03", 
            "level": 2, 
            "title": "Earth and Energy Resources"
        }, 
        "14.03-1R": {
            "id": "14.03-1R", 
            "level": 3, 
            "title": "Resource exploration and extraction - Great South Coast"
        }, 
        "14.03-1S": {
            "id": "14.03-1S", 
            "level": 3, 
            "title": "Resource exploration and extraction"
        }, 
        "15": {
            "id": "15", 
            "level": 1, 
            "title": "Built Environment and Heritage"
        }, 
        "15.01": {
            "id": "15.01", 
            "level": 2, 
            "title": "Built Environment"
        }, 
        "15.01-1R": {
            "id": "15.01-1R", 
            "level": 3, 
            "title": "Urban design - Metropolitan Melbourne"
        }, 
        "15.01-1S": {
            "id": "15.01-1S", 
            "level": 3, 
            "title": "Urban design"
        }, 
        "15.01-2S": {
            "id": "15.01-2S", 
            "level": 3, 
            "title": "Building design"
        }, 
        "15.01-3S": {
            "id": "15.01-3S", 
            "level": 3, 
            "title": "Subdivision design"
        }, 
        "15.01-4R": {
            "id": "15.01-4R", 
            "level": 3, 
            "title": "Healthy neighbourhoods - Metropolitan Melbourne"
        }, 
        "15.01-4S": {
            "id": "15.01-4S", 
            "level": 3, 
            "title": "Healthy neighbourhoods"
        }, 
        "15.01-5S": {
            "id": "15.01-5S", 
            "level": 3, 
            "title": "Neighbourhood character"
        }, 
        "15.01-6S": {
            "id": "15.01-6S", 
            "level": 3, 
            "title": "Design for rural areas"
        }, 
        "15.02": {
            "id": "15.02", 
            "level": 2, 
            "title": "Sustainable Development"
        }, 
        "15.02-1S": {
            "id": "15.02-1S", 
            "level": 3, 
            "title": "Energy and resource efficiency"
        }, 
        "15.03": {
            "id": "15.03", 
            "level": 2, 
            "title": "Heritage"
        }, 
        "15.03-1S": {
            "id": "15.03-1S", 
            "level": 3, 
            "title": "Heritage conservation"
        }, 
        "15.03-2S": {
            "id": "15.03-2S", 
            "level": 3, 
            "title": "Aboriginal cultural heritage"
        }, 
        "16": {
            "id": "16", 
            "level": 1, 
            "title": "Housing"
        }, 
        "16.01": {
            "id": "16.01", 
            "level": 2, 
            "title": "Residential Development"
        }, 
        "16.01-1R": {
            "id": "16.01-1R", 
            "level": 3, 
            "title": "Integrated housing - Metropolitan Melbourne"
        }, 
        "16.01-1S": {
            "id": "16.01-1S", 
            "level": 3, 
            "title": "Integrated housing"
        }, 
        "16.01-2R": {
            "id": "16.01-2R", 
            "level": 3, 
            "title": "Housing opportunity areas - Metropolitan Melbourne"
        }, 
        "16.01-2S": {
            "id": "16.01-2S", 
            "level": 3, 
            "title": "Location of residential development"
        }, 
        "16.01-3R": {
            "id": "16.01-3R", 
            "level": 3, 
            "title": "Housing diversity - Metropolitan Melbourne"
        }, 
        "16.01-3S": {
            "id": "16.01-3S", 
            "level": 3, 
            "title": "Housing diversity"
        }, 
        "16.01-4S": {
            "id": "16.01-4S", 
            "level": 3, 
            "title": "Housing affordability"
        }, 
        "16.01-5R": {
            "id": "16.01-5R", 
            "level": 3, 
            "title": "Rural residential development - Loddon Mallee North"
        }, 
        "16.01-5S": {
            "id": "16.01-5S", 
            "level": 3, 
            "title": "Rural residential development"
        }, 
        "16.01-6S": {
            "id": "16.01-6S", 
            "level": 3, 
            "title": "Community care accommodation"
        }, 
        "16.01-7S": {
            "id": "16.01-7S", 
            "level": 3, 
            "title": "Residential aged care facilities"
        }, 
        "17": {
            "id": "17", 
            "level": 1, 
            "title": "Economic Development"
        }, 
        "17.01": {
            "id": "17.01", 
            "level": 2, 
            "title": "Employment"
        }, 
        "17.01-1R": {
            "id": "17.01-1R", 
            "level": 3, 
            "title": "Diversified economy - Hume"
        }, 
        "17.01-1S": {
            "id": "17.01-1S", 
            "level": 3, 
            "title": "Diversified economy"
        }, 
        "17.01-2R": {
            "id": "17.01-2R", 
            "level": 3, 
            "title": "Innovation and Research - Gippsland"
        }, 
        "17.01-2S": {
            "id": "17.01-2S", 
            "level": 3, 
            "title": "Innovation and Research"
        }, 
        "17.02": {
            "id": "17.02", 
            "level": 2, 
            "title": "Commercial"
        }, 
        "17.02-1R": {
            "id": "17.02-1R", 
            "level": 3, 
            "title": "Commercial centres - Gippsland"
        }, 
        "17.02-1S": {
            "id": "17.02-1S", 
            "level": 3, 
            "title": "Business"
        }, 
        "17.02-2S": {
            "id": "17.02-2S", 
            "level": 3, 
            "title": "Out-of-centre development"
        }, 
        "17.03": {
            "id": "17.03", 
            "level": 2, 
            "title": "Industry"
        }, 
        "17.03-1R": {
            "id": "17.03-1R", 
            "level": 3, 
            "title": "Industrial land supply - Gippsland"
        }, 
        "17.03-1S": {
            "id": "17.03-1S", 
            "level": 3, 
            "title": "Industrial land supply"
        }, 
        "17.03-2S": {
            "id": "17.03-2S", 
            "level": 3, 
            "title": "Industrial development siting"
        }, 
        "17.03-3S": {
            "id": "17.03-3S", 
            "level": 3, 
            "title": "State significant industrial land"
        }, 
        "17.04": {
            "id": "17.04", 
            "level": 2, 
            "title": "Tourism"
        }, 
        "17.04-1R": {
            "id": "17.04-1R", 
            "level": 3, 
            "title": "Tourism - Loddon Mallee South"
        }, 
        "17.04-1S": {
            "id": "17.04-1S", 
            "level": 3, 
            "title": "Facilitating tourism"
        }, 
        "17.04-2S": {
            "id": "17.04-2S", 
            "level": 3, 
            "title": "Coastal and maritime tourism and recreation"
        }, 
        "18": {
            "id": "18", 
            "level": 1, 
            "title": "Transport"
        }, 
        "18.01": {
            "id": "18.01", 
            "level": 2, 
            "title": "Integrated Transport"
        }, 
        "18.01-1S": {
            "id": "18.01-1S", 
            "level": 3, 
            "title": "Land use and transport planning"
        }, 
        "18.01-2R": {
            "id": "18.01-2R", 
            "level": 3, 
            "title": "Transport system - Gippsland"
        }, 
        "18.01-2S": {
            "id": "18.01-2S", 
            "level": 3, 
            "title": "Transport system"
        }, 
        "18.02": {
            "id": "18.02", 
            "level": 2, 
            "title": "Movement networks"
        }, 
        "18.02-1R": {
            "id": "18.02-1R", 
            "level": 3, 
            "title": "Sustainable personal transport - Metropolitan Melbourne"
        }, 
        "18.02-1S": {
            "id": "18.02-1S", 
            "level": 3, 
            "title": "Sustainable personal transport"
        }, 
        "18.02-2R": {
            "id": "18.02-2R", 
            "level": 3, 
            "title": "Principal Public Transport Network"
        }, 
        "18.02-2S": {
            "id": "18.02-2S", 
            "level": 3, 
            "title": "Public Transport"
        }, 
        "18.02-3S": {
            "id": "18.02-3S", 
            "level": 3, 
            "title": "Road system"
        }, 
        "18.02-4S": {
            "id": "18.02-4S", 
            "level": 3, 
            "title": "Car parking"
        }, 
        "18.03": {
            "id": "18.03", 
            "level": 2, 
            "title": "Ports"
        }, 
        "18.03-1R": {
            "id": "18.03-1R", 
            "level": 3, 
            "title": "Planning for ports - Great South Coast"
        }, 
        "18.03-1S": {
            "id": "18.03-1S", 
            "level": 3, 
            "title": "Planning for ports"
        }, 
        "18.03-2S": {
            "id": "18.03-2S", 
            "level": 3, 
            "title": "Planning for port environs"
        }, 
        "18.04": {
            "id": "18.04", 
            "level": 2, 
            "title": "Airports"
        }, 
        "18.04-1R": {
            "id": "18.04-1R", 
            "level": 3, 
            "title": "Melbourne Airport"
        }, 
        "18.04-1S": {
            "id": "18.04-1S", 
            "level": 3, 
            "title": "Planning for airports and airfields"
        }, 
        "18.05": {
            "id": "18.05", 
            "level": 2, 
            "title": "Freight"
        }, 
        "18.05-1R": {
            "id": "18.05-1R", 
            "level": 3, 
            "title": "Freight links - Loddon Mallee South"
        }, 
        "18.05-1S": {
            "id": "18.05-1S", 
            "level": 3, 
            "title": "Freight links"
        }, 
        "19": {
            "id": "19", 
            "level": 1, 
            "title": "Infrastructure"
        }, 
        "19.01": {
            "id": "19.01", 
            "level": 2, 
            "title": "Energy"
        }, 
        "19.01-1R": {
            "id": "19.01-1R", 
            "level": 3, 
            "title": "Energy supply \u2013 Loddon Mallee North"
        }, 
        "19.01-1S": {
            "id": "19.01-1S", 
            "level": 3, 
            "title": "Energy supply"
        }, 
        "19.01-2R": {
            "id": "19.01-2R", 
            "level": 3, 
            "title": "Renewable energy - Loddon Mallee South"
        }, 
        "19.01-2S": {
            "id": "19.01-2S", 
            "level": 3, 
            "title": "Renewable energy"
        }, 
        "19.01-3S": {
            "id": "19.01-3S", 
            "level": 3, 
            "title": "Pipeline infrastructure"
        }, 
        "19.02": {
            "id": "19.02", 
            "level": 2, 
            "title": "Community Infrastructure"
        }, 
        "19.02-1R": {
            "id": "19.02-1R", 
            "level": 3, 
            "title": "Health precincts - Metropolitan Melbourne"
        }, 
        "19.02-1S": {
            "id": "19.02-1S", 
            "level": 3, 
            "title": "Health facilities"
        }, 
        "19.02-2R": {
            "id": "19.02-2R", 
            "level": 3, 
            "title": "Education precincts - Metropolitan Melbourne"
        }, 
        "19.02-2S": {
            "id": "19.02-2S", 
            "level": 3, 
            "title": "Education facilities"
        }, 
        "19.02-3R": {
            "id": "19.02-3R", 
            "level": 3, 
            "title": "Cultural facilities - Metropolitan Melbourne"
        }, 
        "19.02-3S": {
            "id": "19.02-3S", 
            "level": 3, 
            "title": "Cultural Facilities"
        }, 
        "19.02-4R": {
            "id": "19.02-4R", 
            "level": 3, 
            "title": "Social and cultural infrastructure \u2013 Wimmera Southern Mallee"
        }, 
        "19.02-4S": {
            "id": "19.02-4S", 
            "level": 3, 
            "title": "Social and cultural infrastructure"
        }, 
        "19.02-5S": {
            "id": "19.02-5S", 
            "level": 3, 
            "title": "Emergency services"
        }, 
        "19.02-6R": {
            "id": "19.02-6R", 
            "level": 3, 
            "title": "Open space - Metropolitan Melbourne"
        }, 
        "19.02-6S": {
            "id": "19.02-6S", 
            "level": 3, 
            "title": "Open space"
        }, 
        "19.03": {
            "id": "19.03", 
            "level": 2, 
            "title": "Development Infrastructure"
        }, 
        "19.03-1S": {
            "id": "19.03-1S", 
            "level": 3, 
            "title": "Development and infrastructure contributions plans"
        }, 
        "19.03-2S": {
            "id": "19.03-2S", 
            "level": 3, 
            "title": "Infrastructure design and provision"
        }, 
        "19.03-3R": {
            "id": "19.03-3R", 
            "level": 3, 
            "title": "Integrated water management - Loddon Mallee South"
        }, 
        "19.03-3S": {
            "id": "19.03-3S", 
            "level": 3, 
            "title": "Integrated water management"
        }, 
        "19.03-4S": {
            "id": "19.03-4S", 
            "level": 3, 
            "title": "Telecommunications"
        }, 
        "19.03-5S": {
            "id": "19.03-5S", 
            "level": 3, 
            "title": "Waste and resource recovery"
        }, 
        "23": {
            "id": "23", 
            "level": 1, 
            "title": "Operation of the Local Planning Policy Framework (Transitional)"
        }, 
        "23.01": {
            "id": "23.01", 
            "level": 2, 
            "title": "Relation to the Planning Policy Framework"
        }, 
        "23.02": {
            "id": "23.02", 
            "level": 2, 
            "title": "Operation of the Municipal Strategic Statement"
        }, 
        "23.03": {
            "id": "23.03", 
            "level": 2, 
            "title": "Operation of the Local Planning Policies"
        }, 
        "30": {
            "id": "30", 
            "level": 1, 
            "title": "Zones"
        }, 
        "31": {
            "id": "31", 
            "level": 1, 
            "title": "[No Content]"
        }, 
        "32": {
            "id": "32", 
            "level": 1, 
            "title": "Residential Zones"
        }, 
        "32.03": {
            "id": "32.03", 
            "level": 2, 
            "title": "Low Density Residential Zone"
        }, 
        "32.04": {
            "id": "32.04", 
            "level": 2, 
            "title": "Mixed Use Zone"
        }, 
        "32.05": {
            "id": "32.05", 
            "level": 2, 
            "title": "Township Zone"
        }, 
        "32.07": {
            "id": "32.07", 
            "level": 2, 
            "title": "Residential Growth Zone"
        }, 
        "32.08": {
            "id": "32.08", 
            "level": 2, 
            "title": "General Residential Zone"
        }, 
        "32.09": {
            "id": "32.09", 
            "level": 2, 
            "title": "Neighbourhood Residential Zone"
        }, 
        "33": {
            "id": "33", 
            "level": 1, 
            "title": "Industrial Zones"
        }, 
        "33.01": {
            "id": "33.01", 
            "level": 2, 
            "title": "Industrial 1 Zone"
        }, 
        "33.02": {
            "id": "33.02", 
            "level": 2, 
            "title": "Industrial 2 Zone"
        }, 
        "33.03": {
            "id": "33.03", 
            "level": 2, 
            "title": "Industrial 3 Zone"
        }, 
        "34": {
            "id": "34", 
            "level": 1, 
            "title": "Commercial Zones"
        }, 
        "34.01": {
            "id": "34.01", 
            "level": 2, 
            "title": "Commercial 1 Zone"
        }, 
        "34.02": {
            "id": "34.02", 
            "level": 2, 
            "title": "Commercial 2 Zone"
        }, 
        "34.03": {
            "id": "34.03", 
            "level": 2, 
            "title": "Commercial 3 Zone"
        }, 
        "35": {
            "id": "35", 
            "level": 1, 
            "title": "Rural Zones"
        }, 
        "35.03": {
            "id": "35.03", 
            "level": 2, 
            "title": "Rural Living Zone"
        }, 
        "35.04": {
            "id": "35.04", 
            "level": 2, 
            "title": "Green Wedge Zone"
        }, 
        "35.05": {
            "id": "35.05", 
            "level": 2, 
            "title": "Green Wedge A Zone"
        }, 
        "35.06": {
            "id": "35.06", 
            "level": 2, 
            "title": "Rural Conservation Zone"
        }, 
        "35.07": {
            "id": "35.07", 
            "level": 2, 
            "title": "Farming Zone"
        }, 
        "35.08": {
            "id": "35.08", 
            "level": 2, 
            "title": "Rural Activity Zone"
        }, 
        "36": {
            "id": "36", 
            "level": 1, 
            "title": "Public Land Zones"
        }, 
        "36.01": {
            "id": "36.01", 
            "level": 2, 
            "title": "Public Use Zone"
        }, 
        "36.02": {
            "id": "36.02", 
            "level": 2, 
            "title": "Public Park and Recreation Zone"
        }, 
        "36.03": {
            "id": "36.03", 
            "level": 2, 
            "title": "Public Conservation and Resource Zone"
        }, 
        "36.04": {
            "id": "36.04", 
            "level": 2, 
            "title": "Road Zone"
        }, 
        "37": {
            "id": "37", 
            "level": 1, 
            "title": "Special Purpose Zone"
        }, 
        "37.01": {
            "id": "37.01", 
            "level": 2, 
            "title": "Special Use Zone"
        }, 
        "37.02": {
            "id": "37.02", 
            "level": 2, 
            "title": "Comprehensive Development Zone"
        }, 
        "37.03": {
            "id": "37.03", 
            "level": 2, 
            "title": "Urban Floodway Zone"
        }, 
        "37.04": {
            "id": "37.04", 
            "level": 2, 
            "title": "Capital City Zone"
        }, 
        "37.05": {
            "id": "37.05", 
            "level": 2, 
            "title": "Docklands Zone"
        }, 
        "37.06": {
            "id": "37.06", 
            "level": 2, 
            "title": "Priority Development Zone"
        }, 
        "37.07": {
            "id": "37.07", 
            "level": 2, 
            "title": "Urban Growth Zone"
        }, 
        "37.08": {
            "id": "37.08", 
            "level": 2, 
            "title": "Activity Centre Zone"
        }, 
        "37.09": {
            "id": "37.09", 
            "level": 2, 
            "title": "Port Zone"
        }, 
        "40": {
            "id": "40", 
            "level": 1, 
            "title": "Overlays"
        }, 
        "41": {
            "id": "41", 
            "level": 1, 
            "title": "[No Content]"
        }, 
        "42": {
            "id": "42", 
            "level": 1, 
            "title": "Environmental and Landscape Overlays"
        }, 
        "42.01": {
            "id": "42.01", 
            "level": 2, 
            "title": "Environmental Significance Overlay"
        }, 
        "42.02": {
            "id": "42.02", 
            "level": 2, 
            "title": "Vegetation Protection Overlay"
        }, 
        "42.03": {
            "id": "42.03", 
            "level": 2, 
            "title": "Significant Landscape Overlay"
        }, 
        "43": {
            "id": "43", 
            "level": 1, 
            "title": "Heritage and Built Form Overlays"
        }, 
        "43.01": {
            "id": "43.01", 
            "level": 2, 
            "title": "Heritage Overlay"
        }, 
        "43.02": {
            "id": "43.02", 
            "level": 2, 
            "title": "Design and Development Overlay"
        }, 
        "43.03": {
            "id": "43.03", 
            "level": 2, 
            "title": "Incorporated Plan Overlay"
        }, 
        "43.04": {
            "id": "43.04", 
            "level": 2, 
            "title": "Development Plan Overlay"
        }, 
        "43.05": {
            "id": "43.05", 
            "level": 2, 
            "title": "Neighbourhood Character Overlay"
        }, 
        "44": {
            "id": "44", 
            "level": 1, 
            "title": "Land Management Overlays"
        }, 
        "44.01": {
            "id": "44.01", 
            "level": 2, 
            "title": "Erosion Management Overlay"
        }, 
        "44.02": {
            "id": "44.02", 
            "level": 2, 
            "title": "Salinity Management Overlay"
        }, 
        "44.03": {
            "id": "44.03", 
            "level": 2, 
            "title": "Floodway Overlay"
        }, 
        "44.04": {
            "id": "44.04", 
            "level": 2, 
            "title": "Land Subject to Inundation Overlay"
        }, 
        "44.05": {
            "id": "44.05", 
            "level": 2, 
            "title": "Special Building Overlay"
        }, 
        "44.06": {
            "id": "44.06", 
            "level": 2, 
            "title": "Bushfire Management Overlay"
        }, 
        "44.07": {
            "id": "44.07", 
            "level": 2, 
            "title": "State Resource Overlay"
        }, 
        "45": {
            "id": "45", 
            "level": 1, 
            "title": "Other Overlays"
        }, 
        "45.01": {
            "id": "45.01", 
            "level": 2, 
            "title": "Public Acquisition Overlay"
        }, 
        "45.02": {
            "id": "45.02", 
            "level": 2, 
            "title": "Airport Environs Overlay"
        }, 
        "45.03": {
            "id": "45.03", 
            "level": 2, 
            "title": "Environmental Audit Overlay"
        }, 
        "45.04": {
            "id": "45.04", 
            "level": 2, 
            "title": "Road Closure Overlay"
        }, 
        "45.05": {
            "id": "45.05", 
            "level": 2, 
            "title": "Restructure Overlay"
        }, 
        "45.06": {
            "id": "45.06", 
            "level": 2, 
            "title": "Development Contribution Plan Overlay"
        }, 
        "45.07": {
            "id": "45.07", 
            "level": 2, 
            "title": "City Link Project Overlay"
        }, 
        "45.08": {
            "id": "45.08", 
            "level": 2, 
            "title": "Melbourne Airport Environs Overlay"
        }, 
        "45.09": {
            "id": "45.09", 
            "level": 2, 
            "title": "Parking Overlay"
        }, 
        "45.10": {
            "id": "45.10", 
            "level": 2, 
            "title": "Infrastructure Contributions Plan Overlay"
        }, 
        "45.11": {
            "id": "45.11", 
            "level": 2, 
            "title": "Infrastructure Contributions Overlay"
        }, 
        "45.12": {
            "id": "45.12", 
            "level": 2, 
            "title": "Specific Controls Overlay"
        }, 
        "50": {
            "id": "50", 
            "level": 1, 
            "title": "Particular Provisions"
        }, 
        "51": {
            "id": "51", 
            "level": 1, 
            "title": "Provisions that Apply only to a Specified Area"
        }, 
        "51.01": {
            "id": "51.01", 
            "level": 2, 
            "title": "Specific Sites and Exclusions"
        }, 
        "51.02": {
            "id": "51.02", 
            "level": 2, 
            "title": "Metropolitan Green Wedge Land: Core Planning Provisions"
        }, 
        "51.03": {
            "id": "51.03", 
            "level": 2, 
            "title": "Upper Yarra Valley & Dandenong Ranges Regional Strategy Plan"
        }, 
        "51.04": {
            "id": "51.04", 
            "level": 2, 
            "title": "Melbourne Airport Environs Strategy Plan"
        }, 
        "51.05": {
            "id": "51.05", 
            "level": 2, 
            "title": "Williamstown Shipyard Site Strategy Plan"
        }, 
        "52": {
            "id": "52", 
            "level": 1, 
            "title": "Provisions that Require, Enable or Exempt a Permit"
        }, 
        "52.01": {
            "id": "52.01", 
            "level": 2, 
            "title": "[No Content]"
        }, 
        "52.02": {
            "id": "52.02", 
            "level": 2, 
            "title": "Easements, Restrictions and Reserves"
        }, 
        "52.03": {
            "id": "52.03", 
            "level": 2, 
            "title": "[No Content]"
        }, 
        "52.04": {
            "id": "52.04", 
            "level": 2, 
            "title": "Satellite Dish"
        }, 
        "52.05": {
            "id": "52.05", 
            "level": 2, 
            "title": "Signs"
        }, 
        "52.06": {
            "id": "52.06", 
            "level": 2, 
            "title": "Car Parking"
        }, 
        "52.07": {
            "id": "52.07", 
            "level": 2, 
            "title": "[No Content]"
        }, 
        "52.08": {
            "id": "52.08", 
            "level": 2, 
            "title": "Earth and Energy Resources Industry"
        }, 
        "52.09": {
            "id": "52.09", 
            "level": 2, 
            "title": "Stone Extraction and Extractive Industry Interest Areas"
        }, 
        "52.10": {
            "id": "52.10", 
            "level": 2, 
            "title": "[No Content]"
        }, 
        "52.11": {
            "id": "52.11", 
            "level": 2, 
            "title": "Home Based Business"
        }, 
        "52.12": {
            "id": "52.12", 
            "level": 2, 
            "title": "Bushfire Protection: Exemptions"
        }, 
        "52.13": {
            "id": "52.13", 
            "level": 2, 
            "title": "2009 Bushfire: Recovery Exemptions"
        }, 
        "52.14": {
            "id": "52.14", 
            "level": 2, 
            "title": "2009 Bushfire Replacement Buildings"
        }, 
        "52.15": {
            "id": "52.15", 
            "level": 2, 
            "title": "Heliport and Helicopter Landing Site"
        }, 
        "52.16": {
            "id": "52.16", 
            "level": 2, 
            "title": "Native Vegetation Precinct Plan"
        }, 
        "52.17": {
            "id": "52.17", 
            "level": 2, 
            "title": "Native Vegetation"
        }, 
        "52.18": {
            "id": "52.18", 
            "level": 2, 
            "title": "[No Content]"
        }, 
        "52.19": {
            "id": "52.19", 
            "level": 2, 
            "title": "Telecommunications Facility"
        }, 
        "52.20": {
            "id": "52.20", 
            "level": 2, 
            "title": "[No Content]"
        }, 
        "52.21": {
            "id": "52.21", 
            "level": 2, 
            "title": "Private Tennis Court"
        }, 
        "52.22": {
            "id": "52.22", 
            "level": 2, 
            "title": "Community Care Accommodation"
        }, 
        "52.23": {
            "id": "52.23", 
            "level": 2, 
            "title": "Rooming House"
        }, 
        "52.25": {
            "id": "52.25", 
            "level": 2, 
            "title": "Crematorium"
        }, 
        "52.26": {
            "id": "52.26", 
            "level": 2, 
            "title": "[No Content]"
        }, 
        "52.27": {
            "id": "52.27", 
            "level": 2, 
            "title": "Licensed Premises"
        }, 
        "52.28": {
            "id": "52.28", 
            "level": 2, 
            "title": "Gaming"
        }, 
        "52.29": {
            "id": "52.29", 
            "level": 2, 
            "title": "Land Adjacent to a Road Zone, Category 1, or a Public Acquisition Overlay for a Category 1 Road"
        }, 
        "52.30": {
            "id": "52.30", 
            "level": 2, 
            "title": "[No Content]"
        }, 
        "52.31": {
            "id": "52.31", 
            "level": 2, 
            "title": "[No Content]"
        }, 
        "52.32": {
            "id": "52.32", 
            "level": 2, 
            "title": "Wind Energy Facility"
        }, 
        "52.33": {
            "id": "52.33", 
            "level": 2, 
            "title": "Post Boxes and Dry Stone Walls"
        }, 
        "52.34": {
            "id": "52.34", 
            "level": 2, 
            "title": "Bicycle Facilities"
        }, 
        "53": {
            "id": "53", 
            "level": 1, 
            "title": "General Requirements and Performance Standards"
        }, 
        "53.01": {
            "id": "53.01", 
            "level": 2, 
            "title": "Public Open Space Contribution and Subdivision"
        }, 
        "53.02": {
            "id": "53.02", 
            "level": 2, 
            "title": "Bushfire Planning"
        }, 
        "53.03": {
            "id": "53.03", 
            "level": 2, 
            "title": "Brothels"
        }, 
        "53.04": {
            "id": "53.04", 
            "level": 2, 
            "title": "Convenience Restaurant and Take-Away Food Premises"
        }, 
        "53.05": {
            "id": "53.05", 
            "level": 2, 
            "title": "Freeway Service Centre"
        }, 
        "53.06": {
            "id": "53.06", 
            "level": 2, 
            "title": "Live Music and Entertainment Noise"
        }, 
        "53.07": {
            "id": "53.07", 
            "level": 2, 
            "title": "Shipping Container Storage"
        }, 
        "53.08": {
            "id": "53.08", 
            "level": 2, 
            "title": "Cattle Feedlot"
        }, 
        "53.09": {
            "id": "53.09", 
            "level": 2, 
            "title": "Poultry Farm"
        }, 
        "53.10": {
            "id": "53.10", 
            "level": 2, 
            "title": "Uses with Adverse Amenity Potential"
        }, 
        "53.11": {
            "id": "53.11", 
            "level": 2, 
            "title": "Timber Production"
        }, 
        "53.12": {
            "id": "53.12", 
            "level": 2, 
            "title": "Racing Dog Keeping and Training"
        }, 
        "53.13": {
            "id": "53.13", 
            "level": 2, 
            "title": "Renewable Energy Facility (Other Than Wind Energy Facility and Geothermal Energy Extraction)"
        }, 
        "53.14": {
            "id": "53.14", 
            "level": 2, 
            "title": "Resource Recovery"
        }, 
        "53.15": {
            "id": "53.15", 
            "level": 2, 
            "title": "Statement of Underlying Provisions"
        }, 
        "53.16": {
            "id": "53.16", 
            "level": 2, 
            "title": "Pig Farm"
        }, 
        "53.17": {
            "id": "53.17", 
            "level": 2, 
            "title": "Residential Aged Care Facility"
        }, 
        "53.18": {
            "id": "53.18", 
            "level": 2, 
            "title": "Stormwater Management in Urban Development"
        }, 
        "54": {
            "id": "54", 
            "level": 1, 
            "title": "One Dwelling on a Lot"
        }, 
        "54.01": {
            "id": "54.01", 
            "level": 2, 
            "title": "Neighbourhood and Site Description and Design Response"
        }, 
        "54.02": {
            "id": "54.02", 
            "level": 2, 
            "title": "Neighbourhood Character"
        }, 
        "54.03": {
            "id": "54.03", 
            "level": 2, 
            "title": "Site Layout and Building Massing"
        }, 
        "54.04": {
            "id": "54.04", 
            "level": 2, 
            "title": "Amenity Impacts"
        }, 
        "54.05": {
            "id": "54.05", 
            "level": 2, 
            "title": "On-Site Amenity and Facilities"
        }, 
        "54.06": {
            "id": "54.06", 
            "level": 2, 
            "title": "Detailed Design"
        }, 
        "55": {
            "id": "55", 
            "level": 1, 
            "title": "Two or more Dwellings on a lot and Residential Buildings"
        }, 
        "55.01": {
            "id": "55.01", 
            "level": 2, 
            "title": "Neighbourhood and Site Description and Design Response"
        }, 
        "55.02": {
            "id": "55.02", 
            "level": 2, 
            "title": "Neighbourhood Character and Infrastructure"
        }, 
        "55.03": {
            "id": "55.03", 
            "level": 2, 
            "title": "Site Layout and Building Massing"
        }, 
        "55.04": {
            "id": "55.04", 
            "level": 2, 
            "title": "Amenity Impacts"
        }, 
        "55.05": {
            "id": "55.05", 
            "level": 2, 
            "title": "On-Site Amenity and Facilities"
        }, 
        "55.06": {
            "id": "55.06", 
            "level": 2, 
            "title": "Detailed Design"
        }, 
        "55.07": {
            "id": "55.07", 
            "level": 2, 
            "title": "Apartment Developments"
        }, 
        "56": {
            "id": "56", 
            "level": 1, 
            "title": "Residential Subdivision"
        }, 
        "56.01": {
            "id": "56.01", 
            "level": 2, 
            "title": "Subdivision Site and Context Description and Design Response"
        }, 
        "56.02": {
            "id": "56.02", 
            "level": 2, 
            "title": "Policy Implementation"
        }, 
        "56.03": {
            "id": "56.03", 
            "level": 2, 
            "title": "Liveable and Sustainable Communities"
        }, 
        "56.04": {
            "id": "56.04", 
            "level": 2, 
            "title": "Lot Design"
        }, 
        "56.05": {
            "id": "56.05", 
            "level": 2, 
            "title": "Urban Landscape"
        }, 
        "56.06": {
            "id": "56.06", 
            "level": 2, 
            "title": "Access and Mobility Management"
        }, 
        "56.07": {
            "id": "56.07", 
            "level": 2, 
            "title": "Integrated Water Management"
        }, 
        "56.08": {
            "id": "56.08", 
            "level": 2, 
            "title": "Site Management"
        }, 
        "56.09": {
            "id": "56.09", 
            "level": 2, 
            "title": "Utilities"
        }, 
        "57": {
            "id": "57", 
            "level": 1, 
            "title": "[No Content]"
        }, 
        "58": {
            "id": "58", 
            "level": 1, 
            "title": "Apartment Developments"
        }, 
        "58.01": {
            "id": "58.01", 
            "level": 2, 
            "title": "Urban Context Report and Design Response"
        }, 
        "58.02": {
            "id": "58.02", 
            "level": 2, 
            "title": "Urban Context"
        }, 
        "58.03": {
            "id": "58.03", 
            "level": 2, 
            "title": "Site Layout"
        }, 
        "58.04": {
            "id": "58.04", 
            "level": 2, 
            "title": "Amenity Impacts"
        }, 
        "58.05": {
            "id": "58.05", 
            "level": 2, 
            "title": "On-Site Amenity and Facilities"
        }, 
        "58.06": {
            "id": "58.06", 
            "level": 2, 
            "title": "Detailed Design"
        }, 
        "58.07": {
            "id": "58.07", 
            "level": 2, 
            "title": "Internal Amenity"
        }, 
        "59": {
            "id": "59", 
            "level": 1, 
            "title": "VicSmart Applications and Requirements"
        }, 
        "59.01": {
            "id": "59.01", 
            "level": 2, 
            "title": "Realign the Common Boundary Between Two Lots"
        }, 
        "59.02": {
            "id": "59.02", 
            "level": 2, 
            "title": "Subdivision of Buildings and Car Parking spaces"
        }, 
        "59.03": {
            "id": "59.03", 
            "level": 2, 
            "title": "Front Fence in a Residential Zone"
        }, 
        "59.04": {
            "id": "59.04", 
            "level": 2, 
            "title": "Buildings and Works in a Zone (Except a Rural Zone)"
        }, 
        "59.05": {
            "id": "59.05", 
            "level": 2, 
            "title": "Buildings and Works in an Overlay"
        }, 
        "59.06": {
            "id": "59.06", 
            "level": 2, 
            "title": "Remove, Destroy or Lop a Tree"
        }, 
        "59.07": {
            "id": "59.07", 
            "level": 2, 
            "title": "Applications Under Heritage Overlay"
        }, 
        "59.08": {
            "id": "59.08", 
            "level": 2, 
            "title": "Applications Under a Special Building Overlay"
        }, 
        "59.09": {
            "id": "59.09", 
            "level": 2, 
            "title": "Sign"
        }, 
        "59.10": {
            "id": "59.10", 
            "level": 2, 
            "title": "Car Parking"
        }, 
        "59.11": {
            "id": "59.11", 
            "level": 2, 
            "title": "[No Content]"
        }, 
        "59.12": {
            "id": "59.12", 
            "level": 2, 
            "title": "Two Lot Subdivision in a Rural Zone"
        }, 
        "59.13": {
            "id": "59.13", 
            "level": 2, 
            "title": "Buildings and Works in a Rural Zone"
        }, 
        "59.14": {
            "id": "59.14", 
            "level": 2, 
            "title": "Extension to One Dwelling on a Lot in a Residential Zone"
        }, 
        "59.15": {
            "id": "59.15", 
            "level": 2, 
            "title": "Local VicSmart Applications"
        }, 
        "59.16": {
            "id": "59.16", 
            "level": 2, 
            "title": "Information Requirements and Decision Guidelines for Local VicSmart Applications"
        }, 
        "60": {
            "id": "60", 
            "level": 1, 
            "title": "General Provisions"
        }, 
        "61": {
            "id": "61", 
            "level": 1, 
            "title": "[No Content]"
        }, 
        "62": {
            "id": "62", 
            "level": 1, 
            "title": "General Exemptions"
        }, 
        "62.01": {
            "id": "62.01", 
            "level": 2, 
            "title": "Uses not Requiring a Permit"
        }, 
        "62.02": {
            "id": "62.02", 
            "level": 2, 
            "title": "Buildings and Works"
        }, 
        "62.03": {
            "id": "62.03", 
            "level": 2, 
            "title": "Events on Public Land"
        }, 
        "62.04": {
            "id": "62.04", 
            "level": 2, 
            "title": "Subdivisions not Requiring a Permit"
        }, 
        "62.05": {
            "id": "62.05", 
            "level": 2, 
            "title": "Demolition"
        }, 
        "63": {
            "id": "63", 
            "level": 1, 
            "title": "Existing Uses"
        }, 
        "63.01": {
            "id": "63.01", 
            "level": 2, 
            "title": "Extent of Existing Use Rights"
        }, 
        "63.02": {
            "id": "63.02", 
            "level": 2, 
            "title": "Characterisation of Use"
        }, 
        "63.03": {
            "id": "63.03", 
            "level": 2, 
            "title": "Effect of Definitions on Existing Use Rights"
        }, 
        "63.04": {
            "id": "63.04", 
            "level": 2, 
            "title": "Section 1 Uses"
        }, 
        "63.05": {
            "id": "63.05", 
            "level": 2, 
            "title": "Sections 2 and 3 Uses"
        }, 
        "63.06": {
            "id": "63.06", 
            "level": 2, 
            "title": "Expiration of Existing Use Rights"
        }, 
        "63.07": {
            "id": "63.07", 
            "level": 2, 
            "title": "Compliance with Codes of Practice"
        }, 
        "63.08": {
            "id": "63.08", 
            "level": 2, 
            "title": "Alternative Use"
        }, 
        "63.09": {
            "id": "63.09", 
            "level": 2, 
            "title": "Shop Conditions"
        }, 
        "63.10": {
            "id": "63.10", 
            "level": 2, 
            "title": "Damaged or Destroyed Buildings or Works"
        }, 
        "63.11": {
            "id": "63.11", 
            "level": 2, 
            "title": "Proof of Continuous Use"
        }, 
        "63.12": {
            "id": "63.12", 
            "level": 2, 
            "title": "Decision Guidelines"
        }, 
        "64": {
            "id": "64", 
            "level": 1, 
            "title": "General Provisions for Use and Development of Land"
        }, 
        "64.01": {
            "id": "64.01", 
            "level": 2, 
            "title": "Land Used for More Than One Use"
        }, 
        "64.02": {
            "id": "64.02", 
            "level": 2, 
            "title": "Land Used in Conjunction with Another Use"
        }, 
        "64.03": {
            "id": "64.03", 
            "level": 2, 
            "title": "Subdivision of Land in More Than One Zone"
        }, 
        "65": {
            "id": "65", 
            "level": 1, 
            "title": "Decision Guidelines"
        }, 
        "65.01": {
            "id": "65.01", 
            "level": 2, 
            "title": "Approval of An Application or Plan"
        }, 
        "65.02": {
            "id": "65.02", 
            "level": 2, 
            "title": "Approval of An Application to Subdivide Land"
        }, 
        "66": {
            "id": "66", 
            "level": 1, 
            "title": "Referral and Notice Provisions"
        }, 
        "66.01": {
            "id": "66.01", 
            "level": 2, 
            "title": "Subdivision Referrals"
        }, 
        "66.02": {
            "id": "66.02", 
            "level": 2, 
            "title": "Use and Development Referrals"
        }, 
        "66.03": {
            "id": "66.03", 
            "level": 2, 
            "title": "Referral of Permit Applications Under Other State Standard Provisions"
        }, 
        "66.04": {
            "id": "66.04", 
            "level": 2, 
            "title": "Referral of Permit Applications Under Local Provisions"
        }, 
        "66.05": {
            "id": "66.05", 
            "level": 2, 
            "title": "Notice of Permit Applications Under State Standard Provisions"
        }, 
        "66.06": {
            "id": "66.06", 
            "level": 2, 
            "title": "Notice of Permit Applications Under Local Provisions"
        }, 
        "67": {
            "id": "67", 
            "level": 1, 
            "title": "Applications Under Section 96 of the Act"
        }, 
        "67.01": {
            "id": "67.01", 
            "level": 2, 
            "title": "Exemptions from Section 96(1) And 96(2) of the Act"
        }, 
        "67.02": {
            "id": "67.02", 
            "level": 2, 
            "title": "Notice Requirements"
        }, 
        "67.03": {
            "id": "67.03", 
            "level": 2, 
            "title": "Notice Requirements - Native Vegetation"
        }, 
        "67.04": {
            "id": "67.04", 
            "level": 2, 
            "title": "Notice Exemption"
        }, 
        "70": {
            "id": "70", 
            "level": 1, 
            "title": "Operational Provisions"
        }, 
        "71": {
            "id": "71", 
            "level": 1, 
            "title": "Operation of this Planning Scheme"
        }, 
        "71.01": {
            "id": "71.01", 
            "level": 2, 
            "title": "Operation of the Municipal Planning Strategy"
        }, 
        "71.02": {
            "id": "71.02", 
            "level": 2, 
            "title": "Operation of the Planning Policy Framework"
        }, 
        "71.03": {
            "id": "71.03", 
            "level": 2, 
            "title": "Operation of Zones"
        }, 
        "71.04": {
            "id": "71.04", 
            "level": 2, 
            "title": "Operation of Overlays"
        }, 
        "71.05": {
            "id": "71.05", 
            "level": 2, 
            "title": "Operation of Particular Provisions"
        }, 
        "71.06": {
            "id": "71.06", 
            "level": 2, 
            "title": "Operation of VicSmart Planning Applications and Process"
        }, 
        "72": {
            "id": "72", 
            "level": 1, 
            "title": "Administration and Enforcement of this Scheme"
        }, 
        "72.01": {
            "id": "72.01", 
            "level": 2, 
            "title": "Responsible Authority for this Planning Scheme"
        }, 
        "72.02": {
            "id": "72.02", 
            "level": 2, 
            "title": "What Area is Covered by the Planning Scheme"
        }, 
        "72.03": {
            "id": "72.03", 
            "level": 2, 
            "title": "What Does this Scheme Consist of?"
        }, 
        "72.04": {
            "id": "72.04", 
            "level": 2, 
            "title": "Documents Incorporated in this Planning Scheme"
        }, 
        "72.05": {
            "id": "72.05", 
            "level": 2, 
            "title": "When Did this Planning Scheme Begin?"
        }, 
        "72.06": {
            "id": "72.06", 
            "level": 2, 
            "title": "Effect of this Planning Scheme"
        }, 
        "72.07": {
            "id": "72.07", 
            "level": 2, 
            "title": "Determination of Boundaries"
        }, 
        "72.08": {
            "id": "72.08", 
            "level": 2, 
            "title": "Background documents"
        }, 
        "73": {
            "id": "73", 
            "level": 2, 
            "title": "Meaning of Terms"
        }, 
        "73.01": {
            "id": "73.01", 
            "level": 2, 
            "title": "General Terms"
        }, 
        "73.02": {
            "id": "73.02", 
            "level": 2, 
            "title": "Sign Terms"
        }, 
        "73.03": {
            "id": "73.03", 
            "level": 2, 
            "title": "Land Use Terms"
        }, 
        "73.04": {
            "id": "73.04", 
            "level": 2, 
            "title": "Nesting Diagrams"
        }, 
        "74": {
            "id": "74", 
            "level": 1, 
            "title": "Strategic Implementation"
        }, 
        "74.01": {
            "id": "74.01", 
            "level": 2, 
            "title": "Application of Zones, Overlays and Provisions"
        }, 
        "74.02": {
            "id": "74.02", 
            "level": 2, 
            "title": "Further Strategic Work"
        }, 
        "amlist": {
            "id": "amlist", 
            "level": 1, 
            "title": "List Of Amendments To The Victoria Planning Provisions"
        }
    }
}