# Test Results (02-10/03/2020)

The AWS Lambda API is used by the client script (```cumberland.py```) and the Lambda HTML publication functions.

Only representative lines are shown for long lists.

## Client (cumberland.py)

### scan

Command:

```py H:\cumberland.py scan 2020-02-01```

Result:

```
0:00:00.01 XI001 Script (v12) started at 2020-03-10 13:44 (using config file H:cumberland.config)
0:00:00.03 SI001 Processing directories since: 2020-02-01
0:00:00.06 SI004 Processing Alpine
0:00:00.71 SI004 Processing Alpine Resorts
0:00:01.24 SI004 Processing Ararat
...
0:01:23.88 SI004 Processing Yarriambiack
0:01:24.59 LI001 Processing planning scheme prefix: 2020-02-01
0:01:24.66 LI002 Amendments to process: 4

0:01:24.68 XI006 Counters:
0:01:24.69 XI007 Ignored-not-ps 2
0:01:24.71 XI007 Scanned-ps+vpp 84
0:01:24.73 XI007 Scanned-source-am 21,144
0:01:24.74 XI007 Ignored-am-before 20,783
0:01:24.76 XI007 Am-already-processed 226
0:01:24.77 XI007 Am-to-process 226
0:01:24.79 XI007 Ignored-not-am 53
0:01:24.80 XI007 Upload-ignore-not-selected 4
```

Command (scan all):

```py H:\cumberland.py scan 1999-01-01```

Result:

```
0:00:00.01 XI001 Script (v11) started at 2020-03-10 09:31 (using config file H:cumberland.config)
0:00:00.03 SI001 Processing directories since: 1999-01-01
0:00:00.10 SI004 Processing Alpine
0:00:55.43 SI004 Processing Alpine Resorts
0:01:44.61 SI004 Processing Ararat
0:02:53.61 SI004 Processing Ballarat
0:04:53.05 SI004 Processing Banyule
0:06:24.78 SI004 Processing Bass Coast
0:08:25.32 SI004 Processing Baw Baw
0:10:11.87 SI004 Processing Bayside
0:11:27.05 SI004 Processing Benalla
0:12:22.40 SI004 Processing Boroondara
0:14:32.43 SI006 Ignore directories that don't start with a year (Boroondara): Boroondara
0:14:32.44 SI006 Ignore directories that don't start with a year (Boroondara): Planning Scheme Error
0:14:32.46 SI004 Processing Brimbank
0:18:08.09 SI012 Adding directory Brimbank/2018-11-29 C188(Part 1)(revised) to the database (files=486)
0:18:35.86 SI012 Adding directory Brimbank/2019-03-21 C205brim/maps to the database (files=112)
0:20:16.79 SI012 Adding directory Brimbank/2019-03-21 C205brim to the database (files=486)
0:20:43.80 SI012 Adding directory Brimbank/2019-09-20 C202brim/maps to the database (files=112)
...
0:51:47.92 SI004 Processing Greater Geelong
0:59:01.27 SI012 Adding directory Greater Geelong/2019-11-14 C406ggee to the database (files=662)
1:00:23.86 SI012 Adding directory Greater Geelong/2019-11-21 C405ggee/maps to the database (files=547)
1:02:48.35 SI012 Adding directory Greater Geelong/2019-11-21 C405ggee to the database (files=662)

...
1:52:30.77 SI004 Processing Maribyrnong
1:54:05.92 SI004 Processing Maroondah
1:55:18.39 SI004 Processing Melbourne
1:56:44.38 SI008 Ignore directories that contain 'not valid' (Melbourne): 2014-07-01-VC116(not valid)

...
3:34:33.60 SI012 Adding directory Mornington Peninsula/2018-11-08 C250morn to the database (files=500)
3:36:20.14 SI012 Adding directory Mornington Peninsula/2018-11-08 C250morn(revised)/maps to the database (files=501)
3:38:34.23 SI012 Adding directory Mornington Peninsula/2018-11-08 C250morn(revised) to the database (files=555)
...
4:22:12.59 SI012 Adding directory Mount Alexander/2018-11-15 C85(revised)/maps to the database (files=267)
4:23:59.50 SI012 Adding directory Mount Alexander/2018-11-15 C85(revised) to the database (files=458)
4:24:06.64 SI004 Processing Moyne
4:24:23.79 SI012 Adding directory Moyne/2000-12-14 VC10/maps to the database (files=83)
4:25:29.33 SI004 Processing Murrindindi
4:25:50.21 SI012 Adding directory Murrindindi/2000-08-17 VC8/maps to the database (files=105)
4:27:32.28 SI012 Adding directory Murrindindi/2018-11-15 GC100(revised)/maps to the database (files=183)
...
```

Command (scan all):

```py H:\cumberland.py scan 1999-01-01```

Result:

```
0:00:00.01 XI001 Script (v12) started at 2020-03-10 14:52 (using config file H:cumberland.config)
0:00:00.03 SI001 Processing directories since: 1999-01-01
0:00:00.10 SI004 Processing Alpine
0:00:52.06 SI004 Processing Alpine Resorts
...

```



### upload

Command:

```py H:\cumberland.py upload all```

```
0:00:00.01 XI001 Script (v12) started at 2020-03-10 11:22 (using config file H:cumberland.config)
0:00:00.20 LI002 Amendments to process: 8
0:00:08.88 LI011 Greater Shepparton/2019-10-17 C220gshe/maps No files to upload for Greater Shepparton/2019-10-17-C220gshe/2019-10-17-C220gshe/Maps.
0:00:28.52 LI011 Greater Shepparton/2019-10-17 C220gshe No files to upload for Greater Shepparton/2019-10-17-C220gshe/2019-10-17-C220gshe.
0:00:49.95 LI011 Greater Shepparton/2019-12-24 C211gshe No files to upload for Greater Shepparton/2019-12-24-C211gshe/2019-12-24-C211gshe.
0:00:58.90 LI011 Greater Shepparton/2019-12-24 C211gshe/maps No files to upload for Greater Shepparton/2019-12-24-C211gshe/2019-12-24-C211gshe/Maps.
0:00:58.96 LW003  Maroondah/2013-02-21 C82 No files found (files=None).
0:01:18.73 LI011 Brimbank/2019-03-21 C205brim No files to upload for Brimbank/2019-03-21-C205brim/2019-03-21-C205brim.
0:01:39.64 LI011 Greater Geelong/2017-06-01 C341(Revised)/maps No files to upload for Greater Geelong/2017-06-01-C341(Revised)/2017-06-01-C341(Revised)/Maps.
0:02:03.09 LI011 Greater Geelong/2019-12-24 C407ggee/maps No files to upload for Greater Geelong/2019-12-24-C407ggee/2019-12-24-C407ggee/Maps.

0:02:03.12 XI006 Counters:
0:02:03.14 XI007 Upload-psam-processed 8
0:02:03.15 XI007 No-files-to-upload 7
```

### publish

Command:

```py H:\cumberland.py publish all```

```
0:00:00.01 XI001 Script (v12) started at 2020-03-10 11:15 (using config file H:cumberland.config)
0:00:12.37 PI002 Uploaded amendments: 42,055
0:00:12.96 PI008 Not published amendments: 1,410
0:04:11.10 PW003 Source data error (empty directory) for Maroondah/2013-02-21-C82/2013-02-21-C82
0:04:17.95 PW003 Source data error (empty directory) for Indigo/2016-03-10-C65
0:04:51.37 PW003 Source data error (empty directory) for Whitehorse/2018-02-08-C275
0:05:45.87 PI004 Amendments to publish: 1,399
0:05:45.89 PI005
Note: y=yes, n=no, a=publish all remaining, q=exit script.
PI006 Publish Alpine Resorts/2002-05-23 C12/maps (y/n/a/q)? a
0:06:31.38 PI008 Alpine Resorts/2002-05-23 C12/maps published.
0:06:31.47 PI008 Alpine Resorts/2002-07-11 C6/maps published.
0:06:31.55 PI008 Alpine Resorts/2002-10-08 VC16/maps published.
0:06:31.63 PI008 Alpine Resorts/2002-10-31 VC15/maps published.
...
0:08:38.47 PI008 Yarriambiack/2020-01-31 VC170/maps published.
0:08:38.53 PI008 Yarriambiack/2020-02-11 VC168 published.
0:08:38.61 PI008 Yarriambiack/2020-02-11 VC168/maps published.

0:08:38.66 XI006 Counters:
0:08:38.67 XI007 Publish_source_data_error 3
0:08:38.69 XI007 Published 1,399
```

Answer ```y``` to each question:

```
0:00:00.01 XI001 Script (v12) started at 2020-03-10 11:29 (using config file H:cumberland.config)
0:00:12.12 PI002 Uploaded amendments: 42,062
0:00:12.20 PI008 Not published amendments: 10
0:00:13.12 PW003 Source data error (empty directory) for Maroondah/2013-02-21-C82/2013-02-21-C82
0:00:13.27 PW003 Source data error (empty directory) for Indigo/2016-03-10-C65
0:00:13.63 PW003 Source data error (empty directory) for Whitehorse/2018-02-08-C275
0:00:14.68 PI004 Amendments to publish: 7
0:00:14.70 PI005
Note: y=yes, n=no, a=publish all remaining, q=exit script.
PI006 Publish Brimbank/2019-03-21 C205brim (y/n/a/q)? y
PI006 Publish Greater Geelong/2017-06-01 C341(Revised)/maps (y/n/a/q)? y
PI006 Publish Greater Geelong/2019-12-24 C407ggee/maps (y/n/a/q)? y
PI006 Publish Greater Shepparton/2019-10-17 C220gshe (y/n/a/q)? y
PI006 Publish Greater Shepparton/2019-10-17 C220gshe/maps (y/n/a/q)? y
PI006 Publish Greater Shepparton/2019-12-24 C211gshe (y/n/a/q)? y
PI006 Publish Greater Shepparton/2019-12-24 C211gshe/maps (y/n/a/q)? y

0:00:24.85 XI006 Counters:
0:00:24.87 XI007 Publish_source_data_error 3
0:00:24.88 XI007 Published 7
```

Answer ```n``` to the first three question:

```
0:00:00.01 XI001 Script (v12) started at 2020-03-10 13:11 (using config file H:cumberland.config)
0:00:12.98 PI002 Uploaded amendments: 42,098
0:00:13.09 PI008 Not published amendments: 91
0:00:20.91 PW003 Source data error (empty directory) for Maroondah/2013-02-21-C82/2013-02-21-C82
0:00:21.30 PW003 Source data error (empty directory) for Indigo/2016-03-10-C65
0:00:23.30 PW003 Source data error (empty directory) for Whitehorse/2018-02-08-C275
0:00:24.66 PI004 Amendments to publish: 36
0:00:24.68 PI005
Note: y=yes, n=no, a=publish all remaining, q=exit script.
PI006 Publish Mitchell/2019-12-05 C148mith (y/n/a/q)? n
PI006 Publish Mitchell/2019-12-05 C148mith/maps (y/n/a/q)? n
PI006 Publish Moira/2018-10-26 VC155(revised) (y/n/a/q)? n
PI006 Publish Moira/2018-10-26 VC155(revised)/maps (y/n/a/q)? q
0:01:05.87 PI007 User requested exit.

0:01:05.90 XI006 Counters:
0:01:05.91 XI007 Publish_source_data_error 3
0:01:05.93 XI007 Not-published 3
```

And again to veryify that they were not published (with same three names):

```
0:00:00.01 XI001 Script (v12) started at 2020-03-10 13:13 (using config file H:cumberland.config)
0:00:12.02 PI002 Uploaded amendments: 42,102
0:00:12.13 PI008 Not published amendments: 92
0:00:20.69 PW003 Source data error (empty directory) for Maroondah/2013-02-21-C82/2013-02-21-C82
0:00:21.02 PW003 Source data error (empty directory) for Indigo/2016-03-10-C65
0:00:22.90 PW003 Source data error (empty directory) for Whitehorse/2018-02-08-C275
0:00:24.26 PI004 Amendments to publish: 40
0:00:24.27 PI005
Note: y=yes, n=no, a=publish all remaining, q=exit script.
PI006 Publish Mitchell/2019-12-05 C148mith (y/n/a/q)? n
PI006 Publish Mitchell/2019-12-05 C148mith/maps (y/n/a/q)? n
PI006 Publish Moira/2018-10-26 VC155(revised) (y/n/a/q)? n
PI006 Publish Moira/2018-10-26 VC155(revised)/maps (y/n/a/q)? q
0:00:37.86 PI007 User requested exit.

0:00:37.89 XI006 Counters:
0:00:37.91 XI007 Publish_source_data_error 3
0:00:37.92 XI007 Not-published 3
```



### delete

Commands and responses (the first two commands create and verify the test data):

```py H:\cumberland.py test put```

```
0:00:00.01 XI001 Script (v10) started at 2020-03-02 21:34 (using config file H:cumberland.config)
Tests to run: put
```

```py H:\cumberland.py test get```

```
0:00:00.01 XI001 Script (v10) started at 2020-03-02 21:34 (using config file H:cumberland.config)
Tests to run: get

get amendment PSAM(Zealand, 2020-02-23 C500, path=Zealand/2020-01-22-C367zeal/2020-01-22-C367zeal, files=2, uploaded=False, published=False, pub_date=Unpublished
```

```py H:\cumberland.py delete Zealand/2020-02-23+C500```

```
0:00:00.01 XI001 Script (v10) started at 2020-03-02 21:35 (using config file H:cumberland.config)
0:00:00.20 DI002 Zealand/2020-02-23+C500 deleted.
```

### list

Command:

```py H:\cumberland.py list Alpine```

Result:

```
0:00:00.01 XI001 Script (v12) started at 2020-03-10 11:38 (using config file H:cumberland.config)
0:00:00.68 II002 Alpine 1999-09-09 NFPS Uploaded Published(2020-02-17)
0:00:00.70 II002 Alpine 1999-09-09 NFPS/maps Uploaded Published(2020-03-10)
...
0:00:02.20 II002 Alpine 2008-02-04 VC46 Uploaded Published(2020-02-17)
0:00:02.21 II002 Alpine 2008-02-04 VC46/maps Uploaded Published(2020-02-17)
0:00:03.48 II005   Test read url for Alpine/2008-04-07 VC47 (40409)
0:00:03.49 II002 Alpine 2008-04-07 VC47 Uploaded Published(2020-02-17)
0:00:03.51 II002 Alpine 2008-04-07 VC47/maps Uploaded Published(2020-02-17)
...
0:00:08.82 II002 Alpine 2020-02-11 VC168 Uploaded Published(2020-02-17)
0:00:08.84 II002 Alpine 2020-02-11 VC168/maps Uploaded Published(2020-02-17)
0:00:08.85 II001 Alpine (amendments=182 maps=181 uploaded=363 published=363)

0:00:08.87 XI006 Counters:
0:00:08.88 XI007 List amendments 182
0:00:08.90 XI007 List uploaded 363
0:00:08.91 XI007 List published 363
0:00:08.93 XI007 List maps 181
```

Command:

```py H:\cumberland.py list all```

Result:

```
0:00:00.01 XI001 Script (v12) started at 2020-03-10 11:41 (using config file H:cumberland.config)
0:00:01.34 II005   Test read url for Alpine/2008-04-07 VC47 (40409)
0:00:01.87 II005   Test read url for Alpine/2013-04-19 VC95 (44245)
0:00:02.46 II005   Test read url for Alpine/2017-11-21 VC141 (52883)
0:00:02.48 II001 Alpine (amendments=182 maps=181 uploaded=363 published=363)
0:00:03.23 II005   Test read url for Alpine Resorts/2001-09-27 VC13 (26779)
0:00:03.70 II005   Test read url for Alpine Resorts/2009-10-01 VC58 (39195)
0:00:04.13 II005   Test read url for Alpine Resorts/2014-08-22 VC118 (38174)
0:00:12.07 II005   Test read url for Alpine Resorts/2019-09-12 C27 (85542)
...
0:04:41.38 II001 Yarra Ranges (amendments=314 maps=314 uploaded=628 published=628)
0:04:41.94 II005   Test read url for Delatite/2002-10-31 VC15 (40000)
0:04:41.96 II001 Delatite (amendments=14 maps=14 uploaded=28 published=28)
0:04:42.67 II005   Test read url for Victoria Planning Provisions/2013-09-05 VC103 (39888)
0:04:42.69 II001 Victoria Planning Provisions (amendments=163 maps=0 uploaded=163 published=163)

0:04:42.70 XI006 Counters:
0:04:42.72 XI007 List amendments 21,206
0:04:42.74 XI007 List uploaded 42,067
0:04:42.75 XI007 List published 42,059
0:04:42.77 XI007 List maps 20,862

```


### stats

Command:

```py H:\cumberland.py stats all```

Result:

```
0:00:00.01 XI001 Script (v11) started at 2020-03-10 09:29 (using config file H:cumberland.config)
0:00:01.68 TI001 Amendments uploaded: 42,024
0:00:01.78 TI002 Amendments not uploaded: 1
0:00:01.79 TI003 Total amendments (uploaded): 42,025
0:00:02.28 TI004 Amendments published: 40,653
0:00:02.37 TI005 Amendments not published: 1,372
0:00:02.39 TI006 Total amendments (published): 42,025
```

```
0:00:00.01 XI001 Script (v12) started at 2020-03-10 11:28 (using config file H:cumberland.config)
0:00:00.49 TI001 Amendments uploaded: 42,062
0:00:00.57 TI002 Amendments not uploaded: 0
0:00:00.59 TI003 Total amendments (uploaded): 42,062
0:00:01.12 TI004 Amendments published: 42,052
0:00:01.20 TI005 Amendments not published: 10
0:00:01.21 TI006 Total amendments (published): 42,062
```

## Published HTML Lambda functions

### Planning Scheme index

URL:

```https://jo9grgdjai.execute-api.ap-southeast-2.amazonaws.com/prod/cumberland-html```

Result:

```
Planning Scheme Index

Planning Scheme
Alpine
Alpine Resorts
Ararat
Ballarat
...
Yarra
Yarriambiack
Yarra Ranges
Delatite (historical)
Victoria Planning Provisions
```


### Planning Scheme Amendment List

URL:

```https://jo9grgdjai.execute-api.ap-southeast-2.amazonaws.com/prod/cumberland-html/Alpine```

Result:

```
Alpine

Amendment Date  Amendment   Maps
Amendment Date	Amendment	Maps
09 Sep 1999	NFPS	Maps
25 May 2000	VC9	Maps
17 Aug 2000	VC8	Maps
14 Dec 2000	VC10	Maps
21 Dec 2000	C1	Maps
01 Feb 2001	C2	Maps
29 Mar 2001	VC11	Maps
17 May 2001	C3	Maps
24 Aug 2001	VC12	Maps
27 Sep 2001	VC13	Maps
22 Nov 2001	VC14	Maps
10 Jan 2002	C6	Maps
28 Mar 2002	C5	
08 Oct 2002	VC16	Maps
...
26 Sep 2019	VC164	Maps
21 Nov 2019	GC138	Maps
26 Nov 2019	VC158	Maps
03 Dec 2019	VC165	Maps
24 Jan 2020	VC160	Maps
31 Jan 2020	VC170	Maps
11 Feb 2020	VC168	Maps
```


### A Planning Scheme Amendment

URL:

```https://jo9grgdjai.execute-api.ap-southeast-2.amazonaws.com/prod/cumberland-html/Alpine/2020-02-11+VC168```

Result:

```
Alpine VC168 (2020-02-11)
Clause	Clause Type	Size
00	Purpose and Vision	0.81 MB
01	Purposes of this Planning Scheme	0.81 MB
10	Planning Policy Framework	0.74 MB
11	Settlement	0.81 MB
11.01-1S	Settlement	0.84 MB
11.01-1R-7H	Victoria - Hume	1.62 MB
11.01	Victoria	0.75 MB
11.02-1S	Supply of urban land	0.81 MB
11.02-2S	Structure planning	0.81 MB
11.02-3S	Sequencing of development	0.86 MB
11.02	Managing Growth	0.75 MB
11.03-1S	Activity centres	0.86 MB
11.03-2S	Growth areas	0.86 MB
11.03-3S	Peri-urban areas	0.81 MB
11.03-4S	Coastal settlement	0.86 MB
11.03-5S	Distinctive areas and landscapes	0.86 MB
11.03-6S	Regional and local places	0.81 MB
...
74	Strategic Implementation	0.74 MB
74.01	Application of Zones, Overlays and Provisions	0.81 MB
74.02	Further Strategic Work	0.81 MB
AmList	Amendment List	0.01 MB
List of Ams	List of Amendments	0.22 MB
Cover Page	Planning Scheme Cover Page	0.10 MB

Published to the web: 2020-02-17
```

Result (view source):

```
<html><head><title>Cumberland Alpine VC168 (2020-02-11) </title></head>
<body>
<h1>Alpine VC168 (2020-02-11) </h1>


<!-- BEGIN CONTENT -->
<!-- generated (2020-03-10 00:50 UTC) by Lambda function cumberland-html -->
<table border="0">
  <tbody><tr><th align="left">Clause</th><th align="left">Clause Type</th><th align="right">Size</th></tr>
  <tr><td align="left"><a href="https://cumberland-files.s3.amazonaws.com/91b613cfee423afc815b5872f3a8dd65.pdf">00</a></td><td align="left">Purpose and Vision</td><td align="right">0.81 MB</td></tr>
  <tr><td align="left"><a href="https://cumberland-files.s3.amazonaws.com/3e4e934163e4ffa60df842808e8fb48e.pdf">01</a></td><td align="left">Purposes of this Planning Scheme</td><td align="right">0.81 MB</td></tr>
  <tr><td align="left"><a href="https://cumberland-files.s3.amazonaws.com/13db23d5b44a197d01a9fedf8e888ff3.pdf">10</a></td><td align="left">Planning Policy Framework</td><td align="right">0.74 MB</td></tr>

...
  <tr><td align="left"><a href="https://cumberland-files.s3.amazonaws.com/7c18da847fa8afb6389f2acdaf7fd4cc.pdf">List of Ams</a></td><td align="left">List of Amendments</td><td align="right">0.22 MB</td></tr>
  <tr><td align="left"><a href="https://cumberland-files.s3.amazonaws.com/e319dd50f724fd90b1f3493b439ca59f.pdf">Cover Page</a></td><td align="left">Planning Scheme Cover Page</td><td align="right">0.10 MB</td></tr>
</tbody></table><p>Published to the web: 2020-02-17</p>
<!-- END CONTENT -->



    </body></html>
```


# Database cleanup

### test fix-ups

```
0:00:00.01 XI001 Script (v12) started at 2020-03-10 17:32 (using config file H:cumberland.config)
0:00:00.03 Test Tests to run: fix-dups
0:00:51.12 Test6 Deleted Alpine Resorts/2018-11-29 GC113(revised), kept Alpine Resorts/2018-11-29 GC113(Revised)
0:00:51.30 Test6 Deleted Alpine Resorts/2018-11-29 GC113(revised)/maps, kept Alpine Resorts/2018-11-29 GC113(Revised)/maps
0:00:52.65 Test4 Deleted Alpine Resorts/2019-07-04 C28, kept Alpine Resorts/2019-07-04 C028alpr
0:00:52.83 Test4 Deleted Alpine Resorts/2019-07-04 C28/maps, kept Alpine Resorts/2019-07-04 C028alpr/maps
0:00:54.11 Test4 Deleted Alpine Resorts/2019-09-12 C27, kept Alpine Resorts/2019-09-12 C027alpr
0:00:54.25 Test4 Deleted Alpine Resorts/2019-09-12 C27/maps, kept Alpine Resorts/2019-09-12 C027alpr/maps
0:01:22.54 Test6 Deleted Alpine/2013-09-05 VC103(revised), kept Alpine/2013-09-05 VC103(Revised)
0:01:22.72 Test6 Deleted Alpine/2013-09-05 VC103(revised)/maps, kept Alpine/2013-09-05 VC103(Revised)/maps
0:02:37.10 Test6 Deleted Ararat/2018-10-26 VC155(revised), kept Ararat/2018-10-26 VC155(Revised)
0:02:37.34 Test6 Deleted Ararat/2018-10-26 VC155(revised)/maps, kept Ararat/2018-10-26 VC155(Revised)/maps
```

```
0:00:00.01 XI001 Script (v12) started at 2020-03-10 17:36 (using config file H:cumberland.config)
0:00:00.03 Test Tests to run: fix-dups
0:04:29.97 Test6 Deleted Ballarat/2018-10-26 VC155(revised), kept Ballarat/2018-10-26 VC155(Revised)
0:04:30.26 Test6 Deleted Ballarat/2018-10-26 VC155(revised)/maps, kept Ballarat/2018-10-26 VC155(Revised)/maps
0:06:01.88 Test6 Deleted Banyule/2018-10-26 VC155(revised), kept Banyule/2018-10-26 VC155(Revised)
0:06:02.10 Test6 Deleted Banyule/2018-10-26 VC155(revised)/maps, kept Banyule/2018-10-26 VC155(Revised)/maps
0:06:03.13 Test5 Deleted Banyule/2019-01-31 C151, kept Banyule/2019-01-31 C151bany
0:06:03.65 Test5 Deleted Banyule/2019-01-31 C151/maps, kept Banyule/2019-01-31 C151bany/maps
0:06:05.24 Test5 Deleted Banyule/2019-05-27 C154, kept Banyule/2019-05-27 C154bany
0:06:05.48 Test5 Deleted Banyule/2019-05-27 C154/maps, kept Banyule/2019-05-27 C154bany/maps
0:06:09.09 Test5 Deleted Banyule/2019-08-29 C114, kept Banyule/2019-08-29 C114bany
0:06:09.32 Test5 Deleted Banyule/2019-08-29 C114/maps, kept Banyule/2019-08-29 C114bany/maps
0:06:11.15 Test5 Deleted Banyule/2019-10-04 C115, kept Banyule/2019-10-04 C115bany
0:06:11.45 Test5 Deleted Banyule/2019-10-04 C115/maps, kept Banyule/2019-10-04 C115bany/maps
0:08:05.95 Test6 Deleted Bass Coast/2018-11-01 GC111(revised), kept Bass Coast/2018-11-01 GC111(Revised)
0:08:06.31 Test6 Deleted Bass Coast/2018-11-01 GC111(revised)/maps, kept Bass Coast/2018-11-01 GC111(Revised)/maps
0:08:08.79 Test5 Deleted Bass Coast/2019-05-16 C155, kept Bass Coast/2019-05-16 C155basc
0:08:09.18 Test5 Deleted Bass Coast/2019-05-16 C155/maps, kept Bass Coast/2019-05-16 C155basc/maps
0:08:10.16 Test5 Deleted Bass Coast/2019-07-04 C151, kept Bass Coast/2019-07-04 C151basc
0:08:10.55 Test5 Deleted Bass Coast/2019-07-04 C151/maps, kept Bass Coast/2019-07-04 C151basc/maps
0:08:11.54 Test5 Deleted Bass Coast/2019-07-11 C156, kept Bass Coast/2019-07-11 C156basc
0:08:11.88 Test5 Deleted Bass Coast/2019-07-11 C156/maps, kept Bass Coast/2019-07-11 C156basc/maps
0:10:00.79 Test6 Deleted Baw Baw/2018-11-01 GC111(revised), kept Baw Baw/2018-11-01 GC111(Revised)
0:10:01.12 Test6 Deleted Baw Baw/2018-11-01 GC111(revised)/maps, kept Baw Baw/2018-11-01 GC111(Revised)/maps
0:10:03.32 Test5 Deleted Baw Baw/2019-05-03 C135, kept Baw Baw/2019-05-03 C135bawb
0:10:03.62 Test5 Deleted Baw Baw/2019-05-03 C135/maps, kept Baw Baw/2019-05-03 C135bawb/maps
0:10:06.99 Test5 Deleted Baw Baw/2019-11-07 C133, kept Baw Baw/2019-11-07 C133bawb
0:10:07.35 Test5 Deleted Baw Baw/2019-11-07 C133(Revised), kept Baw Baw/2019-11-07 C133bawb(revised)
0:10:07.70 Test5 Deleted Baw Baw/2019-11-07 C133(Revised)/maps, kept Baw Baw/2019-11-07 C133bawb(revised)/maps
0:10:07.96 Test5 Deleted Baw Baw/2019-11-07 C133/maps, kept Baw Baw/2019-11-07 C133bawb/maps
0:11:10.95 Test6 Deleted Bayside/2018-10-26 VC155(revised), kept Bayside/2018-10-26 VC155(Revised)
0:11:11.11 Test6 Deleted Bayside/2018-10-26 VC155(revised)/maps, kept Bayside/2018-10-26 VC155(Revised)/maps
0:11:14.67 Test5 Deleted Bayside/2019-08-15 C151, kept Bayside/2019-08-15 C151bays
0:11:14.82 Test5 Deleted Bayside/2019-08-15 C151/maps, kept Bayside/2019-08-15 C151bays/maps
0:11:16.14 Test5 Deleted Bayside/2019-09-12 C152, kept Bayside/2019-09-12 C152bays
0:11:16.29 Test5 Deleted Bayside/2019-09-12 C152/maps, kept Bayside/2019-09-12 C152bays/maps
0:11:17.82 Test5 Deleted Bayside/2019-11-21 C172, kept Bayside/2019-11-21 C172bays
0:11:17.99 Test5 Deleted Bayside/2019-11-21 C172/maps, kept Bayside/2019-11-21 C172bays/maps
0:11:18.73 Test5 Deleted Bayside/2019-11-21 C173, kept Bayside/2019-11-21 C173bays
0:11:18.88 Test5 Deleted Bayside/2019-11-21 C173/maps, kept Bayside/2019-11-21 C173bays/maps
0:11:20.81 Test2 Deleted not found Bayside/2020-02-06 C161
0:11:20.99 Test2 Deleted not found Bayside/2020-02-06 C161/maps
0:11:21.32 Test2 Deleted not found Bayside/2020-02-06 C161bayspt2
0:11:21.46 Test5 Deleted Bayside/2020-02-06 C161, kept Bayside/2020-02-06 C161bayspt2
0:11:21.56 Test2 Deleted not found Bayside/2020-02-06 C161bayspt2/maps
0:11:21.68 Test5 Deleted Bayside/2020-02-06 C161/maps, kept Bayside/2020-02-06 C161bayspt2/maps
0:11:22.81 Test2 Deleted not found Bayside/2020-03-06 C161bayspt1
0:11:22.98 Test2 Deleted not found Bayside/2020-03-06 C161bayspt1/maps
0:12:11.99 Test6 Deleted Benalla/2018-11-16 C36(revised), kept Benalla/2018-11-16 C36(Revised)
0:12:12.18 Test6 Deleted Benalla/2018-11-16 C36(revised)/maps, kept Benalla/2018-11-16 C36(Revised)/maps
0:12:14.49 Test5 Deleted Benalla/2019-05-09 C38, kept Benalla/2019-05-09 C38bena
0:12:14.66 Test5 Deleted Benalla/2019-05-09 C38/maps, kept Benalla/2019-05-09 C38bena/maps
0:14:01.92 Test5 Deleted Boroondara/2018-11-08 C301, kept Boroondara/2018-11-08 C301boro
0:14:02.11 Test5 Deleted Boroondara/2018-11-08 C301/maps, kept Boroondara/2018-11-08 C301boro/maps
0:14:03.34 Test5 Deleted Boroondara/2018-11-29 C302, kept Boroondara/2018-11-29 C302boro
0:14:03.65 Test5 Deleted Boroondara/2018-11-29 C302(Revised), kept Boroondara/2018-11-29 C302boro(revised)
0:14:03.87 Test5 Deleted Boroondara/2018-11-29 C302(Revised)/maps, kept Boroondara/2018-11-29 C302boro(revised)/maps
0:14:04.07 Test5 Deleted Boroondara/2018-11-29 C302/maps, kept Boroondara/2018-11-29 C302boro/maps
0:14:05.14 Test5 Deleted Boroondara/2019-02-21 C310, kept Boroondara/2019-02-21 C310boro
0:14:05.34 Test5 Deleted Boroondara/2019-02-21 C310/maps, kept Boroondara/2019-02-21 C310boro/maps
0:14:06.84 Test5 Deleted Boroondara/2019-04-15 C309, kept Boroondara/2019-04-15 C309boro
0:14:07.04 Test5 Deleted Boroondara/2019-04-15 C309/maps, kept Boroondara/2019-04-15 C309boro/maps
0:14:10.06 Test5 Deleted Boroondara/2019-07-26 C276, kept Boroondara/2019-07-26 C276boro
0:14:10.24 Test5 Deleted Boroondara/2019-07-26 C276/maps, kept Boroondara/2019-07-26 C276boro/maps
0:14:11.37 Test5 Deleted Boroondara/2019-08-15 C314, kept Boroondara/2019-08-15 C314boro
0:14:11.57 Test5 Deleted Boroondara/2019-08-15 C314/maps, kept Boroondara/2019-08-15 C314boro/maps
0:14:12.73 Test5 Deleted Boroondara/2019-08-30 C266, kept Boroondara/2019-08-30 C266boro
0:14:12.93 Test5 Deleted Boroondara/2019-08-30 C266/maps, kept Boroondara/2019-08-30 C266boro/maps
0:14:13.62 Test5 Deleted Boroondara/2019-09-12 C274(Part 2), kept Boroondara/2019-09-12 C274boro(Part 2)
0:14:13.84 Test5 Deleted Boroondara/2019-09-12 C274(Part 2)/maps, kept Boroondara/2019-09-12 C274boro(Part 2)/maps
0:14:15.32 Test5 Deleted Boroondara/2019-10-17 C303, kept Boroondara/2019-10-17 C303boro
0:14:15.51 Test5 Deleted Boroondara/2019-10-17 C303/maps, kept Boroondara/2019-10-17 C303boro/maps
0:14:16.34 Test5 Deleted Boroondara/2019-10-31 C307, kept Boroondara/2019-10-31 C307boro
0:14:16.52 Test5 Deleted Boroondara/2019-10-31 C307/maps, kept Boroondara/2019-10-31 C307boro/maps
0:14:17.23 Test5 Deleted Boroondara/2019-11-14 C319, kept Boroondara/2019-11-14 C319boro
0:14:17.45 Test5 Deleted Boroondara/2019-11-14 C319/maps, kept Boroondara/2019-11-14 C319boro/maps
0:14:18.49 Test5 Deleted Boroondara/2019-11-29 C326, kept Boroondara/2019-11-29 C326boro
0:14:18.70 Test5 Deleted Boroondara/2019-11-29 C326/maps, kept Boroondara/2019-11-29 C326boro/maps
0:14:20.12 Test5 Deleted Boroondara/2020-01-16 C322, kept Boroondara/2020-01-16 C322boro
0:14:20.34 Test5 Deleted Boroondara/2020-01-16 C322/maps, kept Boroondara/2020-01-16 C322boro/maps
0:14:21.05 Test5 Deleted Boroondara/2020-01-16 C324, kept Boroondara/2020-01-16 C324boro
0:14:21.27 Test5 Deleted Boroondara/2020-01-16 C324/maps, kept Boroondara/2020-01-16 C324boro/maps
0:14:22.79 Test5 Deleted Boroondara/2020-02-06 C312, kept Boroondara/2020-02-06 C312boro
0:14:23.02 Test5 Deleted Boroondara/2020-02-06 C312/maps, kept Boroondara/2020-02-06 C312boro/maps
0:15:41.18 Test6 Deleted Brimbank/2018-02-27 VC144(revised), kept Brimbank/2018-02-27 VC144(Revised)
0:15:41.36 Test6 Deleted Brimbank/2018-02-27 VC144(revised)/maps, kept Brimbank/2018-02-27 VC144(Revised)/maps
0:15:47.76 Test6 Deleted Brimbank/2018-11-29 C188(Part 1)(revised), kept Brimbank/2018-11-29 C188(Part 1)(Revised)
0:15:47.95 Test6 Deleted Brimbank/2018-11-29 C188(Part 1)(revised)/maps, kept Brimbank/2018-11-29 C188(Part 1)(Revised)/maps
0:15:49.49 Test5 Deleted Brimbank/2019-03-21 C205, kept Brimbank/2019-03-21 C205brim
0:15:49.70 Test5 Deleted Brimbank/2019-03-21 C205/maps, kept Brimbank/2019-03-21 C205brim/maps
0:15:52.62 Test5 Deleted Brimbank/2019-09-20 C202, kept Brimbank/2019-09-20 C202brim
0:15:52.79 Test5 Deleted Brimbank/2019-09-20 C202/maps, kept Brimbank/2019-09-20 C202brim/maps
0:15:54.73 Test5 Deleted Brimbank/2020-01-16 C215, kept Brimbank/2020-01-16 C215brim
0:15:54.90 Test5 Deleted Brimbank/2020-01-16 C215/maps, kept Brimbank/2020-01-16 C215brim/maps
0:16:44.35 Test6 Deleted Buloke/2018-11-15 C36(revised), kept Buloke/2018-11-15 C36(Revised)
0:16:44.56 Test6 Deleted Buloke/2018-11-15 C36(revised)/maps, kept Buloke/2018-11-15 C36(Revised)/maps
0:17:58.00 Test5 Deleted Campaspe/2019-07-04 C112, kept Campaspe/2019-07-04 C112camp
0:17:58.22 Test5 Deleted Campaspe/2019-07-04 C112/maps, kept Campaspe/2019-07-04 C112camp/maps
0:19:58.03 Test6 Deleted Cardinia/2018-10-29 GC103(revised), kept Cardinia/2018-10-29 GC103(Revised)
0:19:58.30 Test6 Deleted Cardinia/2018-10-29 GC103(revised)/maps, kept Cardinia/2018-10-29 GC103(Revised)/maps
0:19:59.23 Test5 Deleted Cardinia/2018-12-20 C252, kept Cardinia/2018-12-20 C252card
0:19:59.47 Test5 Deleted Cardinia/2018-12-20 C252/maps, kept Cardinia/2018-12-20 C252card/maps
0:20:00.37 Test5 Deleted Cardinia/2019-01-24 C231, kept Cardinia/2019-01-24 C231card
0:20:00.59 Test5 Deleted Cardinia/2019-01-24 C231/maps, kept Cardinia/2019-01-24 C231card/maps
0:20:01.36 Test5 Deleted Cardinia/2019-02-21 C253, kept Cardinia/2019-02-21 C253card
0:20:01.61 Test5 Deleted Cardinia/2019-02-21 C253/maps, kept Cardinia/2019-02-21 C253card/maps
0:20:03.75 Test5 Deleted Cardinia/2019-07-18 C220, kept Cardinia/2019-07-18 C220card
0:20:04.11 Test5 Deleted Cardinia/2019-07-18 C220/maps, kept Cardinia/2019-07-18 C220card/maps
0:20:05.76 Test5 Deleted Cardinia/2019-08-30 C242, kept Cardinia/2019-08-30 C242card
0:20:06.03 Test5 Deleted Cardinia/2019-08-30 C242/maps, kept Cardinia/2019-08-30 C242card/maps
0:20:07.30 Test5 Deleted Cardinia/2019-09-20 C237, kept Cardinia/2019-09-20 C237card
0:20:07.55 Test5 Deleted Cardinia/2019-09-20 C237/maps, kept Cardinia/2019-09-20 C237card/maps
0:20:08.78 Test5 Deleted Cardinia/2019-10-04 C205, kept Cardinia/2019-10-04 C205card
0:20:09.01 Test5 Deleted Cardinia/2019-10-04 C205/maps, kept Cardinia/2019-10-04 C205card/maps
0:20:09.78 Test5 Deleted Cardinia/2019-11-14 C244, kept Cardinia/2019-11-14 C244card
0:20:10.00 Test5 Deleted Cardinia/2019-11-14 C244/maps, kept Cardinia/2019-11-14 C244card/maps
0:20:11.20 Test5 Deleted Cardinia/2019-11-14 C259, kept Cardinia/2019-11-14 C259card
0:20:11.53 Test5 Deleted Cardinia/2019-11-14 C259(Revised), kept Cardinia/2019-11-14 C259card(revised)
0:20:11.81 Test5 Deleted Cardinia/2019-11-14 C259(Revised)/maps, kept Cardinia/2019-11-14 C259card(revised)/maps
0:20:12.04 Test5 Deleted Cardinia/2019-11-14 C259/maps, kept Cardinia/2019-11-14 C259card/maps
0:20:13.81 Test5 Deleted Cardinia/2019-12-12 C260, kept Cardinia/2019-12-12 C260card
0:20:14.07 Test5 Deleted Cardinia/2019-12-12 C260/maps, kept Cardinia/2019-12-12 C260card/maps
0:20:14.89 Test5 Deleted Cardinia/2019-12-24 C255, kept Cardinia/2019-12-24 C255card
0:20:15.15 Test5 Deleted Cardinia/2019-12-24 C255/maps, kept Cardinia/2019-12-24 C255card/maps
0:22:16.86 Test5 Deleted Casey/2019-04-18 C261, kept Casey/2019-04-18 C261case
0:22:17.09 Test5 Deleted Casey/2019-04-18 C261/maps, kept Casey/2019-04-18 C261case/maps
0:22:17.90 Test5 Deleted Casey/2019-05-09 C257, kept Casey/2019-05-09 C257case
0:22:18.14 Test5 Deleted Casey/2019-05-09 C257/maps, kept Casey/2019-05-09 C257case/maps
0:22:19.43 Test5 Deleted Casey/2019-06-13 C253, kept Casey/2019-06-13 C253case
0:22:19.65 Test5 Deleted Casey/2019-06-13 C253/maps, kept Casey/2019-06-13 C253case/maps
0:22:20.57 Test5 Deleted Casey/2019-07-18 C266, kept Casey/2019-07-18 C266case
0:22:20.82 Test5 Deleted Casey/2019-07-18 C266/maps, kept Casey/2019-07-18 C266case/maps
0:22:21.64 Test5 Deleted Casey/2019-07-25 C224, kept Casey/2019-07-25 C224case
0:22:21.89 Test5 Deleted Casey/2019-07-25 C224/maps, kept Casey/2019-07-25 C224case/maps
0:22:22.74 Test5 Deleted Casey/2019-07-26 C267, kept Casey/2019-07-26 C267case
0:22:22.98 Test5 Deleted Casey/2019-07-26 C267/maps, kept Casey/2019-07-26 C267case/maps
0:22:24.35 Test5 Deleted Casey/2019-08-15 C235, kept Casey/2019-08-15 C235case
0:22:24.60 Test5 Deleted Casey/2019-08-15 C235/maps, kept Casey/2019-08-15 C235case/maps
0:22:25.93 Test5 Deleted Casey/2019-09-05 C192, kept Casey/2019-09-05 C192case
0:22:26.18 Test5 Deleted Casey/2019-09-05 C192/maps, kept Casey/2019-09-05 C192case/maps
0:22:28.46 Test5 Deleted Casey/2019-11-21 C260, kept Casey/2019-11-21 C260case
0:22:28.68 Test5 Deleted Casey/2019-11-21 C260/maps, kept Casey/2019-11-21 C260case/maps
0:22:29.49 Test5 Deleted Casey/2019-11-21 C272, kept Casey/2019-11-21 C272case
0:22:29.71 Test5 Deleted Casey/2019-11-21 C272/maps, kept Casey/2019-11-21 C272case/maps
0:22:30.99 Test5 Deleted Casey/2019-11-29 C198, kept Casey/2019-11-29 C198case
0:22:31.24 Test5 Deleted Casey/2019-11-29 C198/maps, kept Casey/2019-11-29 C198case/maps
0:22:32.10 Test5 Deleted Casey/2019-11-29 C262, kept Casey/2019-11-29 C262case
0:22:32.35 Test5 Deleted Casey/2019-11-29 C262/maps, kept Casey/2019-11-29 C262case/maps
0:22:33.76 Test5 Deleted Casey/2019-12-12 C229, kept Casey/2019-12-12 C229case
0:22:33.98 Test5 Deleted Casey/2019-12-12 C229/maps, kept Casey/2019-12-12 C229case/maps
0:23:23.31 Test6 Deleted Central Goldfields/2018-10-26 VC155(revised), kept Central Goldfields/2018-10-26 VC155(Revised)
0:23:23.53 Test6 Deleted Central Goldfields/2018-10-26 VC155(revised)/maps, kept Central Goldfields/2018-10-26 VC155(Revised)/maps
0:24:09.32 Test6 Deleted Colac Otway/2013-09-05 VC103(revised), kept Colac Otway/2013-09-05 VC103(Revised)
0:24:09.60 Test6 Deleted Colac Otway/2013-09-05 VC103(revised)/maps, kept Colac Otway/2013-09-05 VC103(Revised)/maps
0:24:39.04 Test6 Deleted Colac Otway/2018-11-29 GC113(revised), kept Colac Otway/2018-11-29 GC113(Revised)
0:24:39.29 Test6 Deleted Colac Otway/2018-11-29 GC113(revised)/maps, kept Colac Otway/2018-11-29 GC113(Revised)/maps
0:24:40.23 Test4 Deleted Colac Otway/2019-02-21 C85, kept Colac Otway/2019-02-21 C085cola
0:24:40.50 Test4 Deleted Colac Otway/2019-02-21 C85/maps, kept Colac Otway/2019-02-21 C085cola/maps
0:25:43.51 Test6 Deleted Corangamite/2018-11-29 GC113(revised), kept Corangamite/2018-11-29 GC113(Revised)
0:25:43.73 Test6 Deleted Corangamite/2018-11-29 GC113(revised)/maps, kept Corangamite/2018-11-29 GC113(Revised)/maps
0:25:44.79 Test4 Deleted Corangamite/2019-02-21 C44, kept Corangamite/2019-02-21 C044cora
0:25:45.01 Test4 Deleted Corangamite/2019-02-21 C44/maps, kept Corangamite/2019-02-21 C044cora/maps
0:27:05.91 Test6 Deleted Darebin/2018-11-08 C165(revised), kept Darebin/2018-11-08 C165(Revised)
0:27:06.11 Test6 Deleted Darebin/2018-11-08 C165(revised)/maps, kept Darebin/2018-11-08 C165(Revised)/maps
0:27:08.11 Test5 Deleted Darebin/2019-06-27 C183, kept Darebin/2019-06-27 C183dare
0:27:08.32 Test5 Deleted Darebin/2019-06-27 C183/maps, kept Darebin/2019-06-27 C183dare/maps
0:28:54.55 Test5 Deleted East Gippsland/2018-12-13 C139, kept East Gippsland/2018-12-13 C139egip
0:28:54.84 Test5 Deleted East Gippsland/2018-12-13 C139/maps, kept East Gippsland/2018-12-13 C139egip/maps
0:28:58.63 Test5 Deleted East Gippsland/2019-10-10 C153, kept East Gippsland/2019-10-10 C153egip
0:28:58.93 Test5 Deleted East Gippsland/2019-10-10 C153/maps, kept East Gippsland/2019-10-10 C153egip/maps
0:30:09.87 Test6 Deleted Frankston/2018-11-22 C111(revised), kept Frankston/2018-11-22 C111(Revised)
0:30:10.05 Test6 Deleted Frankston/2018-11-22 C111(revised)/maps, kept Frankston/2018-11-22 C111(Revised)/maps
0:30:11.71 Test5 Deleted Frankston/2019-05-23 C132, kept Frankston/2019-05-23 C132fran
0:30:11.90 Test5 Deleted Frankston/2019-05-23 C132/maps, kept Frankston/2019-05-23 C132fran/maps
0:30:12.58 Test5 Deleted Frankston/2019-05-23 C133, kept Frankston/2019-05-23 C133fran
0:30:12.74 Test5 Deleted Frankston/2019-05-23 C133/maps, kept Frankston/2019-05-23 C133fran/maps
...
```