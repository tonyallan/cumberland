#! python3

"""
This script scans planning scheme history directories for amendments to be published
and then uploads index lists (to an AWS DynamoDB database, one item for clauses and a 
second for maps) and PDF files (to an AWS S3 bucket, named by the file MD5 hash to 
deduplicate the files).

Tony Allan (tony@apms.com.au)
"""

import argparse
import collections
import configparser
import datetime
import hashlib
import json
import os
from pathlib import Path
import pprint
import sys
import time
from urllib.parse import urlencode

import requests

SCRIPT_VERSION = 13

class Message:
    """
    Write the elapsed time, severity level and a message to the console.
    fatal --> exit to be less scary for users.
    """

    def __init__(self, version=None, cmdline=None, config_file=None):
        self.start = datetime.datetime.now()

        self.version = version
        self.cmdline = cmdline

        self.unsent_messages = []
        self.max_unsent = 10

        start_text = str(self.start).split('.')[0][:-3]

        self.message('XI001', f'Script (v{ self.version}) started at {start_text} (using config file {config_file})')


    def message(self, code, msg, level='info', new_line=False):
        assert level in ['info', 'warn', 'error', 'exit'], 'Unknown message level'

        # Workaround for truncated timestamp in the first message
        time.sleep(0.001)

        elapsed_time = str(datetime.datetime.now() - self.start)[:-4]

        if new_line:
            print(f'\n{elapsed_time} {code} {msg}')

        else:
            print(f'{elapsed_time} {code} {msg}')

        self.unsent_messages.append(f'{code} {msg}')

        if len(self.unsent_messages) > self.max_unsent:
            self.send_messages()


    def error(self, code, msg):
        self.message(code, msg, level='error')


    def exit(self, code, msg):
        self.message(code, msg, level='exit')
        sys.exit(1)


    def send_messages(self):
        pass


    def finalise(self):
        self.send_messages()


class PSAM:
    """
    Encapsulate the data in the database for a directory (clause or map).
    """

    def __init__(self, ps=None, am=None, maps=None, path=None):

        if maps:
            am += '/maps'

        self.ps         = ps
        self.am         = am
        self.path       = path
        self.files      = []
        self.uploaded   = False
        self.published  = False
        self.pub_date   = 'Unpublished'


    def load(self, item):

        self.ps         = item.get('ps', None)
        self.am         = item.get('am', None)
        self.path       = item.get('path', None)
        self.files      = item.get('files', [])
        self.uploaded   = item.get('uploaded', False)
        self.published  = item.get('published', False)
        self.pub_date   = item.get('pub_date', 'Unpublished')

        self.validate()

        return self


    def dump(self):

        self.validate()

        return dict(
            ps=self.ps,
            am=self.am,
            path=self.path,
            files=self.files,
            uploaded=self.uploaded,
            published=self.published,
            pub_date=self.pub_date,
        )


    def validate(self):

        assert self.ps is not None, '[PSAM] Planning Scheme cannot be None'
        assert self.am is not None, '[PSAM] Amendment cannot be None'
        assert len(self.ps) > 0,    '[PSAM] (load) Planning Scheme cannot be an empty string'
        assert len(self.am) > 0,    '[PSAM] (load) Amendment cannot be an empty string'
        assert self.am[10] == ' ',  f'[PSAM] (load) Amendment character 10 must be blank: {self.ps}/{self.am}'


    def __str__(self):
        return self.ps + '/' + self.am


    def __repr__(self):
        pub_date = self.pub_date.split(' ')[0]

        if self.files == None:
            len_files = 'Files=None'

        else:
            len_files = len(self.files)

        return f'PSAM({self.ps}, {self.am}, path={self.path}, files={len_files}, ' + \
               f'uploaded={self.uploaded}, published={self.published}, pub_date={pub_date})'


    def __eq__(self, other):
        #print('eq', str(self), str(other), str(self) == str(other))
        return str(self) == str(other)


    def __lt__(self, other):
        #print('lt', str(self), str(other), str(self) < str(other))
        return str(self) < str(other)


    def __cmp__(self, other):
        print('cmp', str(self), str(other), cmp(self, other))
        return cmp(self, other)


class CumberlandApi:
    """
    Database items correspond to a directory countaining clause files or maps files.
    Each amendment is represented by one or two database items.

    am has a /maps suffix to represent map files.

    Call an appropriate Lambda function:

    - test(name)
    - planning_schemes(select, vpp=None)
    - list(ps)
    - flagged(uploaded=None, published=None, count=None)
    - get(ps, am)
    - put(psam):
    - update(ps, am, files=None, uploaded=None, published=None, path=None)
    - delete(ps, am):
    - presigned_urls(hashes)

    Internal function to perform the HTTP request:

    _cumberland_api(api_function, arguments=[], method='get', data=None, query={}, error_exit=True)
    """

    def __init__ (self, base_api, api_key, session):

        self.base_api = base_api
        self.api_key = api_key
        self.session = session


    def test(self, name):
        """
        Fetch test information about the lambda execution. Not normally used.
        name=info is the only currently supported argument.
        """

        result = api._cumberland_api('test', arguments=name)

        return result


    def planning_schemes(self, select=None, vpp=None):
        """
        Select is one of:
        full -- return all planning scheme data
        code -- return a dict of code -> planning scheme name
        name -- return a dict of planning scheme name -> code
        code-list -- return a list of planning scheme codes
        name-list -- return a list of planning scheme names

        Use vpp to add 'vpp and/or 'Victoria Planning Provisions'

        Delatite (code=zzz0) is included.
        """

        query = {}

        if select is not None:
            query['select'] = select

        if vpp is not None:
            query['vpp'] = vpp

        result = api._cumberland_api('planning-schemes', query=query)

        return result['result']


    def list(self, ps=None):
        """
        Expects a planning scheme name to be specified.
        Return a list of dict(keys=am, published, pub_date, uploaded) 
        """

        result = api._cumberland_api('list', arguments=ps)

        if result['code'] == 'success':

            psam_list = []

            for item in result['result']:
                psam_list.append(PSAM().load(item))

            result['list'] = psam_list

        return result


    def flagged(self, uploaded=None, published=None, count=None):
        """
        Expects uploaded=True|False or published=True|False.
        Return a list of PSAM object with (am, ps). For published, also returns pub_date.

        If count=True specified, return a count of items that match the specified attribute) as an int.
        """

        query = {}

        if uploaded is not None:
            query['uploaded'] = uploaded

        if published is not None:
            query['published'] = published

        if count is not None:
            query['count'] = count

        result = self._cumberland_api('flagged', query=query)

        if count:
            return result['result']['count']

        else:
            psam_list = []

            for item in result['result']:
                psam_list.append(PSAM().load(item))

            return psam_list


    def get(self, ps, am):
        """
        Return a PSAM object for the specified ps and am.

        Return code='item-not-found' if the ps and am combination does not exist.
        """

        result = self._cumberland_api('amendment', arguments=[ps, am], error_exit=False)

        if result['code'] == 'success':
            result['psam'] = PSAM().load(result['result'])

        return result


    def put(self, psam):
        """
        Add a new item to the database represented by a PSAM object.

        Returns code='success' if the addition was OK, or
        code='client-error' if the item already exists.
        """

        result = self._cumberland_api('amendment', method='put', arguments=[psam.ps, psam.am], 
            data=psam.dump(), error_exit=False)

        return result


    def update(self, ps, am, files=None, uploaded=None, published=None, path=None):
        """
        Update files and/or uploaded flag and/or published flag and/or the path.

        Returns code='success' if the update was OK.
        """

        query = {}

        if uploaded is not None:
            query['uploaded'] = uploaded

        if published is not None:
            query['published'] = published

        if path is not None:
            query['path'] = path

        result = self._cumberland_api('amendment', method='post', arguments=[ps, am], 
            data=files, query=query, error_exit=False)

        return result


    def delete(self, ps, am):
        """
        Remove the item specicided by ps and am.
        Returns code='success' if the delete was OK.
        """

        result = self._cumberland_api('amendment', method='delete', arguments=[ps, am], error_exit=False)

        return result


    def presigned_urls(self, hashes):
        """
        Return a dict(key=hash) for each of the specified hashes containing an upload URL
        that will expire in 3600 seconds.
        """

        result = self._cumberland_api('presigned-urls', method='post', data=list(hashes), error_exit=False)

        if result['code'] == 'success':
            return result['result']

        else:
            return result


    def _cumberland_api(self, api_function, arguments=[], method='get', data=None, query={}, error_exit=True):

        if isinstance(arguments, str):
            arguments = [arguments]

        if method.lower() == 'get':
            api_type = 'cumberland'

        else:
            api_type = 'cumberland-protected'

        url = '/'.join([self.base_api, api_type, api_function] + arguments)
        url_text = '[' + api_function + ' ' + method.upper() + ']' + '/'.join(arguments)

        if len(query) > 0:
            url += '?' + urlencode(query)
            url_text += '?' + urlencode(query)

        if method.lower() == 'get':
            response = self.session.send(requests.Request('get', url).prepare())

        else:
            response = self.session.send(requests.Request(method, url, data=json.dumps(data, indent=2), 
                headers={'x-api-key': self.api_key}).prepare())

        if response.status_code != 200:
            if response.status_code == 504:
                log.exit('XF090', f'HTTP Gateway Timeout (most likely PC went to sleep). Retry the command.')

            log.exit('XF100', f'HTTP Error ({response.status_code}) for {url_text}\n{response.text}')

        data = response.json()

        if error_exit:
            if data['code'] != 'success':
                    log.exit('XF101', f"API Error ({data['code']}) {data['message']} (for {url_text})")

        return data
 

def get_config(section):

    found = False

    for base in [Path('H:/'), Path.home(), Path.cwd()]:

        try:
            config_file = base / Path('cumberland.config')

            if config_file.exists():
                    found = True
                    break

        except:
            pass

    if found:
        full_config = configparser.ConfigParser()
        full_config.read(config_file)

    else:
        sys.exit(f'XI002 Cannot find config file: {config_file}')

    return (config_file, full_config[section])


def find_map_path(base_path, rel_path, current=None):

    for p in ['PDF Maps', 'PDF MAps', 'Maps']:

        if (Path(base_path) / Path(rel_path) / Path(p)).exists():

            if current is not None:

                if p == current:
                    return None

            return rel_path + '/' + p

    return None


def scan_directories(base_path, argument):
    """
    Read the content of all directories or those since a specified date (since), 
    and their associated files and update the database.

    Most directories are of the form:
        ps             /am_dir1                /am_dir2
        planning_scheme/yyyy-mm-dd-xxnnn suffix
        planning_scheme/yyyy-mm-dd-xxnnn suffix/yyyy-mm-dd-xxnnn suffix   (since about 2011-11-19)

    Suffixes are: 
        (Part nn)     e.g. (Part 1)
        (Part nx)     e.g. (part 1A)
        (Revised nn)
        (Revised)
        (Revoked)
        (VOID)

    Each database item contains the following attributes:
        ps              Planning Scheme name e.g. Alpine
        am              Amendment including the date (e.g. 2004-07-29-C016(Part 1)#maps)
        files           a List with one element per file
                            hash        (e.g. fb1cb25e2eebd339c238388880f57f81)
                            name        (e.g. yarraranges01emo.pdf)
                            size        (e.g. 54234)
        path            source path without base_path (e.g. Yarra Ranges/2004-07-29-C16(Part 1)/Maps)
        pub_date        date when published set to T (e.g. 2020-02-17 20:56) or Unpublished
        published       visible on the departments website (T or F)
        uploaded        all files have a corresponding S3 file -- hash.pdf (T or F)
    """
    since = None

    if argument != 'all':
        since = argument
        log.message('SI001', f'Processing directories since: {since}')
    
    if not (base_path / 'Alpine').exists():
        log.exit('SF002', f'{base_path} configuration error.')

    for ps_dir in os.scandir(base_path):

        if not ps_dir.is_dir():
            counters['Ignored-not-ps'] += 1
            continue

        if not ps_dir.name[0].isalpha():
            counters['Ignored-not-ps'] += 1
            continue

        if ps_dir.name == 'Upper Yarra Valley & Dandenong Ranges Regional Strategy Plan':
            log.message('SI003', f'Ignore non-Planning Scheme directory: {ps_dir.name}')
            counters['Ignored-not-ps'] += 1
            continue

        ps = ps_dir.name

        # scan testing
        # if ps != 'Wellington':
        #     continue
        # if ps != 'Nillumbik':
        #     continue
        # if ps < 'V':
        #     continue

        log.message('SI004', f'Processing {ps}')
        counters['Scanned-ps+vpp'] += 1

        for am_dir1 in os.scandir(base_path / ps_dir):
            counters['Scanned-source-am'] += 1

            am = am_dir1.name
            rel_path = ps + '/' + am

            found = False

            for test_name in ['Approved Amendments', 'Exhibition Amendments', 'Amendments']:
                if test_name in am:
                    found = True
                    break

            if found:
                continue

            if not am_dir1.is_dir():
                #log.message('SI005', f'Ignore directories that are not a directory ({ps}): {am}')
                counters['Ignored-not-am'] += 1
                continue

            if not am[:4].isdigit():
                log.message('SI006', f'Ignore directories that don\'t start with a year ({ps}): {am}')
                counters['Ignored-not-am'] += 1
                continue

            if since is not None:
                if am[:10] < since:
                    counters['Ignored-am-before'] += 1
                    continue

            if 'copy' in am.lower():
                log.message('SI007', f'Ignore directories that contain \'copy\' ({ps}): {am}')
                counters['Ignored-not-am'] += 1
                continue   

            if 'not valid' in am.lower():
                log.message('SI008', f'Ignore directories that contain \'not valid\' ({ps}): {am}')
                counters['Ignored-not-am'] += 1
                continue

            if ps == 'Victoria Planning Provisions':

                if am == '1996-11-18-Pre-Launch version':
                    counters['Ignored-not-am'] += 1
                    continue

                found = False

                for am_dir2 in os.scandir(base_path / ps_dir / am_dir1):

                    if 'PDF' in am_dir2.name.upper():
                        rel_path += '/' + am_dir2.name

                        found = True
                        break

                if not found:
                    log.error('SE009', f'No Victoria Planning Provisions second level directory for ({ps}): {am}')
                    counters['Error'] += 1

            else:
                # Melbourne\1999-04-30-C1
                # Port of Melbourne\2019-12-03-VC165\2019-12-03-VC165

                found = False

                for am_dir2 in os.scandir(base_path / ps_dir / am_dir1):

                    if am_dir2.name[:4].isdigit():
                        rel_path += '/' + am_dir2.name

                        found = True
                        break

                if found:
                    if am_dir1.name[:10] != am_dir2.name[:10]:
                        log.error('SE010', f'First and second level dates don\'t match ({ps}): {am} vs {am_dir2.name}')
                        counters['Error'] += 1

                map_path = find_map_path(base_path, rel_path)

                if map_path is None:
                    #log.error('SE011', f'No PDF Maps or Maps directory for ({ps}): {am}')
                    counters['No-maps-for-am'] += 1

                else:
                    process_directory(base_path, PSAM(ps, am, maps=True, path=map_path))
                    counters['AM-to-process'] += 1

            process_directory(base_path, PSAM(ps, am, maps=False, path=rel_path))
            counters['AM-to-process'] += 1


def process_directory(base_path, psam):

    # Time to change the dash to a space
    psam.am = psam.am[:10] + ' ' + psam.am[11:]

    # Other fixups
    if 'revised' in psam.am:
        psam.am = psam.am.replace('revised', 'Revised')
        log.message('SI016', f"{psam} Unexpected text 'revised', changed to 'Revised' in the database.")

    if 'pt1' in psam.am:
        psam.am = psam.am.replace('pt1', '(Part 1)')
        log.message('SI016', f"{psam} Unexpected text 'pt1', changed to '(Part 1)' in the database.")

    if 'pt2' in psam.am:
        psam.am = psam.am.replace('pt2', '(Part 2)')
        log.message('SI016', f"{psam} Unexpected text 'pt2', changed to '(Part 2)' in the database.")

    if 'pt3' in psam.am:
        psam.am = am = psam.am.replace('pt3', '(Part 3)')
        log.message('SI016', f"{psam} Unexpected text 'pt3', changed to '(Part 3)' in the database.")

    result = api.get(psam.ps, psam.am)

    if ('code' in result) and (result['code'] == 'item-not-found'):

        psam.files = file_list(Path(base_path / psam.path))

        log.message('SI012', f'Adding directory {psam} to the database (files={len(psam.files)})')
        result = api.put(psam)

        if result['code'] == 'success':
            counters['AM-processed'] += 1

        else:
            counters['Error'] += 1
            message = result['message']
            # client-error --> ConditionalCheckFailedException usually a concurrency error.
            log.error('SE013', f'Unexpected error: {message}')

    else:
        counters['AM-already-processed'] += 1


def file_list(path):

    if not path.is_dir():
        return []

    files = []

    for file in os.scandir(path):
        if not file.is_file():
            continue

        md5_digest = None

        try:
            with open(file.path, 'rb') as f:
                md5_digest = hashlib.md5(f.read()).hexdigest()

        except IOError as e:
            log.exit('SF014', f'Could not read file: {file.path} ({e})')

        if md5_digest is None:
            log.exit('SF015', f'Could not create file hash for: {file.path}')
            continue

        files.append(dict(
            name=file.name,
            hash=md5_digest,
            size=file.stat().st_size))

    return files


def upload_files(base_path, argument):
    """
    Scan all amendment items where uploaded = F, check each file and upload any not yet
    uploaded. When all files are processed, set uploaded to T.

    If the files attribute has no files, rescan the directory and process the newly found
    files, otherwise log a warning and continue (manual resolution will be needed).

    Attempt to upload a file five time once every five seconds to allow for transient
    network drive problems.
    """
    select_ps = None

    if argument != 'all':
        select_ps = argument
        log.message('LI001', f'Processing planning scheme prefix: {select_ps}')

    psam_list = api.flagged(uploaded=False)
    log.message('LI002', f'Amendments to process: {len(psam_list):,}')

    to_upload = {}

    for psam_not_uploaded in psam_list:
        to_upload[str(psam_not_uploaded)] = psam_not_uploaded

    for psam_text in sorted(to_upload.keys()):
        psam = to_upload[psam_text]

        if select_ps is not None:
            if not psam.ps.startswith(select_ps):
                counters['Upload-ignore-not-selected'] += 1
                continue

        amendment = api.get(psam.ps, psam.am)['psam']

        counters['Upload-psam-processed'] += 1

        try:
            path = amendment.path

        except AttributeError:
            #sys.exit(amendment)
            log.message('LI015', f"{psam} No path found.")
            counters['Missing-path']
            continue

        hashes = {}

        if amendment.files is None:
            log.message('LW003', f"{psam} No files found (files=None).", level='warn')
            continue

        if len(amendment.files) == 0:
            log.message('LW003', f"{psam} No files found (no files).", level='warn')
            continue

        for file in amendment.files:
            assert file['hash'] is not None, f'{amendment} Hash is None for {file}'

            # Optimisation (x2 speedup)
            url = base_s3 + '/' + file['hash'] + '.pdf'
            response = session.head(url)

            if response.status_code == 200:
                counters['Already-uploaded'] += 1

            else:
                hashes[file['hash']] = file['name']

        presigned_urls = api.presigned_urls(hashes.keys())

        upload_count = 0
        upload_errors = 0

        for hash in presigned_urls:
            url      = base_s3 + '/' + hash + '.pdf'
            put_url  = presigned_urls[hash]
            pdf_file = base_path / Path(path) / Path(hashes[hash])

            try:
                response = session.head(url)

            except requests.exceptions.ConnectionError as e:
                log.exit('LF005', f'Unexpected error for ({url}) during session\n{e}')

            if response.status_code == 403: # Assume 403 = missing object

                retry = 5
                while retry > 0:

                    try:
                        with open(pdf_file, 'rb') as f:
                            response = session.put(put_url, data=f.read(), headers={'Content-Type': 'application/pdf'})
                            break

                    except FileNotFoundError:
                        log.message('LW006', f'File not found: {path}/{hashes[hash]} (retry {retry})', level='warn')
                        counters['Upload-error'] += 1
                        upload_errors += 1

                        time.sleep(5)
                        retry -= 1

                if retry == 0:
                    log.message('LW007', f'Retry failed.', level='warn')
                    break

                if response.status_code == 200:
                    log.message('LI012', f'Uploaded file {pdf_file}')

                else:
                    log.error('LE008', f'HTTP PUT error ({response.status_code}) for {path} {hash}\n{response.text}')
                    counters['Upload-error'] += 1
                    upload_errors += 1

                upload_count += 1
                counters['Upload-files'] += 1


        if upload_errors > 0:
            log.message('LW009', f"{psam} Uploaded {upload_count} files with ({upload_errors}) errors.", level='warn')

        else:
            if not amendment.uploaded:
                api.update(psam.ps, psam.am, uploaded=True)

            if upload_count > 0:
                log.message('LI010', f"{psam} Uploaded {upload_count} files.")
                counters['Upload-psam-completed'] += 1

            else:
                assert path is not None
                assert path != 'None'
                log.message('LI011', f"{psam} No files to upload for {path}.")
                counters['No-files-to-upload'] += 1


def publish_amendments(argument):
    """
    When agreed by the user, set published to True if uploaded=True and published=False and len(files) > 0.
    Published files are available on the departments website.
    """
    select_ps = None

    if argument != 'all':
        select_ps = argument
        log.message('PI001', f'Processing planning scheme prefix: {select_ps}')

    uploaded = api.flagged(uploaded=True)
    log.message('PI002', f'Uploaded amendments: {len(uploaded):,}')

    not_published = api.flagged(published=False)
    log.message('PI008', f'Not published amendments: {len(not_published):,}')

    to_publish = {}

    for psam_not_published in list(not_published):

        if select_ps is not None:
            if not psam_not_published.ps.startswith(select_ps):
                counters['Publish-ignore-not-selected'] += 1
                continue

        if psam_not_published in uploaded:

            psam = api.get(psam_not_published.ps, psam_not_published.am)['psam']

            if len(psam.files) == 0:

                log.message('PW003', f'Source data error (empty directory) for {psam.path}', level='warn')
                counters['Publish_source_data_error'] += 1

            else:
                to_publish[str(psam_not_published)] = psam_not_published

    log.message('PI004', f'Amendments to publish: {len(to_publish):,}')

    remember = None
    publish = False

    if len(to_publish) > 0:
        log.message('PI005', 'Note: y=yes, n=no, a=publish all remaining, q=exit script.')

    for psam_text in sorted(to_publish.keys()):
        psam = to_publish[psam_text]

        if remember is None:
            while True:
                res = input(f'PI006 Publish {psam_text} (y/n/a/q)? ').lower()

                if res in 'ynaq':
                    break

            if res == 'y':
                publish = True

            elif res == 'n':
                publish = False

            elif res == 'a':
                publish = True
                remember = True

            elif res == 'q':
                log.message('PI007', 'User requested exit.')
                break

        if publish:
            api.update(ps=psam.ps, am=psam.am, published=True)
            counters['Published'] += 1

        else:
            counters['Not-published'] += 1

        if remember:
            log.message('PI008', f'{psam} published.')


def delete_amendment(argument):
    """
    Delete a single amendment, e.g. Indigo/2017-08-29-VC139/Map
    Plus is replaced by the space character.
    """

    args = argument.replace('+', ' ').split('/', 2)

    if (len(args) == 0) or (len(args) > 3):
        log.message('DI001', f'{argument} must contain exactly one or two slashes.')
        return

    if len(args) == 2:
        #psam = PSAM(ps=args[0], am=args[1], maps=False)

        result = api.delete(ps=args[0], am=args[1])

        if result['code'] == 'success':
            log.message('DI002', f'{argument} deleted.')

        else:
           log.message('DW003', f"Delete ({result['code']}) {result['message']}", level='warn')


def read_url(ps, am=None, trigger='cumberland-html', test_squiz=False):
    # Squiz Matrix specific code. Will fail if the Squiz Matrix configuration changes
    #squiz_base = 'https://www.planning.vic.gov.au/schemes-and-amendments/browse-planning-scheme/histories/record-between-1997-to-present'
    #squiz_base_webservices = 'https://delwp-stage.clients.squiz.net/_webservices/planning-2018/planning-scheme/amazon-remote-content-pages/'
    #squiz_base_webservices = 'https://www.planning.vic.gov.au/_webservices/planning-2018/planning-scheme/amazon-remote-content-pages/'

    cloudfront = 'https://d18c9ng67a0edy.cloudfront.net/'

    urls = []

    ps_ = ps.replace(' ', '_')
    
    if am is None:
        urls.append(api.base_api + f'/cumberland-html/{ps_}?f_Scheme|planningSchemeName={ps_}')
        urls.append(cloudfront + ps_)

    else:
        am_ = am.replace(' ', '_')

        urls.append(api.base_api + '/cumberland-html/' + ps_ + '/' + am_)
        urls.append(cloudfront + ps_ + '/' + am_)

    # if test_squiz:
    #     if am is None:
    #         if ps not in ['Victoria Planning Provisions', 'Delatite']:
    #             urls.append(squiz_base + f'?f.Scheme%7CplanningSchemeName={ps}'.replace(' ','-'))
    #             urls.append(squiz_base_webservices + f'{ps}?f.Scheme%7CplanningSchemeName={ps}'.replace(' ','-'))

        # else:
        #     # ?f.Scheme%7CplanningSchemeName=Alpine&sq_content_src=%2BdXJsPWh0dHAlM0ElMkYlMkZzMy5kcGNkLnZpYy5nb3YuYXUlMkZwc2gtaW5kZXglMkZhbHBpbmVfdmM5Lmh0bWwmYWxsPTE%3D

    for url in urls:

        response = session.get(url)
        
        if response.status_code == 200:
            counters['Test URL read OK'] += 1

        else:
            log.message('IW003', f'Could not read {url}', level='warn')
            counters['Test Could not read URL'] += 1

        content = response.text

        # Test if something is really broken
        if len(content) < 1000:
            log.exit('IF004', f'Content too short ({len}) for {url}')

        if test_squiz:
            if trigger in content:
                counters['Test Trigger OK'] += 1

            else:
                log.message('IW005', f'Missing trigger string ({trigger}) for {url}', level='warn')
                counters['Test trigger missing'] += 1

        #return len(content)


def list_amendments(argument, test_access=False):

    test_access = True
    count = 0

    if test_access:
        read_url('')

    if argument == 'all':
        ps_list = api.planning_schemes(select='name-list', vpp=True)

    else:
        ps_list = [argument.replace('+', ' ')]

    for ps in ps_list:

        if test_access:
            read_url(ps, test_squiz=True)

        items = api.list(ps)['list']

        count_amendments = 0
        count_maps = 0
        count_uploaded = 0
        count_punlished = 0

        for item in items:

            ps = item.ps
            am = item.am

            count += 1

            if test_access:
                # 1% of amendments
                if count % 100 == 0:
                    read_url(ps, am, test_squiz=True)

                    # if len > 1000:
                    #     log.message('II005', f'  Test read url for {ps}/{am} ({len})')

            pub_date = item.pub_date[:10]

            desc = ''

            if am.endswith('/maps'):
                counters['List maps'] += 1
                count_maps += 1

            else:
                counters['List amendments'] += 1
                count_amendments += 1

            if item.uploaded:
                counters['List uploaded'] += 1
                count_uploaded += 1

                desc += f'Uploaded '

            if item.published:
                counters['List published'] += 1
                count_punlished += 1

                desc += f'Published({pub_date}) ' 

            if argument != 'all':
                log.message('II002', f"{ps} {am} {desc}")

            if item.path is None:
                log.exit('IF006a', f'{ps}/{am} has no path.')

        log.message('II001', f'{ps} (amendments={count_amendments} maps={count_maps} ' + \
            f'uploaded={count_uploaded} published={count_punlished})')
    

def stats(argument):
    psam_uploaded = api.flagged(uploaded=True, count=True)
    log.message('TI001', f'Amendments uploaded:          {psam_uploaded:,}')

    psam_not_uploaded = api.flagged(uploaded=False, count=True)
    log.message('TI002', f'Amendments not uploaded:      {psam_not_uploaded:,}')

    total_uploaded = psam_uploaded + psam_not_uploaded
    log.message('TI003', f'Total amendments (uploaded):  {total_uploaded:,}')

    psam_published = api.flagged(published=True, count=True)
    log.message('TI004', f'Amendments published:         {psam_published:,}')

    psam_not_published = api.flagged(published=False, count=True)
    log.message('TI005', f'Amendments not published:     {psam_not_published:,}')

    total_published = psam_published + psam_not_published
    log.message('TI006', f'Total amendments (published): {total_published:,}')


def test(base_path, argument):
    
    if argument == 'all':
        argument = 'info flagged invalid-am put update-pub update-path get delete presigned'

    log.message('Test0', f'Tests to run: {argument}')

    if 'info' in argument:
        print(json.dumps(api.test(name='info'), indent=2, sort_keys=True))

    if 'flagged' in argument:
        count = api.flagged(uploaded=True, count=True)
        print(f'\nAmendments uploaded: {count:,}')

    ps = 'Zealand'
    am = '2020-02-23 C500'
    path = 'Zealand/2020-01-22-C367zeal/2020-01-22-C367zeal'

    psam = PSAM(ps, am, path=path)

    psam.files = [
            {
                "hash": "Z91b613cfee423afc815b5872f3a8dd65",
                "name": "Z00.pdf",
                "size": int(809326)
            },
            {
                "hash": "3e4e934163e4ffa60df842808e8fb48e",
                "name": "Z01.pdf",
                "size": int(809974)
            }
            ]


    files = [
            {
                "hash": "blah13cfee423afc815b5872f3a8dd65",
                "name": "Xyz00.pdf",
                "size": int(25)
            },
            {
                "hash": "blah934163e4ffa60df842808e8fb48e",
                "name": "Xyz01.pdf",
                "size": int(25)
            }
        ]

    #print('\nMost test are silent if successful.')

    if 'invalid-am' in argument:
        psam_get = api.get(ps, 'unknown am')
        #print ('get (invalid am)', psam_get)
        assert psam_get['code'] == 'item-not-found'

    if 'put' in argument:
        print('put-before', repr(psam))
        result = api.put(psam)
        #print('put-after', result)
        assert result['code'] == 'success', f"[put] {result['code']} \n{result['message']}"

        result = api.get(ps, am)
        print('get-after-put', repr(result['psam']))
        assert result['code'] == 'success', f"[get-after-put] {result['code']} \n{result['message']}"

    if 'update-pub' in argument:
        result = api.update(ps, am, published=True)
        #print('update published', result)
        assert result['code'] == 'success', f"[update-pub] {result['code']} \n{result['message']}"

    if 'update-path' in argument:
        result = api.update(ps, am, path='a/b/c/d')
        #print('update path', result)
        assert result['code'] == 'success', f"[update-pub] {result['code']} \n{result['message']}"

    if 'get' in argument:
        psam_get = api.get(ps, am)['psam']
        print('get amendment', repr(psam_get))
        #print('get amendment files', json.dumps(psam_get.files, indent=2, sort_keys=True))
        assert len(psam_get.files) == 2, f"[get] files != 2"

    if 'delete' in argument:
        result = api.delete(ps, am)
        #print('delete', result)
        assert result['code'] == 'success', f"[delete] {result['code']} \n{result['message']}"

    hashes = ['e27c2adb8e071abfbdfddd1b930f9e33', 'b91ce7c8d855eb1d67b2ac2254a49068', '4ef8a0fc3988cdab28a791739c1a5189']

    if 'presigned' in argument:
        result = api.presigned_urls(hashes)
        assert len(result.keys()) == 3

    if 'fix-ups' in argument:

        checked = {}

        for psam_uploaded in sorted(api.flagged(uploaded=True)):

            # Only the new naming convention (from around 2018-10)
            if psam_uploaded.am[0:4] not in ['2020']:         # ['2016', '2017'] ['2018', '2019', '2020']:
                counters['AM-before-value'] += 1
                continue

            psam = api.get(psam_uploaded.ps, psam_uploaded.am)['psam']

            if psam.path is None:
                log.message('Test1', f'{psam} - path is none - {psam.path}')
                counters['Path-is-none'] += 1
                continue

            if not Path(Path(base_path) / Path(psam.path)).exists():
                log.message('Test2', f'Deleted not found {psam}')
                counters['Path-does-not-exist'] += 1
                api.delete(psam.ps, psam.am)

            ppl = psam.path.lower()
            if ppl in checked:
                dup = checked[ppl]
                #log.message('Test3', f'{psam} duplicate {dup}')

                if len(psam.am) < len(dup.am):
                    api.delete(psam.ps, psam.am)
                    log.message('Test4', f'Deleted {psam}, kept {dup}')

                elif len(psam.am) > len(dup.am):
                    api.delete(dup.ps, dup.am)
                    log.message('Test5', f'Deleted {dup}, kept {psam}')

                elif 'revised' in psam.am:
                    api.delete(psam.ps, psam.am)
                    log.message('Test6', f'Deleted {psam}, kept {dup}')

                elif 'revised' in dup.am:
                    api.delete(dup.ps, dup.am)
                    log.message('Test7', f'Deleted {dup}, kept {psam}')


                counters['Path-duplicate'] += 1

            else:
                checked[psam.path.lower()] = psam


# ----------------------------------------------------------------------------------------
def main(command, argument):

    if command == 'scan':
        # Adds each amendment along with a list of all clause and map files to the cloud database.
        scan_directories(base_path, argument)
        upload_files(base_path, 'all')

    elif command == 'upload':
        # Using data from the cloud database, upload each unique clause and map PDF file to AWS S3.
        upload_files(base_path, argument)

    elif command == 'publish':
        # Set a flag in the index for an amendment to make it available to the Planning website
        publish_amendments(argument)

    elif command == 'delete':
        # Remove the index entry for the amendment and its associated files from the cloud database.
        delete_amendment(argument)

    elif command == 'list':
        # Show basic information and the status (published; all files uploaded) for one or more amendments.
        list_amendments(argument)

    elif command == 'stats':
        # argument is s3
        stats(argument)

    elif command == 'test':
        test(base_path, argument)


if __name__ == '__main__':

    try:
        (config_file, config) = get_config('client')

        try:
            api_key   = config['api_key']
            base_api  = config['base_api']
            base_path = Path(config['base_path'])
            base_s3   = config['base_s3']

        except KeyError as e:
            sys.exit(f'XI004 Cannot find config item: {e}')

        log = Message(version=SCRIPT_VERSION, cmdline=sys.argv, config_file=config_file)

        parser = argparse.ArgumentParser()
        parser.add_argument('command', choices=['scan', 'upload', 'publish', 'delete', 'list', 'stats', 'test'])
        parser.add_argument('argument', default='all')

        args = parser.parse_args()

        counters = collections.Counter()

        with requests.Session() as session:

            api = CumberlandApi(base_api, api_key, session)

            main(args.command, args.argument)

    except KeyboardInterrupt:
        print('\nXI005 Ctrl-C User requested Exit.')

    except:
        raise

    if len(counters) > 0:
        log.message('XI006', 'Counters:', new_line=True)
        for c in counters:
            key = c.capitalize().replace('-',' ').replace('_', ' ').ljust(25)
            log.message('XI007', f'{key} {counters[c]:,}')

    log.finalise()
