# Cumberland

The purpose of this software is to publish the historical amendments to the Victorian 
[Planning Scheme Histories](https://www.planning.vic.gov.au/schemes-and-amendments/browse-planning-schemes) 
(1997 to 2020+) including Victoria Planning Provisions (VPP).

To see the published amendments, select a 
[Planning Scheme](https://www.planning.vic.gov.au/schemes-and-amendments/browse-planning-schemes) 
(1997 to 2020+) or the Victoria Planning Provisions (VPP); and then select the ```Histories``` tab.

Historical amendments are stored with one PDF file per clause or map.


## Client

The client script ```cumberland.py```, performs the following functions:

* Scan the internal source network drive by Planning Scheme (or prefix) for all amendments or for amendments since a particular date.
* Upload details of the amendment and its associated PDF files to an AWS DynamoDB NoSQL database.
* Upload any new PDF files. Only unique files are stored in AWS S3


### Installation and Configuration

These tasks only need to be done once for every PC that will run the script.

Install Python 3.8 (or later). On a standard PC, this task requires a servie request.

Update ```pip``` and install ```requests``` (used to communicate with the cloud API's):

```
py -m pip install --upgrade --user pip
py -3 -m pip install --user requests
```

Download the script (an existing file will be overridden):

```
H:
cd \
curl -O cumberland.py https://gitlab.com/tonyallan/cumberland/-/raw/master/client/cumberland.py
```

A configuration file (```cumberland.config```) must exist in the users ```H:\```, 
their home directory or the directory where the script is run. It contains a token and a base path. 
The following values are examples only:

```
[client]
api_key   = EXAMPLEjkhfH876riuf75B76t7iJ
base_api  = https://jo9grgdjai.execute-api.ap-southeast-2.amazonaws.com/prod
base_s3   = https://cumberland-files.s3.amazonaws.com
base_path = Z:/Planning Scheme Histories
```

Test that it is working OK using the command:

```py cumberland.py stats all```

which will return something like:

```
0:00:00.01 XI001 Script (v12) started at 2020-03-16 11:40 (using config file H:\cumberland.config)
0:00:00.70 TI001 Amendments uploaded:          41,464
0:00:00.78 TI002 Amendments not uploaded:      83
0:00:00.79 TI003 Total amendments (uploaded):  41,547
0:00:01.28 TI004 Amendments published:         41,461
0:00:01.35 TI005 Amendments not published:     86
0:00:01.37 TI006 Total amendments (published): 41,547
```

The script is now successfully downloaded and configured.


### Normal Usage

All command are issued using Windows PowerShell (available in the Windows start menu).

Normally the script and the configuration file are stored in the root directory of ```H:```, although any directory is acceptable.

```
H:
cd \
```

When new amendments are ready:

```
py cumberland.py scan yyy-mm-dd
```

where ```yyyd-mm-dd``` is the earliest directory to [scan](#scan) and [upload](#upload). For example, 
if the directory is ```Swan Hill/2020-02-11-VC168```, the the scan command would be:

```
py cumberland.py scan 2020-02-11
```

When the scan (and creation of the index files is complete), uploading will commence automatically.

To pulish the amendments, use the command:

```
py cumberland.py publish all
```

### Detailed Usage

All command are issued using Windows PowerShell (available in the Windows start menu).

All command may be interupted using Ctrl-C. 
If the script restarted, processing will continue from where it was interupted.
Scan must rescan the directories before continuing.

Basic operation involves a [scan](#scan) followed by [publish](#publish).

General messages:

| Code | Level | Description |
| --- | --- | --- |
| XI001 | Info | Script (v{}) started at {} using config {} |
| XI002 | Exit | Cannot find config file <br> Follow the Installation and Configuration instructions above. |
| XI003 | Exit | Cannot read config file <br> Follow the Installation and Configuration instructions above. |
| XI004 | Exit | Cannot find config item: '{}' <br> Check the contents of the configuration file. |
| XI005 | Info | Ctrl-C User requested Exit. |
| XI006 | Info | Counters: |
| XI007 | Info | {name} {count} <br> Various informational counters. Many refer to previous messages. No action required. |
| XF090 | Exit | HTTP Gateway Timeout (most likely PC went to sleep). Retry the command. |
| XF100 | Exit | HTTP Error ({}) {} for {url} <br> Retry the command. If the problem persists, request support. |
| XF101 | Exit | API Error ({}) {} for {url} <br> Retry the command. If the problem persists, request support. |


#### Scan

Adds each amendment along with a list of all clause and map files to the cloud database.

The command can scan all directories for each planning scheme; 
scan starting with a Planning Scheme prefix; 
or scan starting from a particular amendment date. Arguments are not case sensitive.

Scanning already scanned amendments takes time but is otherwise harmless.
The process may be interupted at any time. It will continue after a rescan of the directories.

Once a scan is completed it will automatically start uploading any outstanding unique files to S3.

```
py cumberland.py scan all
                 scan yyy-mm-dd
```

Examples:

```
py cumberland.py scan all
                 scan 2019-12-30
```

Messages for scan:

| Code | Level | Description |
| --- | --- | --- |
| SI001 | Info | Processing directories since: {date} |
| SF002 | Exit | {base_path} configuration error. <br> The configuration option ```base_path``` is verified by attemting to read the Alpine directory. Check the configuration file, the location specified by ```base_path``` and try again. |
| SI003 | Info | Ignore non-Planning Scheme directory: {dir} <br> Ignored 'Upper Yarra Valley & Dandenong Ranges Regional Strategy Plan' |
| SI004 | Info | Processing {ps} |
| SI006 | Info | Ignore directories that don't start with a year ({ps}): {am} |
| SI007 | Info | Ignore directories that contain 'copy' ({ps}): {am} |
| SI008 | Info | Ignore directories that contain 'not valid' ({ps}): {am} |
| SE009 | Error | No Victoria Planning Provisions second level directory for ({ps}): {am} <br> Fix the non-standard directory name. |
| SE010 | Error | First and second level dates don't match ({ps}): {am} vs {am} <br> Fix the non-standard directory name. |
| SI012 | Info | Adding directory {psam_text} to the database <br> to the database to be processed during [upload](#upload). |
| SE013 | Error | Unexpected error: {error} <br> updating the database. Retry the command. If the problem persists, request support. |
| SF014 | Exit | Could not read binary file: {file} <br> Unable to calculate a MD5 hash for the file. Retry the command. If the problem persists, request support. |
| SF015 | Exit | Could not create file hash for: {file} <br> Unable to calculate a MD5 hash for the file. Retry the command. If the problem persists, request support. |
| SI016 | Info | {ps-am} Unexpected text '{}', changed to '{}' in the database. <br> Catch name variations and fix in the database. |


#### Upload

Normally uploading files will happen as part of a [scan](#scan) but the process can be run separately using this command.

Using data from the cloud database, upload each unique clause and map PDF file to AWS S3. 
If identical PDF files exist as a part of multiple amendments, the file will only be uploaded once.

```
py cumberland.py upload all
                        prefix
```

A ```prefix``` is part of the planning scheme name, 
for example the following will process all planning schemes that begin with 'M':

```
py cumberland.py upload M
```

The upload process may be interupted at any time. 
It will continue with any remaining files once restarted.

Messages for upload:

| Code | Level | Description |
| --- | --- | --- |
| LI001 | Info | Processing planning scheme prefix: {ps} |
| LI002 | Info | Amendments to process: {} |
| LW003 | Warning | {ps-am} No files found. <br> The indicated directory exists but did not contain any PDF files. This may indicate a problem with the source directory. |
| LI004 | Info | {ps-am} Re-processed {} files for path {}. |
| LF005 | Exit | Unexpected error for ({url}) during session {error} <br> Retry the command. If the problem persists, request support. |
| LW006 | Warning | File not found: {file} (retry {retry} <br> An attempt to read the file will be retried uo to five times, in case this was an error accessing the network drive. |
| LW007 | Warning | Retry failed. Clearing file list for {ps-am}. <br> A file that was found during the scan no longer exists so the file list is cleared. Retry the [scan](#scan) to reprocess the amendment.|
| LE008 | Error | HTTP PUT error ({}) for {file} {error} <br> Retry the command. If the problem persists, request support. |
| LW009 | Warning | {ps-am} Uploaded {} files with ({}) errors. <br> Previous error messages should be reviewed. |
| LI010 | Info | {ps-am} Uploaded {} files. Copied {}. |
| LI011 | Info | {ps-am} No files to upload for {path}. |
| LI012 | Info | Uploaded file {pdf_file} <br> Not printed in all circumstances for output brevity. |
| LI013 | Info | {ps-am} Updating path {} in database. |
| LI014 | Info | {ps-am} Could not update path in database. |
| LW015 | Warning | {ps-am} Could not update path in database. |
>>>>>>> need to renumber these

Depending on the number of new files per amendment, the upload function can take several minutes per amendment directory.


#### Publish

Set a flag in the index for an amendment to make it available to the Planning 
[website](https://www.planning.vic.gov.au/schemes-and-amendments/browse-planning-schemes).

All unpublished or a specific amendment may be selected. 
Once published, an amendment will no longer be listed here.

```
py cumberland.py publish all
                         prefix
```

A ```prefix``` is part of the planning scheme name, 
for example the following will publish amendments for all planning schemes that begin with 'M':

```
py cumberland.py publish M
```

If more than one amendment is selected, publish will list outstanding amenmmends and request confirmation (Y/N).

Messages for publish:

| Code | Level | Description |
| --- | --- | --- |
| PI001 | Info | Processing planning scheme prefix: {ps} |
| PI002 | Info | Uploaded amendments: {} |
| PW003 | Warning | Source data error (empty directory) for {path} <br> Fix the source directory and rescan. |
| PI004 | Info | Amendments to publish: {} |
| PI005 | Info | Note: y=yes, n=no, a=publish all remaining, q=exit script. |
| PI006 | Info | Publish {ps-am} (yes/no/all/quit)? |
| PI007 | Info | User requested exit. |
| PI008 | Info | Not published amendments: {} |

The publish command may take a minute or two before the first question, especially if there are many outstaning amendments.


#### Delete

It is unlikely that this function will be needed.

Remove the index entry for the amendment and its associated files from the cloud database. 
Once the website cache has expired, the amendment will no longer be visible on the Planning webite.

```
py cumberland.py delete Yarriambiack/2020-02-11-VC168/Map
```

Each command can only delete one amendment. It will request confirmation (Y/N).
PDF files are not deleted, just the index and list of files from the cloud database.

Messages for stats:

| Code | Level | Description |
| --- | --- | --- |
| DI001 | Info | {ps/am/map} must contain exactly one or two slashes. |
| DI002 | Info | {ps/am/map} deleted. |
| DW003 | Info | Delete (does-not-exist) Planning Scheme and Amendment does not exist <br> Delete (ps-does-not-exist) Planning Scheme does not exist |

The delete command takes several seconds to run.



#### List

Show basic information and the status (published; all files uploaded) for one or all planning schemes.

The published URLs are verified for each planning scheme and for about 1% of amendments. 
The path field is also checked to see that it has been specified..

```
py cumberland.py list all
                 list Yarra
                 list Victoria+Planning+Provisions
```

Messages for list:

| Code | Level | Description |
| --- | --- | --- |
| II001 | Info | {planning scheme} (amendments={} maps={} uploaded={} published={}) <br> If ```all``` specified. |
| II002 | Info | {planning scheme} {amendment} {desc} |
| IW003 | Warning | Could not read published url {url} <br> Test if the published URL is available. Message not shown during normal operation. |
| IF004 | Exit | Content too short ({}) <br> Message not shown during normal operation. |
| IW005 | Warning | Missing trigger string ({trigger}) <br> Test if the Squiz published URL contains the string 'cumberland-html'. |
| II006 | Info | Test read url for {ps}/{am} ({}) <br> Message not shown during normal operation. |

The list command takes about ten seconds per planning scheme to run.



#### Stats

Overall stats.

```
py cumberland.py stats all
```

Messages for stats:

| Code | Level | Description |
| --- | --- | --- |
| TI001 | Info | Amendments uploaded: {}
| TI002 | Info | Amendments not uploaded: {}
| TI003 | Info | Total amendments (uploaded): {}
| TI004 | Info | Amendments published: {}
| TI005 | Info | Amendments published: {}
| TI006 | Info | Total amendments (published): {}

The stats command takes several seconds to run.



### Planning Scheme and Directory Processing Rules

These rules (reflecting existing practice) are used to select the directories to be processed 
and also to extract the required metadata describing each amendment.

The base directory is ```...\Planning Scheme Histories```.

Each folder in the base that starts with an alphabetic character is a planning scheme name.
Each amendment in each planning scheme is processed in turn.
Only PDF files are selected (DOC and JPG files are ignored).

The amemdment directory format is one or two levels:

* ```yyyy-mm-dd-xxnnn suffix```
* ```yyyy-mm-dd-xxnnn suffix/yyyy-mm-dd-xxnnn suffix```   (since about 2011-11-19)

For example:

* ```Melbourne\1999-04-30-C1```
* ```Port of Melbourne\2019-12-03-VC165\2019-12-03-VC165```

The amendment type (xx) must be one of C|VC|V|GC.

Where there are two directory levels the first is *always* taken as the correct name. 
A message is printed if they differ.

Supported suffixes include:

* ```(Part nn)```     e.g. ```(Part 1)```
* ```(Part nx)```     e.g. ```(part 1A)```
* ```(Revised nn)```
* ```(Revised)```
* ```(Revoked)```
* ```(VOID)```

```Victoria Planning Provisions``` are processed as for any planning scheme except that the second 
level directory is labeled ```AM xxnnn PDF```, e.g. ```AM VC94 PDF```. Assume no ```Maps``` directories.

Maps are processed if the first or second (if two) directory contains a directory ```PDF MAPS``` or ```Maps```.
If both ```PDF MAPS``` and ```Maps``` are present the first is chosen.

The following amendment directories are ignored:

1. Do not follow the patterns above
2. Doesn't start with four digits (i.e. 2020)
3. A suffix of ```copy``` or ```not valid```
4. ```1996-11-18-Pre-Launch version```
5. ```Amendments```, ```Approved Amendments``` and ```Exhibition Amendments```
6. ```Current Word Local Prov``` at the first level
7. Any second level ```Victoria Planning Provisions``` directory that doesn not contain ```PDF```.
8. Empty directories.
9. ```Upper Yarra Valley & Dandenong Ranges Regional Strategy Plan```

Examples of names that are OK:

* ```Melbourne\1999-04-30-C1```
* ```Port of Melbourne\2019-12-03-VC165\2019-12-03-VC165```

Examples of directory names that are ignored:

* ```!Minsiterial Directions```
* ```Alpine\Approved Amendments```
* ```Boroondara\Wrong VC147\2018-09-27-C268(Part 1)```
* ```Brimbank\Brimbank\2011-11-18-VC83```
* ```Moreland\2014-07-01-VC116(not valid)\2014-07-01-VC116```

Special cases include:

* ```Revised 19 Nov 2010``` is treated as just ```Revised```
* ```NFPS``` is treated just as ```NFPS```

Mismatched first and second level amendment names:

* ```French Island and Sandstone Island\2004-12-23-VC32\2017-04-13-VC136``` is processed as ```French Island and Sandstone Island\2004-12-23-VC32```
* ```East Gippsland\2013-03-07-C109\2013-03-05-C109``` is processed as ```East Gippsland\2013-03-07-C109```
* ```Darebin\2017-08-11-C160\2017-07-27-VC137``` is processed as ```Darebin\2017-08-11-C160```



## Pubication

All data is public.

### Published URL's

These URL's are wrapped by Squiz Matrix (as part of ```planning.vic.gov.au```) and are not visible to website users. 
CSS is also applied for standard departmental styling.

These URL's can be viewed in a normal browser.

Other information can be viewed by starting with the Planning Scheme Index and clicking on desired links within the pages.

Planning Scheme Index:
```https://jo9grgdjai.execute-api.ap-southeast-2.amazonaws.com/prod/cumberland-html```

Planning Scheme Amendment List:
```https://jo9grgdjai.execute-api.ap-southeast-2.amazonaws.com/prod/cumberland-html/Alpine```

A Planning Scheme Amendment:
```https://jo9grgdjai.execute-api.ap-southeast-2.amazonaws.com/prod/cumberland-html/Alpine/2020-02-11+VC168```


### PDF Files

The base directory for PDF files is: ```https://cumberland-files.s3-ap-southeast-2.amazonaws.com/```
with a filename of the form ```e27c2adb8e071abfbdfddd1b930f9e33.pdf```.


## AWS Resources

### CloudFront

Front-end the HTML generation to allow for the unfixable use by Squiz Matrix of the unsafe pipe character. Although it is only a workaround, once the URL is embedded in the Sqiuz configuration is becomes effectively permanent.

Squiz Matrix caches the output so CloudFront is set to not cache.


### S3 bucket

For storage of PDF files.

* ```cumberland-files``` (Sydney)


### DynamoDB database

All directory data (clauses and maps), one row per directory.

* ```cumberland-psam``` (Sydney) with two indexes: 
  * ```published-index```
  * ```uploaded index```

The database ```cumberland-psam``` normally has two entries for an amendment, one for the clauses and a second (suffixed by ```#maps```) holding the list of maps. These are not combined because the [maximum DynamoDB item size](https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Limits.html#limits-items) is 400KB.


### Lambda functions

All python code to:

1. API's (```cumberland```) for the the client script ```cumberland.py```.
2. HTML generation (```cumberland-html```).

| Lambda Function | Method | API Function | Access | Description |
| --- | --- | --- | --- | --- |
| cumberland | GET    | test             | Public    | Test data |
| cumberland | GET    | planning-schemes | Public    | Query = select=full/code/name/code-list/name-list and optionally vpp=True/False |
| cumberland | GET    | list             | Public    | Subset of data for Amendments associated with the specified Planning Scheme |
| cumberland | GET    | flagged          | Public    | Query = uploaded=True/False, published=True/False, count=True/False |
| cumberland | GET    | amendment        | Public    | Retrieve Amendment associated with the specified Planning Scheme and Amendment (and optionally /maps) |
| cumberland | PUT    | amendment        | Protected | Create a new Amendment associated with the specified Planning Scheme and Amendment (and optionally /maps) |
| cumberland | POST   | amendment        | Protected | Update an Amendment associated with the specified Planning Scheme and Amendment (and optionally /maps) |
| cumberland | DELETE | amendment        | Protected | Delete an Amendment associated with the specified Planning Scheme and Amendment (and optionally /maps) |
| cumberland | POST   | presigned_urls   | Protected | Return a list of pre-signed urls given a list of hashes to allow a subsequent upload to S3 |

Return a ```not-implemented``` error if an unknown method/API function is specified.

For example:

```https://jo9grgdjai.execute-api.ap-southeast-2.amazonaws.com/prod/cumberland/planning-schemes?select=name-list&vpp=True```